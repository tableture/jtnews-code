#import "TabletureAppDelegate.h"
#import "JSON.h"
#import "Constants.h"
#import "SubscriptionViewController.h"
#import "Subscription.h"

@implementation TabletureAppDelegate

#pragma mark - Properties
@synthesize window=_window;
@synthesize tabletureViewController;

@synthesize facebook = _facebook;
@synthesize adsJSONResponse;

//@synthesize productsArrayFromConfigurationFeed;

#pragma mark - Conditional Start
- (void)startWithForm:(BOOL)yesOrNo {
    if(yesOrNo)
    {
        feedViewController = [[FeedViewController alloc] init];
        SubscriptionViewController *subscriptionViewController = [[SubscriptionViewController alloc] init];
        [subscriptionViewController setPurposeIsToUpdate:NO];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:subscriptionViewController];
        
        if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
        {
            //[self.window addSubview:feedViewController.view];
            
             [self.window addSubview:nav.view];
            //[feedViewController release];
        }
        else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
        {
            [self.window setRootViewController:nav];    
        }   
       // [feedViewController presentModalViewController:subscriptionViewController animated:NO];
       // [subscriptionViewController release];
    }
    else
    {
        feedViewController = [[FeedViewController alloc] init];
        if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
        {
            [self.window addSubview:feedViewController.view];
            //[feedViewController release];
        }
        else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
        {
            [self.window setRootViewController:feedViewController];    
        }   
        
        [self downloadAdsFeed];
        
        /*if(kFeedEnabled)
        {
            categoryIndex = 0;
            categoryAdLandscapeIndex = categoryAdPortraitIndex = categoryAdBottomIndex = 1;    
            [self feedStart];
        }
        else
        {
            [self feedStop];
        }*/
    }
}


#pragma mark - Application Delegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {   
   
//    NSLog(@"%@",[[UIDevice currentDevice] uniqueIdentifier]);
    [self.window makeKeyAndVisible];
    
    firstWebServiceConnection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kWebServiceConfiguration, UserID]]] delegate:self startImmediately:YES];
    
    /*
    NSString *path = [[NSBundle mainBundle] pathForResource:
                      @"clear" ofType:@"plist"];
    NSDictionary *originalPlist = [NSDictionary dictionaryWithContentsOfFile:path];
    [originalPlist writeToFile:[NSString stringWithFormat:@"%@/feeds/section_%d.plist",kPathDocumentsDirectory,0]  atomically:YES];
    [originalPlist writeToFile:[NSString stringWithFormat:@"%@/feeds/section_%d.plist",kPathDocumentsDirectory,1]  atomically:YES];
    [originalPlist writeToFile:[NSString stringWithFormat:@"%@/feeds/section_%d.plist",kPathDocumentsDirectory,2]  atomically:YES];
    [originalPlist writeToFile:kPathConfigurationPlistPath atomically:YES];
    [originalPlist writeToFile:kPathHomePlistPath atomically:YES];
    [originalPlist writeToFile:kPathEventsPlistPath atomically:YES];
    [originalPlist writeToFile:kPathAdsPlistPath atomically:YES];
    */

    return YES;
}




- (void)applicationWillResignActive:(UIApplication *)application {
    
    
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    if([self checkDownloadedUserDefaults] == NO)
    {
        feedViewController = [[FeedViewController alloc] init];
        [self.window makeKeyAndVisible];
        [self.window setRootViewController:feedViewController];    
        
        if(kFeedEnabled)
        {
            categoryIndex = 0;
            categoryAdLandscapeIndex = categoryAdPortraitIndex = categoryAdBottomIndex = 1;    
            [self feedStart];
        }
        else
        {
            [self feedStop];
        }
    }
}

/*-(void)applicationDidBecomeActive:(UIApplication *)application {

    [self.window makeKeyAndVisible];
    
    firstWebServiceConnection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", kWebServiceConfiguration, UserID]]] delegate:self startImmediately:YES];
    
    
}*/

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

#pragma mark - Memory Management
- (void)dealloc {
    
//    [productsArrayFromConfigurationFeed release];
    [tabletureViewController release];
    
    [_facebook release];
    
    [_window release];
    [super dealloc];
}

#pragma mark - Feed Methods
- (void)downloadConfigurationFeed {
    [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithFormat:@"%@/feeds",kPathDocumentsDirectory]
                              withIntermediateDirectories:NO 
                                               attributes:nil 
                                                    error:nil];
    
    NSURL *URL = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", kWebServiceConfiguration, UserID]];
    NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL 
                                                cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                            timeoutInterval:kWebServiceTimeoutInterval];
    [URL release];
    configurationFeedConnection = [[NSURLConnection alloc] initWithRequest:URLRequest 
                                                         delegate:self
                                                 startImmediately:YES];
}
- (void)downloadAdsFeed {
    NSDictionary *configurationDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath];
    NSString *adsWebServiceString = [configurationDictionary objectForKey:@"ads"];
    if(adsWebServiceString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:@"ads webservice URL is missing from the feed"];
        }
        else
        {
            [self feedStopError];
        }
    }
    else 
    {
        NSURL *URL = [[NSURL alloc] initWithString:adsWebServiceString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL 
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        adsFeedConnection = [[NSURLConnection alloc] initWithRequest:URLRequest 
                                                            delegate:self
                                                    startImmediately:YES];   
    }             
}
- (void)downloadHomeFeed {
    NSDictionary *configurationDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath];
    NSString *homeWebServiceString = [configurationDictionary objectForKey:@"home"];
    if(homeWebServiceString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:@"home webservice URL is missing from the feed"];
        }
        else
        {
            [self feedStopError];
        }
    }
    else 
    {
        NSURL *URL = [[NSURL alloc] initWithString:homeWebServiceString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        homeFeedConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                             delegate:self
                                                     startImmediately:YES];
    }
}
- (void)downloadEventsFeed {
    NSDictionary *configurationDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath];
    NSString *eventsWebServiceString = [[configurationDictionary objectForKey:@"events"] objectForKey:@"link"];
    if(eventsWebServiceString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:@"events webservice URL is missing from the feed"];
        }
        else
        {
            [self feedStopError];
        }
    } 
    else 
    {
        NSURL *URL = [[NSURL alloc] initWithString:eventsWebServiceString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        eventsFeedConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                               delegate:self
                                                       startImmediately:YES];
    }
}
- (void)downloadCategoriesFeed {
    NSDictionary *configurationDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath];  
    NSDictionary *categoryDictionary = [[configurationDictionary objectForKey:@"list"] objectAtIndex:categoryIndex];
    NSString *categoryWebServiceString = [categoryDictionary objectForKey:@"link"];
    if(categoryWebServiceString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:@"categories webservice URL is missing from the feed"];
        }
        else
        {
            [self feedStopError];
        }
    }
    else 
    {
        NSURL *URL = [[NSURL alloc] initWithString:categoryWebServiceString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        categoryFeedConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                                 delegate:self
                                                         startImmediately:YES];
    }
}

#pragma mark - Images Methods
- (void)downloadHomeAdLandscape {
    [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithFormat:@"%@/ads",kPathDocumentsDirectory] withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
    NSDictionary *homeAdsDictionary = [adsDictionary objectForKey:@"home"];
    NSDictionary *fullPageHomeAdsDictionary = [homeAdsDictionary objectForKey:@"fullPage"];
    
    NSString *landscapeAdURLString = [fullPageHomeAdsDictionary objectForKey:@"landscape"];
    
    //NSLog(@"%@",landscapeAdURLString);
    
    if(landscapeAdURLString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:@"full page, landscape, home ad URL is missing from the feed"];
        }
        else
        {
            [self feedStopError];
        }
    }
    else 
    {
        NSURL *URL = [[NSURL alloc] initWithString:landscapeAdURLString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        homeAdLandscapeConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                                    delegate:self
                                                            startImmediately:YES];
    }
}
- (void)downloadHomeAdPortrait {   
    NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
    NSDictionary *homeAdsDictionary = [adsDictionary objectForKey:@"home"];
    NSDictionary *fullPageHomeAdsDictionary = [homeAdsDictionary objectForKey:@"fullPage"];
    
    NSString *portraitAdURLString = [fullPageHomeAdsDictionary objectForKey:@"portrait"];
    
    //NSLog(@"%@",portraitAdURLString);
    
    if(portraitAdURLString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:@"full page, portrait, home ad URL is missing from the feed"];
        }
        else
        {
            [self feedStopError];
        }
    } 
    else
    {
        NSURL *URL = [[NSURL alloc] initWithString:portraitAdURLString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        homeAdPortraitConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                                   delegate:self
                                                           startImmediately:YES];
    }
}
- (void)downloadHomeAdBottom {
    NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
    NSDictionary *homeAdsDictionary = [adsDictionary objectForKey:@"home"];
    NSString *bottomAdURLString = [[homeAdsDictionary objectForKey:@"bottom"] objectForKey:@"bannerImage"];
    
    //NSLog(@"%@",bottomAdURLString);
    
    if(bottomAdURLString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:@"bottom, home ad URL is missing from the feed"];
        }
        else
        {
            [self feedStopError];
        }
    } 
    else 
    {
        NSURL *URL = [[NSURL alloc] initWithString:bottomAdURLString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        homeAdBottomConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                                delegate:self
                                                         startImmediately:YES]; 
    }
}
- (void)downloadCategoryAdsLandscape {
    NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
    
    NSDictionary *sectionsAdsDictionary = [adsDictionary objectForKey:@"sections"];

    NSDictionary *sectionDictionary = [sectionsAdsDictionary objectForKey:[NSString stringWithFormat:@"%d",categoryAdLandscapeIndex]];
    NSString *landscapeAdSectionURLString = [[sectionDictionary objectForKey:@"fullPage"] objectForKey:@"landscape"];
    
    //NSLog(@"111%@",landscapeAdSectionURLString);
    if(landscapeAdSectionURLString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:[NSString stringWithFormat:@"full page, landscape, section%d ad URL is missing from the feed",categoryAdLandscapeIndex]];
        }
        else
        {
            [self feedStopError];
        }
    } 
    else 
    { 
        NSURL *URL = [[NSURL alloc] initWithString:landscapeAdSectionURLString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        categoryAdLandscapeConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                                        delegate:self
                                                                startImmediately:YES];
    }
}
- (void)downloadCategoryAdsPortrait {
    NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
    
    NSDictionary *sectionsAdsDictionary = [adsDictionary objectForKey:@"sections"];
    
    NSDictionary *sectionDictionary = [sectionsAdsDictionary objectForKey:[NSString stringWithFormat:@"%d",categoryAdPortraitIndex]];
    NSString *portraitAdSectionURLString = [[sectionDictionary objectForKey:@"fullPage"] objectForKey:@"portrait"];
    if(portraitAdSectionURLString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:[NSString stringWithFormat:@"full page, portrait, section%d ad URL is missing from the feed",categoryAdLandscapeIndex]];
        }
        else
        {
            [self feedStopError];
        }
    }
    else
    {
        NSURL *URL = [[NSURL alloc] initWithString:portraitAdSectionURLString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        categoryAdPortraitConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                                       delegate:self
                                                               startImmediately:YES];
    }
}
- (void)downloadCategoryAdsBottom {
    NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
    
    NSDictionary *sectionsAdsDictionary = [adsDictionary objectForKey:@"sections"];
    
    NSDictionary *sectionDictionary = [sectionsAdsDictionary objectForKey:[NSString stringWithFormat:@"%d",categoryAdBottomIndex]];
    NSString *bottomAdSectionURLString = [[sectionDictionary objectForKey:@"bottom"] objectForKey:@"bannerImage"];
    
   // NSLog(@"%@",bottomAdSectionURLString);
    
    if(bottomAdSectionURLString == nil)
    {
        if(kFeedErrorDisplayingEnabled)
        {
            [self feedStopErrorWithMessage:[NSString stringWithFormat:@"bottom, section%d ad URL is missing from the feed",categoryAdLandscapeIndex]];
        }
        else
        {
            [self feedStopError];
        }
    } 
    else 
    {
        NSURL *URL = [[NSURL alloc] initWithString:bottomAdSectionURLString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL
                                                    cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                                timeoutInterval:kWebServiceTimeoutInterval];
        [URL release];
        categoryAdBottomConnection = [[NSURLConnection alloc] initWithRequest:URLRequest
                                                                     delegate:self       
                                                             startImmediately:YES]; 
    }
}

#pragma mark - Feed Manager
- (void)feedStart {
    [self downloadConfigurationFeed];
}

- (void)feedStopErrorWithMessage:(NSString *)theMessage {
    if([self checkDownloadedUserDefaults])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"There was a problem downloading the feeds! Your feeds may be old. The error is: %@",theMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        [alertView show];
        [alertView release];
        [self createSavedPlist];
        [self createSharePlist];
        [self createFacebookPlist];
        [self copyInteriorStoryCSS];
        
//        [self.window setRootViewController:nil];
        if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
        {
            for (UIView *view in [self.window subviews])
            {
                [view removeFromSuperview];
            }
            //            [self.window addSubview:tabletureViewController.view];
            //[tabletureViewController release];
        }
        else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
        {
            [self.window setRootViewController:nil];
            //            [tabletureViewController release];
        }   
        [feedViewController release];
        feedViewController = nil;
        tabletureViewController = [[TabletureViewController alloc] init];
        if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
        {
            [self.window addSubview:tabletureViewController.view];
            //[tabletureViewController release];
        }
        else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
        {
            [self.window setRootViewController:tabletureViewController];
            [tabletureViewController release];
        }   
    }
    else
    {
        if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
        {
            for (UIView *view in [self.window subviews])
            {
                [view removeFromSuperview];
            }
//            [self.window addSubview:tabletureViewController.view];
            //[tabletureViewController release];
        }
        else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
        {
            [self.window setRootViewController:nil];
//            [tabletureViewController release];
        }   
//        [self.window setRootViewController:nil];
        if (feedViewController) {
            [feedViewController release];
            feedViewController = nil;
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"There was a problem downloading the feeds! You don't have any feeds so the app cannot start. The error is: %@", theMessage] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        [alertView show];
        [alertView release];
    }

}
- (void)feedStopError {
    if([self checkDownloadedUserDefaults])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There was a problem downloading the feeds! Your feeds may be old." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        [alertView show];
        [alertView release];
        [self createSavedPlist];
        [self createSharePlist];
        [self createFacebookPlist];
        [self copyInteriorStoryCSS];
        if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
        {
            for (UIView *view in [self.window subviews])
            {
                [view removeFromSuperview];
            }
            //            [self.window addSubview:tabletureViewController.view];
            //[tabletureViewController release];
        }
        else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
        {
            [self.window setRootViewController:nil];
            //            [tabletureViewController release];
        }   
//        [self.window setRootViewController:nil];
        [feedViewController release];
        feedViewController = nil;
        tabletureViewController = [[TabletureViewController alloc] init];
        if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
        {
            [self.window addSubview:tabletureViewController.view];
            //[tabletureViewController release];
        }
        else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
        {
            [self.window setRootViewController:tabletureViewController];
            [tabletureViewController release];
        }   
    }
    else
    {
        if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
        {
            [tabletureViewController.view removeFromSuperview];
            [tabletureViewController release];
        }
        else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
        {
            [self.window setRootViewController:nil];
            [tabletureViewController release];
        }   
        if (feedViewController) {
            [feedViewController release];
            feedViewController = nil;
        }

        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There was a problem downloading the feeds! You don't have any feeds so the app cannot start." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        [alertView show];
        [alertView release];
    }
}
- (void)feedStop {
    [self createSavedPlist];
    [self createSharePlist];
    [self createFacebookPlist];
    [self createDownloadedUserDefaults];
    [self copyInteriorStoryCSS];

    
    if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
    {
        [feedViewController.view removeFromSuperview];
    }
    else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
    {
        [self.window setRootViewController:nil];
    }
    
    [feedViewController release];
    tabletureViewController = [[TabletureViewController alloc] init];
    
    if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
    {
        [self.window addSubview:tabletureViewController.view];
        //[tabletureViewController release];
    }
    else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
    {
        [self.window setRootViewController:tabletureViewController];
        [tabletureViewController release];
    }   
}

#pragma mark - Download Global Manager
- (void)createSavedPlist {
    BOOL savedPlistExists = [[NSFileManager defaultManager] fileExistsAtPath:kPathSavedPlistPath];
    if(savedPlistExists == NO)
    {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [NSMutableArray array], @"list",
                                           [NSNumber numberWithBool:NO], @"saved", nil];
        [dictionary writeToFile:kPathSavedPlistPath atomically:YES];
    }
}
- (void)createSharePlist {
    BOOL sharePlistExists = [[NSFileManager defaultManager] fileExistsAtPath:kPathSharePlistPath];
    if(sharePlistExists == NO)
    {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           @"NO", @"Facebook",
                                           @"NO", @"Twitter", nil];
        [dictionary writeToFile:kPathSharePlistPath atomically:YES];
    }
}
- (void)createFacebookPlist {
    BOOL facebookPlistExists = [[NSFileManager defaultManager] fileExistsAtPath:kPathFacebookPlistPath];
    if(facebookPlistExists == NO)
    {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           [NSMutableArray array], @"posted", nil];
        [dictionary writeToFile:kPathFacebookPlistPath atomically:YES];
    }
}
- (void)copyInteriorStoryCSS {
    
    NSString *destinationPath = [NSString stringWithFormat:@"%@/%@",kPathDocumentsDirectory,@"interiorStory.css"];
    if([[NSFileManager defaultManager] fileExistsAtPath:destinationPath] == NO)
    {
        NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"interiorStory" ofType:@"css"];
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:nil];
    }
}
#pragma mark - User Defaults
- (BOOL)checkDownloadedUserDefaults {
    return ([[[NSUserDefaults standardUserDefaults] objectForKey:@"feeds"] isEqualToNumber:[NSNumber numberWithBool:YES]]);
}
- (void)createDownloadedUserDefaults
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"feeds"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
#pragma mark - NSURLConnectionDelegate Protocol
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    if(connection == firstWebServiceConnection)
    {
        firstWebServiceData = [[NSMutableData alloc] init];
    }
    if(connection == configurationFeedConnection)
    {
        configurationFeedData = [[NSMutableData alloc] init];
    }
    if(connection == adsFeedConnection)
    {
        adsFeedData = [[NSMutableData alloc] init];
    }
    if(connection == homeFeedConnection)
    {
        homeFeedData = [[NSMutableData alloc] init];
    }
    if(connection == eventsFeedConnection)
    {
        eventsFeedData = [[NSMutableData alloc] init];
    }    
    if(connection == categoryFeedConnection)
    {
        categoryFeedData = [[NSMutableData alloc] init];
    }
    if(connection == homeAdLandscapeConnection)
    {
        homeAdLandscapeData = [[NSMutableData alloc] init];
    }
    if(connection == homeAdPortraitConnection)
    {
        homeAdPortraitData = [[NSMutableData alloc] init];
    }
    if(connection == homeAdBottomConnection)
    {
        homeAdBottomData = [[NSMutableData alloc] init];
    }
    if(connection == categoryAdLandscapeConnection)
    {
        categoryAdLandscapeData = [[NSMutableData alloc] init];
    }
    if(connection == categoryAdPortraitConnection)
    {
        categoryAdPortraitData = [[NSMutableData alloc] init];
    }
    if(connection == categoryAdBottomConnection)
    {
        categoryAdBottomData = [[NSMutableData alloc] init];
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if(connection == firstWebServiceConnection)
    {
        [firstWebServiceData appendData:data];
    }
    if(connection == configurationFeedConnection)
    {
        [configurationFeedData appendData:data];
    }
    if(connection == adsFeedConnection)
    {
        [adsFeedData appendData:data];
    }
    if(connection == homeFeedConnection)
    {
        [homeFeedData appendData:data];
    }
    if(connection == eventsFeedConnection)
    {
        [eventsFeedData appendData:data];
    }
    if(connection == categoryFeedConnection)
    {
        [categoryFeedData appendData:data];
    }
    if(connection == homeAdLandscapeConnection)
    {
        [homeAdLandscapeData appendData:data];

    }
    if(connection == homeAdPortraitConnection)
    {
        [homeAdPortraitData appendData:data];

    }
    if(connection == homeAdBottomConnection)
    {
        [homeAdBottomData appendData:data];

    }
    if(connection == categoryAdLandscapeConnection)
    {
        [categoryAdLandscapeData appendData:data];

    }
    if(connection == categoryAdPortraitConnection)
    {
        [categoryAdPortraitData appendData:data];

    }
    if(connection == categoryAdBottomConnection)
    {
        [categoryAdBottomData appendData:data];

    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    //NSLog(@"%@",[error localizedDescription]);
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if(connection == firstWebServiceConnection)
    {
        if(firstWebServiceData != nil)
        {
            [firstWebServiceData release];
            firstWebServiceData = nil;
        }
        NSLog(@"connection failed in firstWebServiceConnection");
    }
    if(connection == configurationFeedConnection)
    {
        if(configurationFeedData != nil)
        {
            [configurationFeedData release];
            configurationFeedData = nil;
        }        
        NSLog(@"connection failed in configurationFeedConnection");
    }
    if(connection == adsFeedConnection)
    {
        if(adsFeedData != nil)
        {
            [adsFeedData release];
            adsFeedData = nil;
        }  
        NSLog(@"connection failed in adsFeedConnection");
    }
    if(connection == homeFeedConnection)
    {
        if(homeFeedData != nil)
        {
            [homeFeedData release];
            homeFeedData = nil;
        }
        NSLog(@"connection failed in homeFeedConnection");
    }
    if(connection == eventsFeedConnection)
    {
        if(eventsFeedData != nil)
        {
            [eventsFeedData release];
            eventsFeedData = nil;
        }
        NSLog(@"connection failed in eventsFeedConnection");
    }
    if(connection == categoryFeedConnection)
    {
        if(categoryFeedData != nil)
        {
            [categoryFeedData release];
            categoryFeedData = nil;
        }
        NSLog(@"connection failed in categoryFeedConnection");
    }
    if(connection == homeAdLandscapeConnection)
    {
        if(homeAdLandscapeData != nil)
        {
            [homeAdLandscapeData release];
            homeAdLandscapeData = nil;
        }
        NSLog(@"connection failed in homeAdLandscapeConnection");
    }
    if(connection == homeAdPortraitConnection)
    {
        if(homeAdPortraitData != nil)
        {
            [homeAdPortraitData release];
            homeAdPortraitData = nil;
        }
        NSLog(@"connection failed in homeAdPortraitConnection");
    }
    if(connection == homeAdBottomConnection)
    {
        if(homeAdBottomData != nil)
        {
            [homeAdBottomData release];
            homeAdBottomData = nil;
        }
        NSLog(@"connection failed in homeAdBottomConnection");
    }
    if(connection == categoryAdLandscapeConnection)
    {
        if(categoryAdLandscapeData != nil)
        {
            [categoryAdLandscapeData release];
            categoryAdLandscapeData = nil;
        }
        NSLog(@"connection failed in categoryAdLandscapeConnection");
    }
    if(connection == categoryAdPortraitConnection)
    {
        if(categoryAdPortraitData != nil)
        {
            [categoryAdPortraitData release];
            categoryAdPortraitData = nil;
        }
        NSLog(@"connection failed in categoryAdPortraitConnection");
    }
    if(connection == categoryAdBottomConnection)
    {
        if(categoryAdBottomData != nil)
        {
            [categoryAdBottomData release];
            categoryAdBottomData = nil;
        }
        NSLog(@"connection failed in categoryAdBottomConnection");
    }
    [connection release];
    [self feedStopError];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    if (connection == firstWebServiceConnection)
    {
        NSString *webServiceConfig = [[NSString alloc] initWithData:firstWebServiceData encoding:NSUTF8StringEncoding];
        //NSLog(@"%@", webServiceConfig);
        NSDictionary *config = [webServiceConfig JSONValue];
        [config writeToFile:kPathConfigurationPlistPath atomically:YES];
        [webServiceConfig release];
        if(kFeedEnabled)
        {
            categoryIndex = 0;
            categoryAdLandscapeIndex = categoryAdPortraitIndex = categoryAdBottomIndex = 1;    
            [self feedStart];
        }
        else
        {
            [self feedStop];
        }
    }
    
    if(connection == configurationFeedConnection)
    {
        NSString *configurationString = [[NSString alloc] initWithData:configurationFeedData 
                                                               encoding:NSUTF8StringEncoding];
        [configurationFeedData release];
        configurationFeedData = nil;
        NSDictionary *configurationJSONResponse = [[NSDictionary alloc] initWithDictionary:[configurationString JSONValue]];
        [configurationString release];
        BOOL isWrite = [configurationJSONResponse writeToFile:kPathConfigurationPlistPath 
                                    atomically:YES];
        NSLog(@"Write configFeed = %i", isWrite);

        
        
        

        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]];
        if([[dictionary objectForKey:@"appIsFree"] boolValue] == YES)
        {
            [self startWithForm:NO];
        }
        else
        {
            NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
            BOOL productWasBought = [[userDefaults objectForKey:@"aProductWasBought"] boolValue];
            if (productWasBought)
            {
                //take date from service
                NSDateFormatter *rfc3339DateFormatter = [[[NSDateFormatter alloc] init] autorelease];
                
                NSString *dateString = [[configurationJSONResponse objectForKey:@"currendDay"] stringByReplacingOccurrencesOfString:@"+00:00" withString:@""];
                [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss"];
                
                NSDate *dateFromServer = [rfc3339DateFormatter dateFromString:dateString];
                /////////////
                
               
                int period = [[userDefaults objectForKey:@"Period"] intValue];
                
                NSDate *buyingDate = [userDefaults objectForKey:@"buyDate"];
                
                NSDateComponents *comp = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:buyingDate];
                NSLog(@"buying date day %d month %d and year %d",[comp day],[comp month],[comp year]);
                comp = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:dateFromServer];
                NSLog(@"date from server day %d month %d and year %d",[comp day],[comp month],[comp year]);
                
                
                NSDate *date = [[NSDate alloc] initWithTimeInterval:-60*60*24*period sinceDate:dateFromServer]; 
                
                comp = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
                NSLog(@"date to compare day %d month %d and year %d",[comp day],[comp month],[comp year]);
                
                switch ([buyingDate compare:date]) {
                    case NSOrderedAscending:
                    {
                        NSLog(@"ascending YES");
                        [userDefaults removeObjectForKey:@"aProductWasBought"];
                        [self startWithForm:YES];
                    }
                        break;
                    case NSOrderedDescending:
                    {
                        NSLog(@"descending NO");
                        [self startWithForm:NO];
                    }
                        break;
                    case NSOrderedSame:
                    {
                        NSLog(@"same NO");
                        [self startWithForm:NO];
                    }
                        break;
                    default:
                        break;
                }
               // }
                [date release];
            }
            else
            {
                [self startWithForm:YES];
            }
        }
        [self downloadAdsFeed]; 
        [configurationJSONResponse release];
    }
    
    if(connection == adsFeedConnection)
    {
        NSString *adsString = [[NSString alloc] initWithData:adsFeedData 
                                                     encoding:NSUTF8StringEncoding];
        [adsFeedData release];
        adsFeedData = nil;
        self.adsJSONResponse = [[NSDictionary alloc] initWithDictionary:[adsString JSONValue]];
        
        [adsString release];
        BOOL isWrite = [adsJSONResponse writeToFile:kPathAdsPlistPath 
                          atomically:YES];
        NSLog(@"Write adsFeed = %i", isWrite);
        [adsJSONResponse release];

        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        [self downloadHomeFeed];
    }
    if(connection == homeFeedConnection)
    {
        NSString *homeString = [[NSString alloc] initWithData:homeFeedData 
                                                      encoding:NSUTF8StringEncoding];
        [homeFeedData release];
        homeFeedData = nil;
        NSDictionary *homeJSONResponse = [[NSDictionary alloc] initWithDictionary:[homeString JSONValue]];
        [homeString release];
        //NSLog(@"%@", kPathHomePlistPath);
        BOOL isWrite = [homeJSONResponse writeToFile:kPathHomePlistPath atomically:YES];  
        NSLog(@"Write homeFeed = %i", isWrite);
        [homeJSONResponse release];

        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        [self downloadEventsFeed];
    }
    if(connection == eventsFeedConnection)
    {
        NSString *eventsString = [[NSString alloc] initWithData:eventsFeedData 
                                                        encoding:NSUTF8StringEncoding];
        [eventsFeedData release];
        eventsFeedData = nil;
        NSDictionary *eventsJSONResponse = [[NSDictionary alloc] initWithDictionary:[eventsString JSONValue]];
        [eventsString release];
        BOOL isWrite = [eventsJSONResponse writeToFile:kPathEventsPlistPath atomically:YES]; 
        NSLog(@"Write eventFeed = %i", isWrite);
        
        [eventsJSONResponse release];

        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        [self downloadCategoriesFeed];
    }
    if(connection == categoryFeedConnection)
    {
        NSString *categoryString = [[NSString alloc] initWithData:categoryFeedData
                                                          encoding:NSUTF8StringEncoding];
        [categoryFeedData release];
        categoryFeedData = nil;
        NSDictionary *categoryJSONResponse = [[NSDictionary alloc] initWithDictionary:[categoryString JSONValue]];
        [categoryString release];
        BOOL isWrite = [categoryJSONResponse writeToFile:[NSString stringWithFormat:@"%@/feeds/section_%d.plist",kPathDocumentsDirectory,categoryIndex] 
                               atomically:YES];
        NSLog(@"Write category%iFeed = %i", categoryIndex, isWrite);
        [categoryJSONResponse release];
        

        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        categoryIndex++;
        NSDictionary *configurationDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath]; 
        int categoriesNumber = [[configurationDictionary objectForKey:@"categoriesNumber"] intValue];
        if(categoryIndex < categoriesNumber)
        {
            [self downloadCategoriesFeed];
        }
        else
        {
            [self downloadHomeAdLandscape];
        }        
    }
    if(connection == homeAdLandscapeConnection)
    {
        NSString *landscapeAdPath = [NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_landscape.jpg"];
        [homeAdLandscapeData writeToFile:landscapeAdPath 
                              atomically:YES];
        NSLog(@"HomeAd Landscape = %@", landscapeAdPath);
        [homeAdLandscapeData release];
        homeAdLandscapeData = nil;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        

        [self downloadHomeAdPortrait];
    }
    if(connection == homeAdPortraitConnection)
    {
        NSString *portraitAdPath = [NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_portrait.jpg"];
        [homeAdPortraitData writeToFile:portraitAdPath 
                             atomically:YES];
        NSLog(@"HomeAd portait = %@", portraitAdPath);
        [homeAdPortraitData release];
        homeAdPortraitData = nil;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        [self downloadHomeAdBottom];
    }
    if(connection == homeAdBottomConnection)
    {
        NSString *bottomAdPath = [NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_bottom.jpg"];
        [homeAdBottomData writeToFile:bottomAdPath
                           atomically:YES];
        NSLog(@"HomeAd Bottom = %@", bottomAdPath);
        [homeAdBottomData release];
        homeAdBottomData = nil;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        [self downloadCategoryAdsLandscape];
    }
    if(connection == categoryAdLandscapeConnection)
    {
        NSString *landscapeAdSectionURLPath = [NSString stringWithFormat:@"%@/ads/section_%d_landscape.jpg",kPathDocumentsDirectory,categoryAdLandscapeIndex - 1];

        [categoryAdLandscapeData writeToFile:landscapeAdSectionURLPath
                                  atomically:YES];
        NSLog(@"CategoryAd Lanscape = %@", landscapeAdSectionURLPath);
        [categoryAdLandscapeData release];
        categoryAdLandscapeData = nil;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
        
        NSDictionary *sectionsAdsDictionary = [adsDictionary objectForKey:@"sections"];
        int totalSections = [[sectionsAdsDictionary allKeys] count];
        categoryAdLandscapeIndex++;
        if(categoryAdLandscapeIndex - 1 < totalSections)
        {
            [self downloadCategoryAdsLandscape];
        }
        else
        {
            [self downloadCategoryAdsPortrait];
        }
    }
    if(connection == categoryAdPortraitConnection)
    {
        NSString *portraitAdSectionURLPath = [NSString stringWithFormat:@"%@/ads/section_%d_portrait.jpg",kPathDocumentsDirectory,categoryAdPortraitIndex - 1];
        [categoryAdPortraitData writeToFile:portraitAdSectionURLPath
                                atomically:YES];
        NSLog(@"CategoryAd Portait = %@", portraitAdSectionURLPath);
        [categoryAdPortraitData release];
        categoryAdPortraitData = nil;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
        
        NSDictionary *sectionsAdsDictionary = [adsDictionary objectForKey:@"sections"];
        int totalSections = [[sectionsAdsDictionary allKeys] count];
        categoryAdPortraitIndex++;
        if(categoryAdPortraitIndex - 1 < totalSections)
        {
            [self downloadCategoryAdsPortrait];
        }
        else
        {
            [self downloadCategoryAdsBottom];
        }
    }
    if(connection == categoryAdBottomConnection)
    {
        NSString *bottomAdSectionURLPath = [NSString stringWithFormat:@"%@/ads/section_%d_bottom.jpg",kPathDocumentsDirectory,categoryAdBottomIndex - 1];
        
        /*NSString *url = [NSString stringWithFormat:@"%@",connection];
        NSArray *content = [url componentsSeparatedByString:@"/"];
        NSString *name = [content objectAtIndex:[content count]-1];
        NSString *bottomAdSectionURLPath = [NSString stringWithFormat:@"%@/%@",kPathDocumentsDirectory,name];*/
        
        [categoryAdBottomData writeToFile:bottomAdSectionURLPath
                               atomically:YES];
        NSLog(@"CategoryAd Bottom = %@", bottomAdSectionURLPath);
        [categoryAdBottomData release];
        categoryAdBottomData = nil;
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
        
        NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
        
        NSDictionary *sectionsAdsDictionary = [adsDictionary objectForKey:@"sections"];
        int totalSections = [[sectionsAdsDictionary allKeys] count];
        categoryAdBottomIndex++;
        if(categoryAdBottomIndex - 1 < totalSections)
        {
            [self downloadCategoryAdsBottom];
        }
        else
        {
            [self feedStop];
        }
    }
}
- (NSCachedURLResponse *)connection:(NSURLConnection *)theConnection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

- (void) resetApp
{
    categoryIndex = 0;
    categoryAdLandscapeIndex = categoryAdPortraitIndex = categoryAdBottomIndex = 1;
}


@end
