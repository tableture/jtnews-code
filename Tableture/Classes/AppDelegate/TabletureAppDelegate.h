#import "TabletureViewController.h"
#import "FeedViewController.h"
#import "Facebook.h"

@interface TabletureAppDelegate : NSObject <UIApplicationDelegate> {
    TabletureViewController *tabletureViewController;
    FeedViewController *feedViewController;
    
    NSURLConnection *configurationFeedConnection;
    NSMutableData *configurationFeedData;
    
    NSURLConnection *adsFeedConnection;
    NSMutableData *adsFeedData;
    
    NSURLConnection *homeFeedConnection;
    NSMutableData *homeFeedData;
    
    NSURLConnection *eventsFeedConnection;
    NSMutableData *eventsFeedData;
    
    NSURLConnection *categoryFeedConnection;
    NSMutableData *categoryFeedData;
    int categoryIndex;
    
    NSURLConnection *homeAdLandscapeConnection;
    NSMutableData *homeAdLandscapeData;
    
    NSURLConnection *homeAdPortraitConnection;
    NSMutableData *homeAdPortraitData;
    
    NSURLConnection *homeAdBottomConnection;
    NSMutableData *homeAdBottomData;
    
    NSURLConnection *categoryAdLandscapeConnection;
    NSMutableData *categoryAdLandscapeData;
    int categoryAdLandscapeIndex;
    
    NSURLConnection *categoryAdPortraitConnection;
    NSMutableData *categoryAdPortraitData;
    int categoryAdPortraitIndex;
    
    NSURLConnection *categoryAdBottomConnection;
    NSMutableData *categoryAdBottomData;
    int categoryAdBottomIndex;
    
    NSURLConnection *firstWebServiceConnection;
    NSMutableData *firstWebServiceData;
    
    
    //Facebook
    Facebook *_facebook;

#ifdef DEBUG
    
#endif
}

#pragma mark - Properties
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) TabletureViewController *tabletureViewController;
@property (nonatomic, retain) Facebook *facebook;
@property (nonatomic, retain) NSDictionary *adsJSONResponse;

//@property (nonatomic, retain) NSArray *productsArrayFromConfigurationFeed;

#pragma mark - Feed Methods
- (void)downloadConfigurationFeed;
- (void)downloadAdsFeed;
- (void)downloadHomeFeed;
- (void)downloadEventsFeed;
- (void)downloadCategoriesFeed;

#pragma mark - Images Methods
- (void)downloadHomeAdLandscape;
- (void)downloadHomeAdPortrait;
- (void)downloadHomeAdBottom;
- (void)downloadCategoryAdsLandscape;
- (void)downloadCategoryAdsPortrait;
- (void)downloadCategoryAdsBottom;

#pragma mark - Feed Manager
- (void)feedStart;
- (void)feedStop;
- (void)feedStopError;
- (void)feedStopErrorWithMessage:(NSString *)theMessage;

#pragma mark - Download Global Manager
- (void)createSavedPlist;
- (void)createSharePlist;
- (void)createFacebookPlist;
- (void)copyInteriorStoryCSS;

#pragma mark - User Defaults
- (BOOL)checkDownloadedUserDefaults;
- (void)createDownloadedUserDefaults;

#pragma mark - Conditional Start
- (void)startWithForm:(BOOL)yesOrNo;

- (void) resetApp;
@end
