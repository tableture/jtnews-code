#import "Subscription.h"

@interface SubscriptionViewController : UIViewController <
    UITextFieldDelegate,
    UIPickerViewDelegate,
    UIPickerViewDataSource,
    UIActionSheetDelegate,
    UIAlertViewDelegate,
    SubscriptionDelegate> {
    
        IBOutlet UILabel *titleLabel;
        IBOutlet UILabel *firstNameLabel;
        IBOutlet UITextField *firstNameTextField;
        IBOutlet UILabel *lastNameLabel;
        IBOutlet UITextField *lastNameTextField;
        IBOutlet UILabel *addressLabel;
        IBOutlet UITextField *addressTextField;
        IBOutlet UILabel *cityLabel;
        IBOutlet UITextField *cityTextField;
        IBOutlet UILabel *stateLabel;
        IBOutlet UITextField *stateTextField;
        IBOutlet UILabel *zipLabel;
        IBOutlet UITextField *zipTextField;
        IBOutlet UILabel *emailLabel;
        IBOutlet UITextField *emailTextField;
        IBOutlet UILabel *phoneLabel;
        IBOutlet UITextField *phoneTextField;
        IBOutlet UILabel *paymentLabel;
        IBOutlet UITextField *paymentTextField;
        IBOutlet UILabel *couponLabel;
        IBOutlet UITextField *couponTextField;
        IBOutlet UILabel *requiredLabel;
        IBOutlet UIButton *submitButton;            
        
        IBOutlet UIView *loadingView;
        IBOutlet UIButton *cancelButton;
        
        IBOutlet UIButton *paymentPlanButton;
        NSMutableArray *products;
        
        UIPickerView *pickerView;
        
        Subscription *subscription;   
        
        NSURLConnection *createSubscriptionURLConnection;
        NSMutableData *createSubscriptionData;
                     
        BOOL productBought;
        IBOutlet UIActivityIndicatorView *spinner;
        
        BOOL purposeIsToUpdate;
        
        NSURLConnection *updateSubscriptionURLConnection;
        NSMutableData *updateSubscriptionData;
        UIButton *choosePaymentPlan;
        
        BOOL errorAlreadyAppeared;
}

#pragma mark - Properties

@property (nonatomic, assign) BOOL purposeIsToUpdate;
@property (nonatomic, retain) NSMutableArray *products;
@property (nonatomic, retain) UIPickerView *pickerView;

#pragma mark - IBActions
- (IBAction)submitButtonPressed;
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)choosePaymentPlan:(id)sender;

#pragma mark - Loading Screen
- (void)showLoading;
- (void)hideLoading;

#pragma mark - Payment Notifications
- (void)transactionFailed:(NSNotification *)note;
- (void)transactionRestored:(NSNotification *)note;
- (void)transactionPurchased:(NSNotification *)note;
- (void)transactionPurchasing:(NSNotification *)note;

#pragma mark - Subscription Webservices
-(void) updateSubscription;
- (void)createSubscription;
@end
