#import "TopStoryViewHome.h"
#import "NewsView.h"
#import "NewestNewsCategoryView.h"
#import "SecondNewsView.h"
#import "MiddleAdView.h"
#import "ComingEventsView.h"
#import "SavedStoriesView.h"
#import "BottomAdView.h"

@interface HomeViewController : UIViewController {    
    IBOutlet UIScrollView *homeScrollView;
    
    UILabel *topTitle;
    AsyncImageView *topStoryImage;    
    TopStoryViewHome *topStoryView;
    
    NewsView *newsView1;
    NewsView *newsView2;
    NewsView *newsView3;
    
    NewestNewsCategoryView *newestNewsCategoryView1;
    NewestNewsCategoryView *newestNewsCategoryView2;

    SecondNewsView *secondNewsView;
    
    MiddleAdView *middleAdView;
    
    ComingEventsView *comingEventsView;
    SavedStoriesView *savedStoriesView;
    
    BottomAdView *bottomAdView;
    
    BOOL didAddedBorders;
    
    BOOL bottomAdIsUp;
}

@property(nonatomic, assign) BOOL bottomAdIsUp;
#pragma mark - Saved Articles Changed
- (void)updateInterfaceForSavedArticles; 

@end
