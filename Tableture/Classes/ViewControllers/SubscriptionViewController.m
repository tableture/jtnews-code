#import "SubscriptionViewController.h"
#import "Constants.h"
#import "JSON.h"

@implementation SubscriptionViewController

#pragma mark - Properties
@synthesize pickerView;
@synthesize products;
@synthesize purposeIsToUpdate;

#pragma mark - Memory Management
- (void)dealloc {
    [titleLabel release];
    [firstNameLabel release];
    [firstNameTextField release];
    [lastNameLabel release];
    [lastNameTextField release];
    [addressLabel release];
    [addressTextField release];
    [cityLabel release];
    [cityTextField release];
    [stateLabel release];
    [stateTextField release];
    [zipLabel release];
    [zipTextField release];
    [emailLabel release];
    [emailTextField release];
    [phoneLabel release];
    [phoneTextField release];
    [paymentLabel release];
    [paymentTextField release];
    [couponLabel release];
    [couponTextField release];
    [requiredLabel release];
    [submitButton release];
    [loadingView release];
    [spinner release];
    [cancelButton release];
    [paymentPlanButton release];
    [super dealloc];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewDidUnload {
    [titleLabel release];
    titleLabel = nil;
    [firstNameLabel release];
    firstNameLabel = nil;
    [firstNameTextField release];
    firstNameTextField = nil;
    [lastNameLabel release];
    lastNameLabel = nil;
    [lastNameTextField release];
    lastNameTextField = nil;
    [addressLabel release];
    addressLabel = nil;
    [addressTextField release];
    addressTextField = nil;
    [cityLabel release];
    cityLabel = nil;
    [cityTextField release];
    cityTextField = nil;
    [stateLabel release];
    stateLabel = nil;
    [stateTextField release];
    stateTextField = nil;
    [zipLabel release];
    zipLabel = nil;
    [zipTextField release];
    zipTextField = nil;
    [emailLabel release];
    emailLabel = nil;
    [emailTextField release];
    emailTextField = nil;
    [phoneLabel release];
    phoneLabel = nil;
    [phoneTextField release];
    phoneTextField = nil;
    [paymentLabel release];
    paymentLabel = nil;
    [paymentTextField release];
    paymentTextField = nil;
    [couponLabel release];
    couponLabel = nil;
    [couponTextField release];
    couponTextField = nil;
    [requiredLabel release];
    requiredLabel = nil;
    [submitButton release];
    submitButton = nil;
    [loadingView release];
    loadingView = nil;
    [spinner release];
    spinner = nil;
    [cancelButton release];
    cancelButton = nil;
    [paymentPlanButton release];
    paymentPlanButton = nil;
    [super viewDidUnload];
}

#pragma mark - View Lifecycle

-(void) viewWillDisappear:(BOOL)animated
{
    [Subscription resetSharedSubscription];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(transactionFailed:) 
                                                 name:SKTransactionFailed 
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(transactionRestored:) 
                                                 name:SKTransactionRestored 
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(transactionPurchased:) 
                                                 name:SKTransactionPurchased 
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(transactionPurchasing:) 
                                                 name:SKTransactionPurchasing 
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    productBought = NO;
    if ([self purposeIsToUpdate]) 
    {
        cancelButton.alpha = 1;
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
       firstNameTextField.text =  [standardUserDefaults objectForKey:@"FirstName"];
       lastNameTextField.text =  [standardUserDefaults objectForKey: @"LastName"];
       addressTextField.text = [standardUserDefaults objectForKey: @"Address"];
       cityTextField.text = [standardUserDefaults objectForKey: @"City"];
       stateTextField.text = [standardUserDefaults objectForKey: @"State"];
       zipTextField.text = [standardUserDefaults objectForKey: @"ZIP"];
       emailTextField.text = [standardUserDefaults objectForKey: @"Email"];
       phoneTextField.text = [standardUserDefaults objectForKey: @"Phone"];
       couponTextField.text = [standardUserDefaults objectForKey: @"Coupon"];
        
    }
    self.navigationController.navigationBarHidden = YES;
    NSLog(@"%@", ApplicationDelegate.window.subviews);
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    errorAlreadyAppeared = NO;
    
    [Subscription resetSharedSubscription];
    subscription = [Subscription sharedSubscription];
    [subscription setDelegate:self];
    [self showLoading];
    [subscription getProducts];
    
}

#pragma mark - Orientation Support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	//if(UIInterfaceOrientationIsPortrait(interfaceOrientation))
    [self.view endEditing:YES];
        return YES;
    //else return NO;
   // return (UIInterfaceOrientationIsPortrait(interfaceOrientation));
    //return NO;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation 
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        [titleLabel setFrame:CGRectMake(380, 45, 270, 45)];
        
        const float kTopPadding = 60;
        const float kLeftPadding = 150;
        const float kRequiredPadding = 150;
        
        [firstNameLabel setFrame:CGRectMake(67 + kLeftPadding, 242 - kTopPadding, 297, 24)];
        [lastNameLabel setFrame:CGRectMake(67 + kLeftPadding, 282 - kTopPadding, 297, 24)];
        [addressLabel setFrame:CGRectMake(67 + kLeftPadding, 322 - kTopPadding, 297, 24)];
        [cityLabel setFrame:CGRectMake(67 + kLeftPadding, 362 - kTopPadding, 297, 24)];
        [stateLabel setFrame:CGRectMake(67 + kLeftPadding, 402 - kTopPadding, 297, 24)];
        [zipLabel setFrame:CGRectMake(67 + kLeftPadding, 442 - kTopPadding, 297, 24)];
        [emailLabel setFrame:CGRectMake(67 + kLeftPadding, 482 - kTopPadding, 297, 24)];
        [phoneLabel setFrame:CGRectMake(67 + kLeftPadding, 522 - kTopPadding, 297, 24)];
        [paymentLabel setFrame:CGRectMake(67 + kLeftPadding, 562 - kTopPadding, 297, 24)];
        [couponLabel setFrame:CGRectMake(67 + kLeftPadding, 602 - kTopPadding, 297, 24)];
        [requiredLabel setFrame:CGRectMake(67 + kLeftPadding, 822 - kTopPadding - kRequiredPadding, 297, 24)];
        
        [firstNameTextField setFrame:CGRectMake(400 + kLeftPadding, 238 - kTopPadding, 286, 31)];        
        [lastNameTextField setFrame:CGRectMake(400 + kLeftPadding, 278 - kTopPadding, 286, 31)];
        [addressTextField setFrame:CGRectMake(400 + kLeftPadding, 318 - kTopPadding, 286, 31)];
        [cityTextField setFrame:CGRectMake(400 + kLeftPadding, 358 - kTopPadding, 286, 31)];
        [stateTextField setFrame:CGRectMake(400 + kLeftPadding, 398 - kTopPadding, 286, 31)];
        [zipTextField setFrame:CGRectMake(400 + kLeftPadding, 438 - kTopPadding, 286, 31)];
        [emailTextField setFrame:CGRectMake(400 + kLeftPadding, 478 - kTopPadding, 286, 31)];
        [phoneTextField setFrame:CGRectMake(400 + kLeftPadding, 518 - kTopPadding, 286, 31)];
        [paymentTextField setFrame:CGRectMake(400 + kLeftPadding, 558 - kTopPadding, 286, 31)];
        [couponTextField setFrame:CGRectMake(400 + kLeftPadding, 598 - kTopPadding, 286, 31)];
        
        [submitButton setFrame:CGRectMake(450, 650, 152, 37)];
    }
    else
    {
        [titleLabel setFrame:CGRectMake(249, 45, 270, 45)];
        
        [firstNameLabel setFrame:CGRectMake(67, 242, 297, 24)];
        [lastNameLabel setFrame:CGRectMake(67, 282, 297, 24)];
        [addressLabel setFrame:CGRectMake(67, 322, 297, 24)];
        [cityLabel setFrame:CGRectMake(67, 362, 297, 24)];
        [stateLabel setFrame:CGRectMake(67, 402, 297, 24)];
        [zipLabel setFrame:CGRectMake(67, 442, 297, 24)];
        [emailLabel setFrame:CGRectMake(67, 482, 297, 24)];
        [phoneLabel setFrame:CGRectMake(67, 522, 297, 24)];
        [paymentLabel setFrame:CGRectMake(67, 562, 297, 24)];
        [couponLabel setFrame:CGRectMake(67, 602, 297, 24)];
        [requiredLabel setFrame:CGRectMake(67, 822, 297, 24)];
        
        [firstNameTextField setFrame:CGRectMake(400, 238, 286, 31)];        
        [lastNameTextField setFrame:CGRectMake(400, 278, 286, 31)];
        [addressTextField setFrame:CGRectMake(400, 318, 286, 31)];
        [cityTextField setFrame:CGRectMake(400, 358, 286, 31)];
        [stateTextField setFrame:CGRectMake(400, 398, 286, 31)];
        [zipTextField setFrame:CGRectMake(400, 438, 286, 31)];
        [emailTextField setFrame:CGRectMake(400, 478, 286, 31)];
        [phoneTextField setFrame:CGRectMake(400, 518, 286, 31)];
        [paymentTextField setFrame:CGRectMake(400, 558, 286, 31)];
        [couponTextField setFrame:CGRectMake(400, 598, 286, 31)];
        
        [submitButton setFrame:CGRectMake(308, 873, 152, 37)];
    }
}

#pragma mark - UIPickerViewDataSource Protocol
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [products count];
}

#pragma mark - UIPickerViewDelegate Protocol
- (NSString *)pickerView:(UIPickerView *)pickerView 
             titleForRow:(NSInteger)row 
            forComponent:(NSInteger)component {
    SKProduct *aSKProduct = [products objectAtIndex:row];
    
	return [NSString stringWithFormat:@"%@ - %@",[aSKProduct localizedTitle],[Subscription priceForProductWithIdentifier:[aSKProduct productIdentifier]]];
}
- (UIView *)pickerView:(UIPickerView *)thePickerView 
            viewForRow:(NSInteger)row 
          forComponent:(NSInteger)component 
           reusingView:(UIView *)view {
    
	UILabel *aLabel = [[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 260, 44)] autorelease];	
	aLabel.textAlignment = UITextAlignmentCenter;
	aLabel.backgroundColor = [UIColor clearColor];
	aLabel.textColor = [UIColor blackColor];
	aLabel.font = [UIFont systemFontOfSize:14];
	aLabel.text = [self pickerView:thePickerView 
                       titleForRow:row 
                      forComponent:component];   
	return aLabel;
}

#pragma mark - UITextViewDelegate Protocol
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    /*if (textField == paymentTextField){
        [textField resignFirstResponder];
        
        if([products count])
        {
            UIActionSheet *menuActionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                                              delegate:self
                                                     cancelButtonTitle:nil
                                                destructiveButtonTitle:nil
                                                     otherButtonTitles:@"Done",nil];
            
            pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 280, 216)];
            [pickerView setDelegate:self];
            [pickerView setShowsSelectionIndicator:YES];
            
            [menuActionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
            [menuActionSheet showFromRect:paymentTextField.frame 
                                   inView:self.view 
                                 animated:YES];
            [menuActionSheet showInView:self.view];
            [menuActionSheet addSubview:self.pickerView];        
            [menuActionSheet release];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Products list is empty" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            [alertView release];
        }
    }*/
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - alert view protocol

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex 
{
    [self cancelButtonPressed:nil];
}

#pragma mark - UIActionSheetDelegate Protocol
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    paymentTextField.text = [self pickerView:pickerView 
                                 titleForRow:[pickerView selectedRowInComponent:0] 
                                forComponent:0];
    [paymentTextField becomeFirstResponder];
}

#pragma mark - SubscriptionDelegate Protocol
- (void)subscriptionDelegateDidFinishSearchingProducts:(Subscription *)theSubscription withError:(BOOL)YESorNO
{
    [self hideLoading];
    if(YESorNO == NO)
    {
        products = [theSubscription.validProducts mutableCopy];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The products could not have been received from Apple" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
    }
}


#pragma mark - IBActions
- (IBAction)submitButtonPressed {
    submitButton.enabled = NO;

    if([products count]==0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Products list is empty" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
        submitButton.enabled = YES;
    }    
    else
    {
    
        if ([firstNameTextField.text isEqualToString:@""])
        {
            [Utilities showOkAlertwithTitle:@"Error" andMessage:@"First name field is empty"];
            submitButton.enabled = YES;
            return;
        }
        if ([lastNameTextField.text isEqualToString:@""])
        {
            [Utilities showOkAlertwithTitle:@"Error" andMessage:@"Last name field is empty"];
            submitButton.enabled = YES;
            return;
        }
        if ([addressTextField.text isEqualToString:@""])
        {
            [Utilities showOkAlertwithTitle:@"Error" andMessage:@"Address field is empty"];
            submitButton.enabled = YES;
            return;
        }
        if ([cityTextField.text isEqualToString:@""])
        {
            [Utilities showOkAlertwithTitle:@"Error" andMessage:@"City field is empty"];
            submitButton.enabled = YES;
            return;
        }
        if ([stateTextField.text isEqualToString:@""])
        {
            [Utilities showOkAlertwithTitle:@"Error" andMessage:@"State field is empty"];
            submitButton.enabled = YES;
            return;
        }
        if ([zipTextField.text isEqualToString:@""])
        {
            [Utilities showOkAlertwithTitle:@"Error" andMessage:@"ZIP field is empty"];
            submitButton.enabled = YES;
            return;
        }
        if ([emailTextField.text isEqualToString:@""])
        {
            [Utilities showOkAlertwithTitle:@"Error" andMessage:@"Email field is empty"];
            submitButton.enabled = YES;
            return;
        }
        else
        {
            if (![Utilities validateEmail:emailTextField.text])
            {
                [Utilities showOkAlertwithTitle:@"Error" andMessage:@"Please enter a valid email address"];
                submitButton.enabled = YES;
                return;
            }
        }
        
        if([products count]==0)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Products list is empty" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            [alertView release];
            submitButton.enabled = YES;
            return;
        }
        
        if([paymentTextField.text length]==0)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No product selected" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            [alertView release];
            
            submitButton.enabled = YES;
            return;
        }

        [spinner startAnimating];
        
        if (purposeIsToUpdate)
        {
            [subscription makePaymentForIdentifier:[[products objectAtIndex:[pickerView selectedRowInComponent:0]] productIdentifier]];
            return;
        }
        
        if(productBought)
        {               
            [self createSubscription];
        }
        else
        {
            [subscription makePaymentForIdentifier:[[products objectAtIndex:[pickerView selectedRowInComponent:0]] productIdentifier]];
        }
    }
}

- (IBAction)cancelButtonPressed:(id)sender
{
    /*if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SubscriptionViewWasRemoved" object:nil];
        [createSubscriptionURLConnection cancel];
        [subscription cancelRequest];
        [subscription setDelegate:nil];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDidStopSelector:@selector(removeFromSuperview)];
        [UIView setAnimationDelegate:self.view];
        
        self.view.alpha = 0;
        
        [UIView commitAnimations];
    }
    else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
    {
        [[ApplicationDelegate window] setRootViewController:[ApplicationDelegate tabletureViewController]];  
    }*/
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)choosePaymentPlan:(id)sender 
{
    if([products count])
    {
        UIActionSheet *menuActionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                                                                     delegate:self
                                                            cancelButtonTitle:nil
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:@"Done",nil];
        
        pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 280, 216)];
        [pickerView setDelegate:self];
        [pickerView setShowsSelectionIndicator:YES];
        
        [menuActionSheet setActionSheetStyle:UIActionSheetStyleBlackOpaque];
        [menuActionSheet showFromRect:paymentTextField.frame 
                               inView:self.view 
                             animated:YES];
        [menuActionSheet showInView:self.view];
        [menuActionSheet addSubview:self.pickerView];        
        [menuActionSheet release];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Products list is empty" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        [alertView release];
    }

}

#pragma mark - Loading Screen
- (void)showLoading {
    [loadingView setHidden:NO];
}
- (void)hideLoading {
    [loadingView setHidden:YES];
}

#pragma mark - Payment Notifications
- (void)transactionFailed:(NSNotification *)note {
    if (!errorAlreadyAppeared)
    {
        submitButton.enabled = YES;
        [spinner stopAnimating];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"There were some problems with the transaction. Please try again later!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
        errorAlreadyAppeared = YES;
    }

}
- (void)transactionRestored:(NSNotification *)note {
    productBought = YES;
    [[NSUserDefaults standardUserDefaults] setObject:[[products objectAtIndex:[pickerView selectedRowInComponent:0]]  productIdentifier] forKey:@"PAYMENT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [paymentTextField setUserInteractionEnabled:NO];
    if (purposeIsToUpdate)
    {
        [self updateSubscription];
    }
    else
        [self createSubscription];        
}
- (void)transactionPurchased:(NSNotification *)note {
    NSLog(@"transaction purchased that seems that appear twice");
    productBought = YES;
    [[NSUserDefaults standardUserDefaults] setObject:[[products objectAtIndex:[pickerView selectedRowInComponent:0]]  productIdentifier] forKey:@"PAYMENT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [paymentTextField setUserInteractionEnabled:NO];
    if (purposeIsToUpdate)
    {
        [self updateSubscription];
    }
    else
        [self createSubscription];
}
- (void)transactionPurchasing:(NSNotification *)note {

}

#pragma mark - Subscription Webservices

-(void) updateSubscription
{
    NSURL *URL = [[NSURL alloc] initWithString:[NSString stringWithFormat:kWebServiceUpdateSubscriptionWithPaymentPlanID_UserID,
                                                [[NSUserDefaults standardUserDefaults] objectForKey:@"PAYMENT"],
                                                [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_ID"]
                                                ]];
    NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL 
                                                cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                            timeoutInterval:kWebServiceTimeoutInterval];
    [URL release];
    updateSubscriptionURLConnection = [[NSURLConnection alloc] initWithRequest:URLRequest 
                                                                      delegate:self
                                                              startImmediately:YES];
}

- (void)createSubscription {
    NSDictionary *settingsDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]];
    
    NSURL *URL = [[NSURL alloc] initWithString:[NSString stringWithFormat:kWebServiceCreateSubscriptionWithClientID_PaymentPlanID_Email_FirstName_LastName_Address_City_State_Zipcode,
                                                [settingsDictionary objectForKey:@"clientID"],  
                                                [[products objectAtIndex:[pickerView selectedRowInComponent:0]] productIdentifier],
                                                emailTextField.text,
                                                [firstNameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
                                                [lastNameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
                                                [addressTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
                                                [cityTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
                                                [stateTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
                                                [zipTextField.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"]]];
    NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL 
                                                cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                            timeoutInterval:kWebServiceTimeoutInterval];
    [URL release];
    createSubscriptionURLConnection = [[NSURLConnection alloc] initWithRequest:URLRequest 
                                                                      delegate:self
                                                              startImmediately:YES];
}   

#pragma mark - NSURLConnectionDelegate Protocol
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;    
    if(connection == createSubscriptionURLConnection)
    {
        NSLog(@"createSubscription - didReceiveResponse");
        createSubscriptionData = [[NSMutableData alloc] init];
    }
    if(connection == updateSubscriptionURLConnection)
    {
        //        NSLog(@"updateSubscription - didReceiveResponse");
        updateSubscriptionData = [[NSMutableData alloc] init];
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if(connection == createSubscriptionURLConnection)
    {
        NSLog(@"createSubscription - didReceiveData");
        [createSubscriptionData appendData:data];
    }
    if(connection == updateSubscriptionURLConnection)
    {
        //        NSLog(@"updateSubscriptionData - didReceiveData");
        [updateSubscriptionData appendData:data];
    }
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if(connection == updateSubscriptionURLConnection)
    {
        //        NSLog(@"updateSubscription - didFailWithError: %@", [error localizedDescription]);
        if(updateSubscriptionData != nil)
        {
            [updateSubscriptionData release];
            updateSubscriptionData = nil;
        }        
    }

    if(connection == createSubscriptionURLConnection)
    {
        NSLog(@"createSubscription - didFailWithError: %@", [error localizedDescription]);
        if(createSubscriptionData != nil)
        {
            [createSubscriptionData release];
            createSubscriptionData = nil;
        }        
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The required fields were not properly completed. Please check the information and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
    
    [connection release];
    submitButton.enabled = YES;
    [spinner stopAnimating];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"did finish loading loading loading");
    if(connection == updateSubscriptionURLConnection)
    {
        NSLog(@"updateSubscription - didFinishLoading");
        NSString *updateSubscriptionString = [[NSString alloc] initWithData:updateSubscriptionData 
                                                                   encoding:NSUTF8StringEncoding];
        [updateSubscriptionData release];
        updateSubscriptionData = nil;
             
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:kPathConfigurationPlistPath,kPathDocumentsDirectory]];
        
        NSDictionary *configurationJSONResponse = [NSDictionary dictionaryWithDictionary:[updateSubscriptionString JSONValue]];    
        NSLog(@"%@",configurationJSONResponse);
        [updateSubscriptionString release];
        NSNumber *number = [configurationJSONResponse objectForKey:@"success"];
        if(number)
        {
            int period = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Period"] intValue];
            period += [[[[dict objectForKey:@"products"] objectAtIndex:[pickerView selectedRowInComponent:0]] objectForKey:@"days"] intValue];
            NSLog(@"period %d",period);
           [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:period] forKey:@"Period"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Subscription updated successfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            //[alertView show];
            //[alertView release];
            [self cancelButtonPressed:nil];
        }
        else
        {
            NSString *error = [configurationJSONResponse objectForKey:@"error"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error when communicating with server. Try again." message:error delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            [alertView release];
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
    }

    else if(connection == createSubscriptionURLConnection)
    {
        NSLog(@"createSubscription - didFinishLoading");
        NSString *createSubscriptionString = [[NSString alloc] initWithData:createSubscriptionData 
                                                                   encoding:NSUTF8StringEncoding];
        [createSubscriptionData release];
        createSubscriptionData = nil;

        NSDictionary *configurationJSONResponse = [NSDictionary dictionaryWithDictionary:[createSubscriptionString JSONValue]];  
        NSLog(@"did finish loading response %@",configurationJSONResponse);
        [createSubscriptionString release];
        NSNumber *number = [configurationJSONResponse objectForKey:@"user_id"];
        if(number)
        {
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:kPathConfigurationPlistPath,kPathDocumentsDirectory]];
            NSNumber *period = [NSNumber numberWithInt:[[[[dict objectForKey:@"products"] objectAtIndex:[pickerView selectedRowInComponent:0]] objectForKey:@"days"] intValue]];
            
            /////save user's informations
            [standardUserDefaults setObject:firstNameTextField.text forKey:@"FirstName"];
            [standardUserDefaults setObject:lastNameTextField.text forKey:@"LastName"];
            [standardUserDefaults setObject:addressTextField.text forKey:@"Address"];
            [standardUserDefaults setObject:cityTextField.text forKey:@"City"];
            [standardUserDefaults setObject:stateTextField.text forKey:@"State"];
            [standardUserDefaults setObject:zipTextField.text forKey:@"ZIP"];
            [standardUserDefaults setObject:emailTextField.text forKey:@"Email"];
            [standardUserDefaults setObject:phoneTextField.text forKey:@"Phone"];
            [standardUserDefaults setObject:couponTextField.text forKey:@"Coupon"];
            
            
            [standardUserDefaults setObject:period forKey:@"Period"];
            [standardUserDefaults setObject:[NSNumber numberWithBool:YES] forKey:@"aProductWasBought"];
            [standardUserDefaults setObject:number forKey:@"USER_ID"];
            [standardUserDefaults setObject:[NSDate date] forKey:@"buyDate"];
            [standardUserDefaults synchronize];
            [self dismissModalViewControllerAnimated:NO];
            [ApplicationDelegate startWithForm:NO];
        }
        else
        {
            NSString *error = [configurationJSONResponse objectForKey:@"error"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error when communicating with server. Try again." message:error delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            [alertView release];
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
    }
    submitButton.enabled = YES;
    [spinner stopAnimating];
}
- (NSCachedURLResponse *)connection:(NSURLConnection *)theConnection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

#pragma mark - UIKeyboardNotifications

- (void)keyboardWillShow:(NSNotification *)notif
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        [UIView animateWithDuration:0.35 animations:^{
            NSLog(@"landscape keyboardWillShow");
           
            CGRect rect = self.view.frame;
            rect.origin.y = -175;
            self.view.frame = rect;
           
             NSLog(@"%@ %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
        }];
    }
}

- (void)keyboardWillHide:(NSNotification *)notif
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        [UIView animateWithDuration:0.35 animations:^{
            NSLog(@"landscape keyboardWillHide");
           
            CGRect rect = self.view.frame;
            rect.origin.y = 0;
            self.view.frame = rect;
           
            NSLog(@"%@ %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));

        }];
    }
}

@end
