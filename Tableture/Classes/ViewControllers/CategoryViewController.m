#import "CategoryViewController.h"
#import "Constants.h"
#import "NewsView.h"

@implementation CategoryViewController
@synthesize bottomAdIsUp;

#pragma mark - Memory Management
- (void)dealloc {   
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if(newsViewArray)
    {
        [newsViewArray release];
    }
    [categoryScrollView release];
    [categoryDescription release];
    [super dealloc];
}
- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [newsViewArray release];
    newsViewArray = nil;
    [categoryScrollView release];
    categoryScrollView = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle
- (id)initWithCategoryId:(int)theCategoryID {
    if((self = [super init]))
    {
        categoryId = theCategoryID;
        NSString *sectionPath = [NSString stringWithFormat:@"%@/feeds/section_%d.plist",kPathDocumentsDirectory,categoryId-1];
        categoryDescription = [[NSDictionary alloc] initWithContentsOfFile:sectionPath];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInterfaceBottomAd) name:kNotificationBottomAdTouched object:nil];
    [self setupView];
}

#pragma mark - Custom Methods
- (void)setupView {  

    NSDictionary *adsSectionsDictionary = [[NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath] objectForKey:@"sections"];
    
    BOOL found = NO;
    for(int i = 0; i < [[adsSectionsDictionary allKeys] count] && !found; ++i)
    {
        NSString *key = [[adsSectionsDictionary allKeys] objectAtIndex:i];
        NSDictionary *adsSpecificSectionDictionary = [adsSectionsDictionary objectForKey:key];
        if([[adsSpecificSectionDictionary objectForKey:@"section_id"] isEqualToString:[NSString stringWithFormat:@"%d",categoryId]])
        {
            categoryAds = [[NSDictionary alloc] initWithDictionary:adsSpecificSectionDictionary];
            found = YES; 
        }
    }
    
    const float sideMargin = 30;
    const float betweenSpace = 20;

    const float borderSize = 24;
    

    
    topAd1 = [[TopAdView alloc] init];
    NSString *topAdIdLeft = [NSString stringWithFormat:@"section_%d_left",categoryId];
    [topAd1 initWithImageString:[[categoryAds objectForKey:@"left"] objectForKey:@"bannerImage"]
                     andtopAdId:topAdIdLeft];
    [topAd1 setAutoresizingMask:UIViewAutoresizingNone];
    [topAd1 setAutoresizesSubviews:NO];
    [categoryScrollView addSubview:topAd1];
    [topAd1 release];
    
    NSString *topAdIdRight = [NSString stringWithFormat:@"section_%d_right",categoryId];
    topAd2 = [[TopAdView alloc] init];
    [topAd2 initWithImageString:[[categoryAds objectForKey:@"right"] objectForKey:@"bannerImage"]
                     andtopAdId:topAdIdRight];
    [topAd2 setAutoresizingMask:UIViewAutoresizingNone];
    [topAd2 setAutoresizesSubviews:NO];
    [categoryScrollView addSubview:topAd2];    
    [topAd2 release];
    
    if([[categoryDescription objectForKey:@"specialZone"] isEqualToString:@"YES"]) 
    {
            const float betweenNewsSpace = 20;
        //CORRECT STRING
        NSMutableString *HTMLString = [NSMutableString string];
        NSString *webserviceHTMLString = [categoryDescription objectForKey:@"specialZoneHtml"];
        [HTMLString appendFormat:@"<html><head><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/></head><body>%@</body></html>",webserviceHTMLString];
        //TESTING STRING1
        //NSString *htmlString = [[categoryDescription objectForKey:@"topStory"] objectForKey:@"html"];  
        
        //TESTING STRING2
        //NSString *htmlString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"faqs" ofType:@"html"]
        //                                                 encoding:NSUTF8StringEncoding 
        //                                                    error:nil];
        
        //SPECIAL ZONE VIEW     
        {            
            NSString *sectionPath = [NSString stringWithFormat:@"%@/section_%d.html",kPathDocumentsDirectory,categoryId-1];
            [HTMLString writeToFile:sectionPath 
                         atomically:YES
                           encoding:NSUTF8StringEncoding 
                              error:nil];
            specialZoneView = [[SpecialZoneView alloc] init];
            [specialZoneView initWithFrame:CGRectMake(sideMargin,
                                                      topAd1.frame.origin.y + topAd1.frame.size.height + betweenSpace,
                                                      300, 
                                                      500)];
            [specialZoneView initWithHTML:[NSString stringWithFormat:@"section_%d.html",categoryId-1]];
            [specialZoneView setAutoresizingMask:UIViewAutoresizingNone];
            [categoryScrollView addSubview:specialZoneView];
            [specialZoneView release];
        }
        
        //TITLE LABEL 
        {
            titleLabel = [[UILabel alloc] init];
            [titleLabel setFrame:CGRectMake(specialZoneView.frame.origin.x + specialZoneView.frame.size.width + betweenSpace, 
                                            specialZoneView.frame.origin.y, 
                                            self.view.frame.size.width - specialZoneView.frame.origin.x - specialZoneView.frame.size.width - betweenSpace - sideMargin - borderSize, 
                                            40)];
            [titleLabel setTextAlignment:UITextAlignmentCenter];
            [titleLabel setFont:[UIFont fontWithName:@"Georgia" size:40]];
            [titleLabel setText:[categoryDescription objectForKey:@"CategoryTitle"]];
            [titleLabel setBackgroundColor:[UIColor clearColor]];
            [categoryScrollView addSubview:titleLabel]; 
            [titleLabel release];;
        }
            
        //RIGHT SCROLL VIEW
        {
            //RIGHT SCROLL VIEW
            {
                rightScrollView = [[UIScrollView alloc] init];
                [rightScrollView setAutoresizingMask:UIViewAutoresizingNone];
                [rightScrollView setFrame:CGRectMake(titleLabel.frame.origin.x, 
                                                     titleLabel.frame.origin.y + titleLabel.frame.size.height + 20,
                                                     titleLabel.frame.size.width,
                                                     specialZoneView.frame.size.height - titleLabel.frame.size.height - 20)];
                [rightScrollView setAutoresizingMask:UIViewAutoresizingNone];
                [categoryScrollView addSubview:rightScrollView];
                [rightScrollView release];
            }
        
            //TOP STORY VIEW
            {
                topStoryView = [[TopStoryViewCategory alloc] initWithFrame:CGRectMake(0, 
                                                                                      0, 
                                                                                      rightScrollView.frame.size.width - 140 - 20, 
                                                                                      140)];
                [topStoryView initWithTitle:[[categoryDescription objectForKey:@"topStory"] objectForKey:@"title"] 
                                  andTeaser:[[categoryDescription objectForKey:@"topStory"] objectForKey:@"teaser"]
                                   andImage:[[categoryDescription objectForKey:@"topStory"] objectForKey:@"thumb"]
                                      andId:[[categoryDescription objectForKey:@"topStory"] objectForKey:@"id"]];;
                [rightScrollView addSubview:topStoryView];
                [topStoryView release];
            }
            
            //NEWS VIEWS + MIDDLE AD VIEW
            {
                newsViewArray = [[NSMutableArray alloc] initWithArray:[categoryDescription objectForKey:@"news"]];
                int firstColumn = [newsViewArray count]/2 + [newsViewArray count]%2;
                
                CGRect lastNewsFrame = CGRectZero;
                for(int i = 0; i < [newsViewArray count]; ++i)
                {
                    NSDictionary *newsDictionary = [newsViewArray objectAtIndex:i];
                    NewsView *newsView = [[NewsView alloc] init];
                    if(i < firstColumn)
                    {
                        [newsView setFrame:CGRectMake(0,
                                                      140 + i * (110 + betweenNewsSpace) + betweenNewsSpace,
                                                      (rightScrollView.frame.size.width - sideMargin) / 2,
                                                      110)];
                    }
                    else
                    {
                        if(i - firstColumn == 0)
                        {
                            [newsView setFrame:CGRectMake(((rightScrollView.frame.size.width - sideMargin) / 2) + sideMargin, 
                                                         140 + (i - firstColumn) * (110 + betweenNewsSpace) + betweenNewsSpace ,
                                                         (rightScrollView.frame.size.width - sideMargin) / 2,
                                                          110)];
                        }
                        else
                        {
                            [newsView setFrame:CGRectMake(((rightScrollView.frame.size.width - sideMargin) / 2) + sideMargin, 
                                                          140 + 180 + (i - firstColumn) * (110 + betweenNewsSpace) + betweenNewsSpace,
                                                          (rightScrollView.frame.size.width - sideMargin) / 2,
                                                          110)];
                        }
                    }
                    [newsView initWithTitle:[newsDictionary objectForKey:@"title"]
                                   andTeaser:[newsDictionary objectForKey:@"teaser"]
                                    andImage:[newsDictionary objectForKey:@"thumb"]
                                      andId:[newsDictionary objectForKey:@"id"]];
                    if(i == [newsViewArray count]-1)
                    {
                        lastNewsFrame = newsView.frame;
                    }
                    
                    [rightScrollView addSubview:newsView];   
                    [newsView release];
                }
               
                middleAd = [[MiddleAdView alloc] init];
                [middleAd setFrame:CGRectMake(326, 
                                              140 + 110 + betweenNewsSpace + betweenNewsSpace, 
                                              325, 
                                              180)];
                [middleAd initWithImageString:[[categoryAds objectForKey:@"middle"] objectForKey:@"bannerImage"] andAdId:[NSString stringWithFormat:@"category_%d",categoryId]];
                [rightScrollView addSubview:middleAd];
                [middleAd release];
                [rightScrollView setContentSize:CGSizeMake(titleLabel.frame.size.width, 
                                                           lastNewsFrame.origin.y + lastNewsFrame.size.height + 40)];
            }            
        }
        
        //BOTTOM AD VIEW
        {
            bottomAdView = [[BottomAdView alloc]initWithFrame:CGRectMake(specialZoneView.frame.origin.x,
                                                                         specialZoneView.frame.origin.y + specialZoneView.frame.size.height + 100,
                                                                         rightScrollView.frame.origin.x + rightScrollView.frame.size.width - 35,
                                                                         579)];
            [bottomAdView initWithImagePath:[NSString stringWithFormat:@"%@/ads/home_bottom.jpg",kPathDocumentsDirectory]];
        
            [categoryScrollView addSubview:bottomAdView];
            [bottomAdView release];
        }
    }    
    else {
        const float betweenNewsSpace = 25;
        //TITLE LABEL 
        {
            titleLabel = [[UILabel alloc] init];
            [titleLabel setFrame:CGRectMake(topAd1.frame.origin.x, 
                                            topAd1.frame.origin.y + topAd1.frame.size.height + betweenSpace, 
                                            topAd1.frame.size.width + betweenSpace + topAd2.frame.size.width, 
                                            40)];
            [titleLabel setTextAlignment:UITextAlignmentCenter];
            [titleLabel setBackgroundColor:[UIColor clearColor]];
            [titleLabel setFont:[UIFont fontWithName:@"Georgia" size:40]];
            [titleLabel setText:[categoryDescription objectForKey:@"CategoryTitle"]];
            [categoryScrollView addSubview:titleLabel]; 
            [titleLabel release];   

        }
        
        //RIGHT SCROLL VIEW
        {
            //RIGHT SCROLL VIEW
            {
                rightScrollView = [[UIScrollView alloc] init];
                [rightScrollView setAutoresizingMask:UIViewAutoresizingNone];
                [rightScrollView setFrame:CGRectMake(titleLabel.frame.origin.x, 
                                                     titleLabel.frame.origin.y + titleLabel.frame.size.height + 20,
                                                     titleLabel.frame.size.width,
                                                     self.view.frame.size.height - kHeightBottomMenu - titleLabel.frame.origin.x - titleLabel.frame.size.height - 40)];
                [rightScrollView setAutoresizingMask:UIViewAutoresizingNone];
                [categoryScrollView addSubview:rightScrollView];
                [rightScrollView release];                
            }
            
            //TOP STORY VIEW
            {
                topStoryView = [[TopStoryViewCategory alloc] initWithFrame:CGRectMake(0, 
                                                                                      0, 
                                                                                      rightScrollView.frame.size.width - 20 - 20, 
                                                                                      20)];
                [topStoryView initWithTitle:[[categoryDescription objectForKey:@"topStory"] objectForKey:@"title"] 
                                  andTeaser:[[categoryDescription objectForKey:@"topStory"] objectForKey:@"teaser"]
                                   andImage:[[categoryDescription objectForKey:@"topStory"] objectForKey:@"thumb"]
                                      andId:[[categoryDescription objectForKey:@"topStory"] objectForKey:@"id"]];
                [rightScrollView addSubview:topStoryView];
                [topStoryView release];
            }
                        
            //NEWS VIEWS + MIDDLE AD VIEW
            {

                newsViewArray = [[NSMutableArray alloc] initWithArray:[categoryDescription objectForKey:@"news"]];
                int firstColumn = [newsViewArray count]/2 + [newsViewArray count]%2;

                CGRect lastNewsFrame = CGRectZero;
                for(int i = 0; i < [newsViewArray count]; ++i)
                {   
                    NSDictionary *newsDictionary = [newsViewArray objectAtIndex:i];
                    NewsView *newsView = [[NewsView alloc] init];
                    [newsView setAutoresizingMask:UIViewAutoresizingNone];
                    if(i < firstColumn)
                    {
                        [newsView setFrame:CGRectMake(0,
                                                      140 + i * 120 + betweenNewsSpace,
                                                      (rightScrollView.frame.size.width - sideMargin) / 2,
                                                      110)];
                    }
                    else
                    {
                        if(i - firstColumn == 0)
                        {
                            [newsView setFrame:CGRectMake(((rightScrollView.frame.size.width - sideMargin) / 2) + sideMargin, 
                                                          140 + (i - firstColumn) * 120 + betweenNewsSpace,
                                                          (rightScrollView.frame.size.width - sideMargin) / 2,
                                                          110)];
                        }
                        else
                        {
                            [newsView setFrame:CGRectMake(((rightScrollView.frame.size.width - sideMargin) / 2) + sideMargin, 
                                                          180 + 180 + (i - firstColumn) * 120 + betweenNewsSpace,
                                                          (rightScrollView.frame.size.width - sideMargin) / 2,
                                                          110)];
                        }
                    }
                    [newsView initWithTitle:[newsDictionary objectForKey:@"title"]
                                  andTeaser:[newsDictionary objectForKey:@"teaser"]
                                   andImage:[newsDictionary objectForKey:@"thumb"]
                                      andId:[newsDictionary objectForKey:@"id"]];
                    if(i == [newsViewArray count]-1)
                    {
                        lastNewsFrame = newsView.frame;
                    }
                    [rightScrollView addSubview:newsView];                  
                    [newsView release];
                }
                                
                middleAd = [[MiddleAdView alloc] init];
                [middleAd setFrame:CGRectMake(((rightScrollView.frame.size.width - sideMargin) / 2) + sideMargin, 
                                              140 + 110 + betweenNewsSpace, 
                                              (rightScrollView.frame.size.width - sideMargin) / 2, 
                                              180)];
                [middleAd initWithImageString:[[categoryAds objectForKey:@"middle"] objectForKey:@"bannerImage"] andAdId:[NSString stringWithFormat:@"category_%d",categoryId]];
                [rightScrollView addSubview:middleAd];
                [middleAd release];
                [rightScrollView setContentSize:CGSizeMake(titleLabel.frame.size.width, 
                                                           lastNewsFrame.origin.y + lastNewsFrame.size.height + 40)];
            }            
        }
        
        //BOTTOM AD VIEW
        {
            bottomAdView = [[BottomAdView alloc]initWithFrame:CGRectMake(rightScrollView.frame.origin.x,
                                                                         rightScrollView.frame.origin.y + rightScrollView.frame.size.height + 100,
                                                                         rightScrollView.frame.origin.x + rightScrollView.frame.size.width - 35,
                                                                         579)];
            NSLog(@"%@",kPathDocumentsDirectory);
            //[bottomAdView initWithImagePath:[NSString stringWithFormat:@"%@/ads/home_bottom.jpg",kPathDocumentsDirectory]];
            [bottomAdView initWithImagePath:[NSString stringWithFormat:@"%@/ads/section_%d_bottom.jpg",kPathDocumentsDirectory, categoryId-1]];

            [categoryScrollView addSubview:bottomAdView];
            [bottomAdView release];
        }
    }
    [categoryAds release];
    [self willAnimateRotationToInterfaceOrientation:[[UIDevice currentDevice] orientation]
                                           duration:0];
} 

#pragma mark - Orientation Support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation 
                                         duration:(NSTimeInterval)duration
{   
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration]; 
    const float sideMargin = 30;
    const float topMargin = 20;
    const float betweenSpace = 20;
    const float borderSize = 24;

    if([[categoryDescription objectForKey:@"specialZone"] isEqualToString:@"YES"])
    {
        if(UIInterfaceOrientationIsLandscape([[ApplicationDelegate tabletureViewController] interfaceOrientation]))
        {
            CGRect landscapeFrame = CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar);

            bottomAdIsUp = NO;
            [categoryScrollView setFrame:landscapeFrame];
            [categoryScrollView setContentSize:landscapeFrame.size];
            [topAd1 setFrame:CGRectMake(sideMargin, 
                                        topMargin, 
                                        (1024 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                        150)];
            [topAd2 setFrame:CGRectMake(topAd1.frame.origin.x + topAd1.frame.size.width + betweenSpace, 
                                        topMargin, 
                                        (1024 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                        150)];    
            [topAd1 updateContentOnOrientationChange:toInterfaceOrientation];
            [topAd2 updateContentOnOrientationChange:toInterfaceOrientation];
           
            [specialZoneView setFrame:CGRectMake(sideMargin,
                                                 topAd1.frame.origin.y + topAd1.frame.size.height + betweenSpace,
                                                 300, 
                                                 420)];
            
            [titleLabel setFrame:CGRectMake(specialZoneView.frame.origin.x + specialZoneView.frame.size.width + betweenSpace, 
                                            specialZoneView.frame.origin.y, 
                                            1024 - specialZoneView.frame.origin.x - specialZoneView.frame.size.width - betweenSpace - sideMargin - borderSize, 
                                            40)];

            [rightScrollView setFrame:CGRectMake(titleLabel.frame.origin.x - 4, 
                                                 titleLabel.frame.origin.y + titleLabel.frame.size.height + 20,
                                                 titleLabel.frame.size.width,
                                                 specialZoneView.frame.size.height - titleLabel.frame.size.height - 20)];
            [topStoryView setFrame:CGRectMake(0,
                                              0,
                                              rightScrollView.frame.size.width,
                                              160)];
            int firstColumn = [newsViewArray count]/2 + [newsViewArray count]%2;
            int i = 0;
            for(UIView *view in [rightScrollView subviews])
            {
                if([view isKindOfClass:[NewsView class]] || [view isKindOfClass:[MiddleAdView class]])
                {
                    CGRect frame = view.frame;
//                    frame.origin.y += 10;
                    if(frame.origin.x != 0)
                    {
                        frame.origin.x = 318;
                    }
                    frame.size.width = 300;
                    view.frame = frame;
                    [(NewsView *)view updateContentOnOrientationChange:toInterfaceOrientation];
                }
                else
                {
                    if([view respondsToSelector:@selector(updateContentOnOrientationChange:)])
                    {
                        [(id)view updateContentOnOrientationChange:toInterfaceOrientation];
                    }
                }
                
                if([view isKindOfClass:[MiddleAdView class]])
                {
                    CGRect frame = view.frame;
                    //                    frame.origin.y +=15;
                    frame.size.height = 130;
                    view.frame = frame;
                    [(NewsView *)view updateContentOnOrientationChange:toInterfaceOrientation];
                    //                    [view setBackgroundColor:[UIColor redColor]];
                }
                if([view isKindOfClass:[NewsView class]] && i>firstColumn+1)
                {
                    CGRect frame = view.frame;
//                    frame.origin.y -= 20;
                    view.frame = frame;
                }
                
                i++;
                
                
            }
            
//            NSLog(@"%@",NSStringFromCGRect(middleAd.frame));
            
            [bottomAdView setFrame:CGRectMake(specialZoneView.frame.origin.x,
                                              specialZoneView.frame.origin.y + specialZoneView.frame.size.height + 10,
                                              rightScrollView.frame.origin.x + rightScrollView.frame.size.width - 30,
                                              561)];
            [bottomAdView updateContentOnOrientationChange:toInterfaceOrientation];
        }
        else
        {  
            CGRect portraitFrame = CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar);
            bottomAdIsUp = NO;
            [categoryScrollView setFrame:portraitFrame];
            [categoryScrollView setContentSize:portraitFrame.size];
            [topAd1 setFrame:CGRectMake(sideMargin, 
                                        topMargin, 
                                        (768 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                        111)];
            [topAd2 setFrame:CGRectMake(topAd1.frame.origin.x + topAd1.frame.size.width + betweenSpace, 
                                        topMargin, 
                                        (768 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                        111)];    
            [topAd1 updateContentOnOrientationChange:toInterfaceOrientation];
            [topAd2 updateContentOnOrientationChange:toInterfaceOrientation];      
           
            [titleLabel setFrame:CGRectMake(sideMargin, 
                                            topAd1.frame.origin.y + topAd1.frame.size.height + 20, 
                                            768 - betweenSpace - sideMargin - borderSize, 
                                            40)];
            
            [rightScrollView setFrame:CGRectMake(titleLabel.frame.origin.x - 4, 
                                                 titleLabel.frame.origin.y + titleLabel.frame.size.height + 20,
                                                 titleLabel.frame.size.width - 5,
                                                 470)];
            [specialZoneView setFrame:CGRectMake(sideMargin,
                                                 rightScrollView.frame.origin.y + rightScrollView.frame.size.height + 20,
                                                 rightScrollView.frame.size.width, 
                                                 200)];
            [topStoryView setFrame:CGRectMake(0,
                                              0,
                                              rightScrollView.frame.size.width,
                                              130)]; 
            
            int firstColumn = [newsViewArray count]/2 + [newsViewArray count]%2;
            int i = 0;
            
            for(UIView *view in [rightScrollView subviews])
            {
                if([view isKindOfClass:[NewsView class]] || [view isKindOfClass:[MiddleAdView class]])
                {
                    CGRect frame = view.frame;
                    if(frame.origin.x != 0) 
                        frame.origin.x = 360;
                    frame.size.width = 330;
                    view.frame = frame;
                    [(NewsView *)view updateContentOnOrientationChange:toInterfaceOrientation];
                }
                if([view respondsToSelector:@selector(updateContentOnOrientationChange:)])
                {
                    [(id)view updateContentOnOrientationChange:toInterfaceOrientation];
                }
                if([view isKindOfClass:[MiddleAdView class]])
                {
                    CGRect frame = view.frame;
                    //                    frame.origin.y +=15;
                    frame.size.height = 130;
                    view.frame = frame;
                    [(NewsView *)view updateContentOnOrientationChange:toInterfaceOrientation];
                    //                    [view setBackgroundColor:[UIColor redColor]];
                }
                if([view isKindOfClass:[NewsView class]] && i>firstColumn+1)
                {
                    CGRect frame = view.frame;
//                    frame.origin.y -= 20;
                    view.frame = frame;
                }
                
                i++;
            }
            
//            NSLog(@"%@",NSStringFromCGRect(middleAd.frame));
            
            [bottomAdView setFrame:CGRectMake(specialZoneView.frame.origin.x,
                                              specialZoneView.frame.origin.y + specialZoneView.frame.size.height + 10,
                                              specialZoneView.frame.size.width,
                                              413)];
            [bottomAdView updateContentOnOrientationChange:toInterfaceOrientation];
        }
    }
    else
    {
        if(UIInterfaceOrientationIsLandscape([[ApplicationDelegate tabletureViewController] interfaceOrientation]))
        {
            CGRect landscapeFrame = CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar);
            
            bottomAdIsUp = NO;
            [categoryScrollView setFrame:landscapeFrame];
            [categoryScrollView setContentSize:landscapeFrame.size];
            [topAd1 setFrame:CGRectMake(sideMargin, 
                                        topMargin, 
                                        (1024 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                        150)];
            [topAd2 setFrame:CGRectMake(topAd1.frame.origin.x + topAd1.frame.size.width + betweenSpace, 
                                        topMargin, 
                                        (1024 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                        150)];    
            [topAd1 updateContentOnOrientationChange:toInterfaceOrientation];
            [topAd2 updateContentOnOrientationChange:toInterfaceOrientation];
              
            
            [titleLabel setFrame:CGRectMake(topAd1.frame.origin.x, 
                                            topAd1.frame.origin.y + topAd1.frame.size.height + betweenSpace, 
                                            topAd1.frame.size.width + betweenSpace + topAd2.frame.size.width, 
                                            40)];           
            
            [rightScrollView setFrame:CGRectMake(titleLabel.frame.origin.x, 
                                                 titleLabel.frame.origin.y + titleLabel.frame.size.height + 20,
                                                 titleLabel.frame.size.width,
                                                 self.view.frame.size.height - kHeightBottomMenu - titleLabel.frame.origin.y - titleLabel.frame.size.height - 80)];        
            [topStoryView setFrame:CGRectMake(-20,
                                              0,
                                              rightScrollView.frame.size.width,
                                              140)];
            [topStoryView updateContentOnOrientationChange:toInterfaceOrientation];
            int firstColumn = [newsViewArray count]/2 + [newsViewArray count]%2;
            int i=0;
            
            for(UIView *view in [rightScrollView subviews])
            {                
                if([view isKindOfClass:[NewsView class]] || [view isKindOfClass:[MiddleAdView class]])
                {
                    CGRect frame = view.frame;

                    if(i<=firstColumn) 
                        frame.origin.x = 0;
                    else
                        frame.origin.x = ((rightScrollView.frame.size.width - sideMargin) / 2) + sideMargin;
                    
                    frame.size.width = (rightScrollView.frame.size.width - sideMargin) / 2;
                    view.frame = frame;
                    [(NewsView *)view updateContentOnOrientationChange:toInterfaceOrientation];
                }

                if([view isKindOfClass:[MiddleAdView class]])
                {
                    CGRect frame = view.frame;
//                    frame.origin.y +=10;
                    frame.origin.y = 283.0;
//                                        NSLog(@"%f",frame.origin.y);
                    frame.size.height = 196;
                    view.frame = frame;
                    [(NewsView *)view updateContentOnOrientationChange:toInterfaceOrientation];
                }
//                if([view isKindOfClass:[NewsView class]] && i>firstColumn+1)
//                {
//                    CGRect frame = view.frame;
//                    frame.origin.y += 40;
//                    view.frame = frame;
//                }
                
                i++;
            }
//            NSLog(@"%@",NSStringFromCGRect(middleAd.frame));
            [bottomAdView setFrame:CGRectMake(rightScrollView.frame.origin.x - 15,
                                              rightScrollView.frame.origin.y + rightScrollView.frame.size.height + 10,
                                              rightScrollView.frame.origin.x + rightScrollView.frame.size.width - 30,
                                              561)];
            [bottomAdView updateContentOnOrientationChange:toInterfaceOrientation];
        }
        else
        {    
            CGRect portraitFrame = CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar);
            bottomAdIsUp = NO;
            [categoryScrollView setFrame:portraitFrame];
            [categoryScrollView setContentSize:portraitFrame.size];
            [topAd1 setFrame:CGRectMake(sideMargin, 
                                        topMargin, 
                                        (768 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                        111)];
            [topAd2 setFrame:CGRectMake(topAd1.frame.origin.x + topAd1.frame.size.width + betweenSpace, 
                                        topMargin, 
                                        (768 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                        111)];    
            [topAd1 updateContentOnOrientationChange:toInterfaceOrientation];
            [topAd2 updateContentOnOrientationChange:toInterfaceOrientation];      
            
            [titleLabel setFrame:CGRectMake(sideMargin, 
                                            topAd1.frame.origin.y + topAd1.frame.size.height + 20, 
                                            768 - betweenSpace - sideMargin - borderSize, 
                                            40)];
            
            [rightScrollView setFrame:CGRectMake(titleLabel.frame.origin.x, 
                                                 titleLabel.frame.origin.y + titleLabel.frame.size.height + 20,
                                                 titleLabel.frame.size.width - 10,
                                                 690)];
            
            [topStoryView setFrame:CGRectMake(-20,
                                              0,
                                              rightScrollView.frame.size.width,
                                              130)]; 
            [topStoryView updateContentOnOrientationChange:toInterfaceOrientation];
            
            int firstColumn = [newsViewArray count]/2 + [newsViewArray count]%2;
            int i=0;
            
            for(UIView *view in [rightScrollView subviews])
            {                
                if([view isKindOfClass:[NewsView class]] || [view isKindOfClass:[MiddleAdView class]])
                {
                    CGRect frame = view.frame;
                    
                    if(i<=firstColumn) 
                        frame.origin.x = 0;
                    else
                        frame.origin.x = ((rightScrollView.frame.size.width - sideMargin) / 2) + sideMargin;
                    
                    frame.size.width = (rightScrollView.frame.size.width - sideMargin) / 2;
                    view.frame = frame;
                    [(NewsView *)view updateContentOnOrientationChange:toInterfaceOrientation];
                }
                if([view isKindOfClass:[MiddleAdView class]])
                {
                    CGRect frame = view.frame;
//                    frame.origin.y +=15;
                    frame.origin.y = 310;
                    NSLog(@"%f",frame.origin.y);
                    frame.size.height = 140;
                    view.frame = frame;
                    [(NewsView *)view updateContentOnOrientationChange:toInterfaceOrientation];
//                    [view setBackgroundColor:[UIColor redColor]];
                }
//                if([view isKindOfClass:[NewsView class]] && i>firstColumn+1)
//                {
//                    CGRect frame = view.frame;
//                    frame.origin.y -= 20;
//                    view.frame = frame;
//                }
                
                i++;
            }
//            NSLog(@"%@",NSStringFromCGRect(middleAd.frame));

            
            [bottomAdView setFrame:CGRectMake(rightScrollView.frame.origin.x - 16,
                                              rightScrollView.frame.origin.y + rightScrollView.frame.size.height + 10,
                                              rightScrollView.frame.size.width,
                                              413)];
            [bottomAdView updateContentOnOrientationChange:toInterfaceOrientation];
       
        }
    }
    
    
    if (UIInterfaceOrientationIsLandscape([[ApplicationDelegate tabletureViewController] interfaceOrientation]))
    {
        NSError *error;
        NSString *path=[[NSBundle mainBundle] pathForResource:@"style_landscape" ofType:@"css"];
        NSString *content=[[NSString alloc] initWithContentsOfFile:path];
        NSString * pathToIndexHTML = [kPathDocumentsDirectory stringByAppendingPathComponent:@"style.css"];
        NSURL * urlToIndexHTML = [NSURL fileURLWithPath:pathToIndexHTML];
        [content writeToURL:urlToIndexHTML atomically: YES encoding:NSUTF8StringEncoding error:&error];
        [content release];
    }
    else {
        
        NSError *error;
        NSString *path=[[NSBundle mainBundle] pathForResource:@"style_portrait" ofType:@"css"];
        NSString *content=[[NSString alloc] initWithContentsOfFile:path];
        NSString * pathToIndexHTML = [kPathDocumentsDirectory stringByAppendingPathComponent:@"style.css"];
        NSURL * urlToIndexHTML = [NSURL fileURLWithPath:pathToIndexHTML];
        [content writeToURL:urlToIndexHTML atomically: YES encoding:NSUTF8StringEncoding error:&error];
        [content release];
    }
    
//    NSLog(@"%@",NSStringFromCGRect(topAd1.frame));
//    NSLog(@"%@",NSStringFromCGRect(topAd2.frame));
    
    
}

#pragma mark - Update
- (void)updateInterfaceBottomAd
{
    CGRect frame = bottomAdView.frame;
    if(frame.size.height == 561)
    {
        if(bottomAdIsUp)
        {
            frame.origin.y = frame.origin.y + 579 - 105;
            bottomAdIsUp = NO; 
        }
        else
        {
            frame.origin.y = frame.origin.y - 579 + 105;
            bottomAdIsUp = YES;
        }       
    }
    else
    {
        if(bottomAdIsUp)
        {
            frame.origin.y = frame.origin.y + 429 - 75;
            bottomAdIsUp = NO;
        }
        else
        {
            frame.origin.y = frame.origin.y - 429 + 75;
            bottomAdIsUp = YES;
        }
    }
    
    [UIView beginAnimations:nil
                    context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    bottomAdView.frame = frame;    
    [UIView commitAnimations];
    
//    NSLog(@"%@",NSStringFromCGRect(bottomAdView.frame));
}
@end
