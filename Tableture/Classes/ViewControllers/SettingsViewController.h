#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Facebook.h"

#import "SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"
#import "SubscriptionViewController.h"

@interface SettingsViewController : UIViewController <MFMailComposeViewControllerDelegate, FBRequestDelegate, FBSessionDelegate, SA_OAuthTwitterControllerDelegate, SA_OAuthTwitterEngineDelegate> {
    
    SubscriptionViewController *subscriptionVC;
    
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *applicationSettingsLabel;
    IBOutlet UIButton *emailButton;
    IBOutlet UIButton *webButton;
    IBOutlet UILabel *contentSizeLabel;
    IBOutlet UISlider *contentSlider;
    IBOutlet UILabel *facebookSharingLabel;
    IBOutlet UISwitch *facebookSharingSwitch;
    IBOutlet UILabel *twitterSharingLabel;
    IBOutlet UISwitch *twitterSharingSwitch;
    IBOutlet UILabel *subscriptionManagementLabel;
    IBOutlet UILabel *subscriptionNeededLabel;
    IBOutlet UIButton *updateButton;
    IBOutlet UIButton *cancelButton;
    
    //Facebook
    Facebook *_facebook;
	NSArray *_permissions;
    
    //Twitter
    SA_OAuthTwitterEngine *_twitter;
    UIButton *cancelButtonPressed;
    
    //NSURLConnections for Cancel
    
    
    NSURLConnection *cancelSubscriptionURLConnection;
    NSMutableData *cancelSubscriptionData;
    
    BOOL shouldRotate;
}
- (IBAction)cancelButtonPressed;
- (IBAction)updateButtonPressed;
- (IBAction)emailButtonPressed;
- (IBAction)webButtonPressed;
- (IBAction)facebookSharingSwitchValueChanged;
- (IBAction)twitterSharingSwitchValueChanged;
- (IBAction)contentSliderValueChanged;

- (void)refresh;
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject;


#pragma mark - Facebook Methods
- (void)facebookLogin;
- (void)facebookLogout;

#pragma mark - Twitter Methods
- (void)twitterLogin;
- (void)twitterLogout;
@end
