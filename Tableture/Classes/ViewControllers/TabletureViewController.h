#import "BottomMenuView.h"
#import "HomeViewController.h"
#import "EventsViewController.h"
#import "SettingsViewController.h"
#import "CategoryViewController.h"
#import "NewsViewController.h"

@interface TabletureViewController : UIViewController <BottomMenuViewDelegate> {
//    HomeViewController *homeViewController;
//    EventsViewController *eventsViewController;
//    SettingsViewController *settingsViewController;
//    NSMutableArray *categoriesViewControllers;
    BottomMenuView *bottomMenuView;
    UIViewController *onlyViewController;
    BOOL timerIsValid;
    NSTimer *timer;
    NSMutableArray *categories;
    UIImageView *adImageView;
    
    BOOL firstTime;    
    int numberOfCategories;
    
    int currentCategoryIndex;
    
    int currentTabIndex;

}
@property (nonatomic, assign) int currentTabIndex;
@property (nonatomic, assign) BOOL timerIsValid;
#pragma mark - TabBar Manipulation
- (void)fillContentsArray;

#pragma mark - Page Switching
- (void)gotoProperPageViewControllerForContentId:(NSString*)contentId 
                                        andIndex:(int)index 
                                     andOldIndex:(int)oldIndex;
- (void)stopCurlAnimationHome:(id)sender;
- (void)stopCurlAnimationEvents:(id)sender;

#pragma mark - Touches
- (void)presentAdViewControllerWithInfoDictionary:(NSDictionary *)adDictionary;
- (void)presentNewsViewControllerWithInfoDictionary:(NSDictionary *)newsDictionary;
- (void)presentSavedStoriesViewController;
- (void)jumpToEvents;
- (void)jumpToCategoryWithCategoryID:(int)theCategoryID;
@end
