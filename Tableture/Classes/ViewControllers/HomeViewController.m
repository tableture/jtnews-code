#import "HomeViewController.h"
#import "Constants.h"

@implementation HomeViewController
@synthesize bottomAdIsUp;
#pragma mark - Memory Management
- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [homeScrollView release];
    homeScrollView = nil;
    [super viewDidUnload];
}
- (void)dealloc {
   
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [homeScrollView release];    
    [super dealloc];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Insert Feed
- (void)insertTopStory {
    NSDictionary *homeDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath];
    topTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 25, 728, 85)];
    [topTitle setText:[homeDictionary objectForKey:@"headTitle"]];
    [topTitle setFont:[UIFont fontWithName:@"Georgia" size:65]];
    [topTitle setTextAlignment:UITextAlignmentLeft];
    [topTitle setBackgroundColor:[UIColor clearColor]];
    [homeScrollView addSubview:topTitle];     
    [topTitle release];
    
    //NSLog(@"home dict %@",homeDictionary);
    NSDictionary *topStoryDictionary = [homeDictionary objectForKey:@"topStory"];
    
    topStoryView = [[TopStoryViewHome alloc] initWithFrame:CGRectMake(20, 120, 325, 140)];
    [topStoryView initWithTitle:[topStoryDictionary objectForKey:@"title"] 
                      andTeaser:[topStoryDictionary objectForKey:@"teaser"]];
    topStoryView.tag = 1;
    [homeScrollView addSubview:topStoryView];
    [topStoryView release];
    
    topStoryImage = [[AsyncImageView alloc] initWithFrame:CGRectMake(370, 120, 360, 250)];
    [topStoryImage setIsHome:YES];
    
    NSString *imageString = [topStoryDictionary objectForKey:@"thumb"];
    NSArray *content = [imageString componentsSeparatedByString:@"/"];
    NSString *name = [content objectAtIndex:[content count]-1];
    [topStoryImage loadImageFromURL:[NSURL URLWithString:imageString] 
                            andName:name
                          withScale:YES];	    
    [homeScrollView addSubview:topStoryImage];
    [topStoryImage release];
}

- (void)insertNews {
    NSDictionary *homeDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath];
    NSArray *newsArray = [homeDictionary objectForKey:@"news"];
    NSDictionary *newsDictionary;
    
    newsDictionary = [newsArray objectAtIndex:0];
    newsView1 = [[NewsView alloc] initWithFrame:CGRectMake(20, 
                                                           topStoryView.frame.origin.y + topStoryView.frame.size.height + 30,
                                                           325, 
                                                           110)];
    [newsView1 initWithTitle:[newsDictionary objectForKey:@"title"]
                   andTeaser:[newsDictionary objectForKey:@"teaser"]
                    andImage:[newsDictionary objectForKey:@"thumb"]
                       andId:[newsDictionary objectForKey:@"id"]];
    newsView1.tag = 1;
    [homeScrollView addSubview:newsView1];
    [newsView1 release];
    
    
    
    newsDictionary = [newsArray objectAtIndex:1];
    newsView2 = [[NewsView alloc] initWithFrame:CGRectMake(18, 
                                                           newsView1.frame.origin.y + newsView1.frame.size.height + 30, 
                                                           325, 
                                                           110)];
    [newsView2 initWithTitle:[newsDictionary objectForKey:@"title"]
                   andTeaser:[newsDictionary objectForKey:@"teaser"]
                    andImage:[newsDictionary objectForKey:@"thumb"]
                       andId:[newsDictionary objectForKey:@"id"]];
    newsView2.tag = 2;
    [homeScrollView addSubview:newsView2];
    [newsView2 release];
    
    newsDictionary = [newsArray objectAtIndex:2];
    newsView3 = [[NewsView alloc] initWithFrame:CGRectMake(18,  
                                                           newsView2.frame.origin.y + newsView2.frame.size.height + 30, 
                                                           325, 
                                                           110)];
    [newsView3 initWithTitle:[newsDictionary objectForKey:@"title"]
                   andTeaser:[newsDictionary objectForKey:@"teaser"]
                    andImage:[newsDictionary objectForKey:@"thumb"]
                       andId:[newsDictionary objectForKey:@"id"]];
    newsView3.tag = 3;
    [homeScrollView addSubview:newsView3];
    [newsView3 release];
}
- (void)insertNewestNewsCategory {
    NSDictionary *homeDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath];
    NSArray *newestNewsCategoryArray = [homeDictionary objectForKey:@"newestNewsCategory"];
    NSDictionary *newestNewsCategoryDictionary;
    
    newestNewsCategoryDictionary = [newestNewsCategoryArray objectAtIndex:0];
    newestNewsCategoryView1 = [[NewestNewsCategoryView alloc] initWithFrame:CGRectMake(topStoryImage.frame.origin.x, 
                                                                                       topStoryImage.frame.origin.y + topStoryImage.frame.size.height + 20, 
                                                                                       topStoryImage.frame.size.width, 
                                                                                       115)];
    [newestNewsCategoryView1 initWithTitle:[newestNewsCategoryDictionary objectForKey:@"categoryName"] 
                                andTeaser1:[NSString stringWithFormat:@"• %@",[newestNewsCategoryDictionary objectForKey:@"titleNews1"]]
                                andTeaser2:[NSString stringWithFormat:@"• %@",[newestNewsCategoryDictionary objectForKey:@"titleNews2"]] 
                                andTeaser3:[NSString stringWithFormat:@"• %@",[newestNewsCategoryDictionary objectForKey:@"titleNews3"]] 
                                     andID:[[newestNewsCategoryDictionary objectForKey:@"categoryID"] intValue]];
    newestNewsCategoryView1.tag = 1;
    [homeScrollView addSubview:newestNewsCategoryView1];
    [newestNewsCategoryView1 release];
    
    newestNewsCategoryDictionary = [newestNewsCategoryArray objectAtIndex:1];
    newestNewsCategoryView2 = [[NewestNewsCategoryView alloc] initWithFrame:CGRectMake(topStoryImage.frame.origin.x, 
                                                                                       newestNewsCategoryView1.frame.origin.y + newestNewsCategoryView1.frame.size.height + 20, 
                                                                                       topStoryImage.frame.size.width, 
                                                                                       115)];
    [newestNewsCategoryView2 initWithTitle:[newestNewsCategoryDictionary objectForKey:@"categoryName"] 
                                andTeaser1:[NSString stringWithFormat:@"• %@",[newestNewsCategoryDictionary objectForKey:@"titleNews1"]]
                                andTeaser2:[NSString stringWithFormat:@"• %@",[newestNewsCategoryDictionary objectForKey:@"titleNews2"]] 
                                andTeaser3:[NSString stringWithFormat:@"• %@",[newestNewsCategoryDictionary objectForKey:@"titleNews3"]] 
                                     andID:[[newestNewsCategoryDictionary objectForKey:@"categoryID"] intValue]];
    newestNewsCategoryView2.tag = 2;
    [homeScrollView addSubview:newestNewsCategoryView2];
    [newestNewsCategoryView2 release];
}
- (void)insertMiddleAd {
    NSMutableDictionary *adDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
	middleAdView = [[MiddleAdView alloc]initWithFrame:CGRectMake(newestNewsCategoryView2.frame.origin.x,
                                                                 newestNewsCategoryView2.frame.origin.y + newestNewsCategoryView2.frame.size.height + 15,
                                                                 newestNewsCategoryView2.frame.size.width,
                                                                 155)];
    [middleAdView initWithImageString:[[[adDictionary objectForKey:@"home"] objectForKey:@"middle"] objectForKey:@"bannerImage"] andAdId:@"home"];
    [homeScrollView addSubview:middleAdView];
    [middleAdView release];
}
- (void)insertComingEvents {
    NSDictionary *homeDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath];
    NSArray *comingEventsArray = [homeDictionary objectForKey:@"comingEvents"];
    comingEventsView = [[ComingEventsView alloc] initWithFrame:CGRectMake(middleAdView.frame.origin.x,
                                                                          middleAdView.frame.origin.y + middleAdView.frame.size.height + 15,
                                                                          middleAdView.frame.size.width,
                                                                          85)];
    [comingEventsView initWithTitle:@"Upcoming events"
                     andTitleEvent1:[[comingEventsArray objectAtIndex:0] objectForKey:@"titleEvent"]
                    andTeaserEvent1:[[comingEventsArray objectAtIndex:0] objectForKey:@"teaserEvent"]
                     andTitleEvent2:[[comingEventsArray objectAtIndex:1] objectForKey:@"titleEvent"] 
                    andTeaserEvent2:[[comingEventsArray objectAtIndex:1] objectForKey:@"teaserEvent"]];
    [homeScrollView addSubview:comingEventsView];
    [comingEventsView release];
    
}
- (void)insertSecondNews {
    NSDictionary *homeDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath];
    NSDictionary *secondNewsDictionary = [homeDictionary objectForKey:@"secondNews"];
    
    secondNewsView = [[SecondNewsView alloc] initWithFrame:CGRectMake(18, 
                                                                      newsView3.frame.origin.y + newsView3.frame.size.height + 30,
                                                                      333, 
                                                                      200)];
    [secondNewsView initWithCategory:[secondNewsDictionary objectForKey:@"CategoryName"]
                            andTitle:[secondNewsDictionary objectForKey:@"title"] 
                           andTeaser:[secondNewsDictionary objectForKey:@"teaser"] 
                            andImage:[secondNewsDictionary objectForKey:@"thumb"]];
    [homeScrollView addSubview:secondNewsView];
    [secondNewsView release];
}
- (void)insertSavedStories {
    savedStoriesView = [[SavedStoriesView alloc] initWithFrame:comingEventsView.frame];
    [homeScrollView addSubview:savedStoriesView];
    [savedStoriesView setHidden:YES];
    [savedStoriesView release];
    
}
- (void)insertBottomAd {
    bottomAdView = [[BottomAdView alloc]initWithFrame:CGRectMake(secondNewsView.frame.origin.x,
                                                                 secondNewsView.frame.origin.y + secondNewsView.frame.size.height + 15,
                                                                 comingEventsView.frame.origin.x + comingEventsView.frame.size.width - 15,
                                                                 429)];

    [bottomAdView initWithImagePath:[NSString stringWithFormat:@"%@/ads/home_bottom.jpg",kPathDocumentsDirectory]];
    [homeScrollView addSubview:bottomAdView];
    [bottomAdView release];
}

- (void)insertFeed {
    [self insertTopStory];
    [self insertNews];
    [self insertNewestNewsCategory];
    [self insertMiddleAd];
    [self insertComingEvents];
    [self insertSecondNews];   
    [self insertSavedStories];
    [self insertBottomAd];
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInterfaceForSavedArticles) name:kNotificationSavedPlistChanged object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInterfaceBottomAd) name:kNotificationBottomAdTouched object:nil];
    
    didAddedBorders = NO;
    [self.view setBackgroundColor:[UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0]];
    [self insertFeed];
    
    if(didAddedBorders == NO)
    {
        UIImage *border = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"border" ofType:@"png"]];

        
        UIImageView *imageView1 = [[UIImageView alloc] initWithImage:border];
        [imageView1 setFrame:CGRectMake(1000, 768 - kHeightBottomMenu - kHeightStatusBar, 24, 768)];
        [homeScrollView addSubview:imageView1];
        [imageView1 release];
        
        UIImageView *imageView2 = [[UIImageView alloc] initWithImage:border];
        [imageView2 setFrame:CGRectMake(1000, -768, 24, 768)];
        [homeScrollView addSubview:imageView2];
        [imageView2 release];    
        
        didAddedBorders = YES;
    }
    
    [self willAnimateRotationToInterfaceOrientation:[[ApplicationDelegate tabletureViewController] interfaceOrientation] 
                                           duration:0];
//    [self willAnimateRotationToInterfaceOrientation:[[UIDevice currentDevice] orientation]
//                                           duration:0];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateInterfaceForSavedArticles];
}
#pragma mark - Rotation Support
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation 
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [topTitle setTextAlignment:UITextAlignmentCenter];
    
    if(UIInterfaceOrientationIsPortrait(toInterfaceOrientation))
    {
        CGRect portraitFrame = CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar);
        [homeScrollView setFrame:portraitFrame];
        bottomAdIsUp = NO;
        [topTitle setFrame:CGRectMake(20, 25, 728, 85)];        
        [topStoryView setFrame:CGRectMake(20, 120, 325, 140)];        
        [topStoryImage setFrame:CGRectMake(370, 120, 360, 250)];                
        [newsView1 setFrame:CGRectMake(20, topStoryView.frame.origin.y + topStoryView.frame.size.height + 30, 325, 110)];
        [newsView2 setFrame:CGRectMake(18, newsView1.frame.origin.y + newsView1.frame.size.height + 30, 325, 110)];
        [newsView3 setFrame:CGRectMake(18, newsView2.frame.origin.y + newsView2.frame.size.height + 30, 325, 110)];        
        [newestNewsCategoryView1 setFrame:CGRectMake(topStoryImage.frame.origin.x, topStoryImage.frame.origin.y + topStoryImage.frame.size.height + 20, topStoryImage.frame.size.width, 115)];
        [newestNewsCategoryView2 setFrame:CGRectMake(topStoryImage.frame.origin.x, newestNewsCategoryView1.frame.origin.y + newestNewsCategoryView1.frame.size.height + 20,  topStoryImage.frame.size.width, 115)];
        [middleAdView setFrame:CGRectMake(newestNewsCategoryView2.frame.origin.x, newestNewsCategoryView2.frame.origin.y + newestNewsCategoryView2.frame.size.height + 15,newestNewsCategoryView2.frame.size.width, 155)];       
        [comingEventsView setFrame:CGRectMake(middleAdView.frame.origin.x, middleAdView.frame.origin.y + middleAdView.frame.size.height + 15, middleAdView.frame.size.width, 85)];
        [savedStoriesView setFrame:comingEventsView.frame];
        [secondNewsView setFrame:CGRectMake(18, newsView3.frame.origin.y + newsView3.frame.size.height + 30, newsView1.frame.size.width, 200)];
        [bottomAdView setFrame:CGRectMake(secondNewsView.frame.origin.x,
                                          secondNewsView.frame.origin.y + secondNewsView.frame.size.height,
                                          comingEventsView.frame.origin.x + comingEventsView.frame.size.width - 15,
                                          429)];
        
        [topStoryImage refresh];
        [homeScrollView setContentSize:homeScrollView.frame.size];  
    }
    else
    {             
        CGRect landscapeFrame = CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar);
        [homeScrollView setFrame:landscapeFrame];
        bottomAdIsUp = NO;
        [topTitle setFrame:CGRectMake(20, 25, 1024 - 60, 120)];        
        [topStoryView setFrame:CGRectMake(20, 160, 430, 140)];        
        [topStoryImage setFrame:CGRectMake(topStoryView.frame.origin.x + topStoryView.frame.size.width + 25, 160, 505, 351)];
        [newsView1 setFrame:CGRectMake(20, topStoryView.frame.origin.y + topStoryView.frame.size.height + 30, 430, 110)];       
        [newsView2 setFrame:CGRectMake(18, newsView1.frame.origin.y + newsView1.frame.size.height + 30, 430, 110)];       
        [newsView3 setFrame:CGRectMake(18, newsView2.frame.origin.y + newsView2.frame.size.height + 30, 430, 110)];        
        [newestNewsCategoryView1 setFrame:CGRectMake(topStoryImage.frame.origin.x, topStoryImage.frame.origin.y + topStoryImage.frame.size.height + 20, topStoryImage.frame.size.width/2 - 5, 115)];        
        [newestNewsCategoryView2 setFrame:CGRectMake(topStoryImage.frame.origin.x + newestNewsCategoryView1.frame.size.width + 10, newestNewsCategoryView1.frame.origin.y, newestNewsCategoryView1.frame.size.width, 115)];        
        [middleAdView setFrame:CGRectMake(newestNewsCategoryView1.frame.origin.x, newestNewsCategoryView1.frame.origin.y + newestNewsCategoryView2.frame.size.height + 15, newestNewsCategoryView1.frame.size.width + 10 + newestNewsCategoryView2.frame.size.width, 218)];        
        [comingEventsView setFrame:CGRectMake(middleAdView.frame.origin.x, middleAdView.frame.origin.y + middleAdView.frame.size.height + 15, middleAdView.frame.size.width, 85)];
        [savedStoriesView setFrame:comingEventsView.frame];
        [secondNewsView setFrame:CGRectMake(18, newsView3.frame.origin.y + newsView3.frame.size.height + 30, newsView1.frame.size.width, 220)];
        [bottomAdView setFrame:CGRectMake(secondNewsView.frame.origin.x,
                                          secondNewsView.frame.origin.y + secondNewsView.frame.size.height + 15 + 20,
                                          comingEventsView.frame.origin.x + comingEventsView.frame.size.width - 15,
                                          579)];
        [topStoryImage refresh];
        CGFloat height = comingEventsView.frame.origin.y + comingEventsView.frame.size.height + 100;
        [homeScrollView setContentSize:CGSizeMake(768, height)];
    }
    
    for(id objectView in [homeScrollView subviews])
    {
        if([objectView respondsToSelector:@selector(updateContentOnOrientationChange:)])
        {
            [objectView updateContentOnOrientationChange:toInterfaceOrientation];   
        }
    }
    [self updateInterfaceForSavedArticles];
    
//    NSLog(@"%@",NSStringFromCGRect(topStoryImage.frame));
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {

	return YES;
    
}


#pragma mark - Saved Articles Changed
- (void)updateInterfaceForSavedArticles
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:kPathSavedPlistPath];
    if([[dictionary objectForKey:@"saved"] boolValue] == YES)
    {
        [savedStoriesView setHidden:NO];
        [comingEventsView setHidden:YES];
    }
    else
    {
        [savedStoriesView setHidden:YES];
        [comingEventsView setHidden:NO];
    }
}
- (void)updateInterfaceBottomAd
{
    CGRect frame = bottomAdView.frame;
    if(frame.size.height == 579)
    {
        if(bottomAdIsUp)
        {
            frame.origin.y = frame.origin.y + 579 - 90;
            bottomAdIsUp = NO; 
        }
        else
        {
            frame.origin.y = frame.origin.y - 579 + 90;
            bottomAdIsUp = YES;
        }       
    }
    else
    {
        if(bottomAdIsUp)
        {
            frame.origin.y = frame.origin.y + 429 - 65;
            bottomAdIsUp = NO;
        }
        else
        {
            frame.origin.y = frame.origin.y - 429 + 65;
            bottomAdIsUp = YES;
        }
    }

    [UIView beginAnimations:nil
                    context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    bottomAdView.frame = frame;    
    [UIView commitAnimations];
//    NSLog(@"%@",NSStringFromCGRect(bottomAdView.frame));
}
@end
