#import "FeedViewController.h"
#import "Constants.h"

@implementation FeedViewController

#pragma mark - Memory Management
- (void)dealloc {
    [loadingLabel release];
    [loadingSpinner release];
    [localImageView release];
    [super dealloc];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void)viewDidUnload {
    [loadingLabel release];
    loadingLabel = nil;
    [loadingSpinner release];
    loadingSpinner = nil;
    [localImageView release];
    localImageView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [loadingLabel setText:kFeedDownloadingText];  
    [loadingSpinner startAnimating];
    [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Orientation Support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    
    if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        [localImageView setFrame:CGRectMake(350, 260, 328, 58)];
        [loadingLabel setFrame:CGRectMake(20, 341, 984, 58)];
        [loadingSpinner setFrame:CGRectMake(502, 403, 20, 20)];
    }
    else
    {
        [localImageView setFrame:CGRectMake(220, 362, 328, 58)];
        [loadingLabel setFrame:CGRectMake(20, 450, 728, 58)];
        [loadingSpinner setFrame:CGRectMake(380, 520, 20, 20)];
    }
}
@end
