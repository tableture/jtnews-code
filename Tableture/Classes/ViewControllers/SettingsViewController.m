#import "SettingsViewController.h"
#import "Constants.h"
#import "JSON.h"
#import <QuartzCore/QuartzCore.h>
#import <StoreKit/StoreKit.h>

@implementation SettingsViewController

#pragma mark - Memory Warning
- (void)viewDidUnload
{
    [titleLabel release];
    titleLabel = nil;
    [applicationSettingsLabel release];
    applicationSettingsLabel = nil;
    [emailButton release];
    emailButton = nil;
    [webButton release];
    webButton = nil;
    [contentSizeLabel release];
    contentSizeLabel = nil;
    [contentSlider release];
    contentSlider = nil;
    [facebookSharingLabel release];
    facebookSharingLabel = nil;
    [facebookSharingSwitch release];
    facebookSharingSwitch = nil;
    [twitterSharingLabel release];
    twitterSharingLabel = nil;
    [twitterSharingSwitch release];
    twitterSharingSwitch = nil;
    [subscriptionManagementLabel release];
    subscriptionManagementLabel = nil;
    [subscriptionNeededLabel release];
    subscriptionNeededLabel = nil;
    [updateButton release];
    updateButton = nil;
    [cancelButton release];
    cancelButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [titleLabel release];
    [applicationSettingsLabel release];
    [emailButton release];
    [webButton release];
    [contentSizeLabel release];
    [contentSlider release];
    [facebookSharingLabel release];
    [facebookSharingSwitch release];
    [twitterSharingLabel release];
    [twitterSharingSwitch release];
    [subscriptionManagementLabel release];
    [subscriptionNeededLabel release];
    [updateButton release];
    [cancelButton release];
    [cancelButtonPressed release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) updateSubscriptionTitle
{
//    NSDate *buyDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"buyDate"];
//    int period = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Period"] intValue];
//    
//    NSDate *date = [[NSDate alloc] initWithTimeInterval:60*60*24*period sinceDate:buyDate];
//    
//    NSDateComponents *comp = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
//    
//    [subscriptionNeededLabel setText:[NSString stringWithFormat:@"Valid until %d/%d/%d",[comp day],[comp month],[comp year]]];
//    
//    [date release];
}

#pragma mark - View lifecycle

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    shouldRotate = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSDictionary *homeDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath];
    [titleLabel setText:[homeDictionary objectForKey:@"headTitle"]];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]];
    if([[dictionary objectForKey:@"appIsFree"] boolValue] == YES)
    {
//        [subscriptionNeededLabel setText:@"No subscription needed"];
        [updateButton setHidden:YES];
        [cancelButton setHidden:YES];
    }
    else
    {
//        NSDate *buyDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"buyDate"];
//        int period = [[[NSUserDefaults standardUserDefaults] objectForKey:@"Period"] intValue];
//        
//        NSDate *date = [[NSDate alloc] initWithTimeInterval:60*60*24*period sinceDate:buyDate];
//        
//        NSDateComponents *comp = [[NSCalendar currentCalendar] components: NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
//        
//        [subscriptionNeededLabel setText:[NSString stringWithFormat:@"Valid until %d/%d/%d",[comp day],[comp month],[comp year]]];
//        
//        [date release];
    }
    NSString *pathToFont = [kPathDocumentsDirectory stringByAppendingPathComponent:@"font.plist"];
	NSURL *url =[NSURL fileURLWithPath:pathToFont];
	NSMutableDictionary *fontSize = [[[NSMutableDictionary alloc]initWithContentsOfURL:url] autorelease];
    if(fontSize != nil)
    {
        float size = [[fontSize objectForKey:@"font"] floatValue];
        contentSlider.value = size;
        [contentSizeLabel setFont:[UIFont systemFontOfSize:contentSlider.value]];
        [contentSizeLabel setText:[NSString stringWithFormat:@"Content Story Font Size: %.0f",contentSlider.value]];
    }
    
    [emailButton.layer setMasksToBounds:YES];
	[emailButton.layer setCornerRadius:10.0];
	[webButton.layer setMasksToBounds:YES];
	[webButton.layer setCornerRadius:10.0];

    [self willAnimateRotationToInterfaceOrientation:[[ApplicationDelegate tabletureViewController] interfaceOrientation]
                                           duration:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSubscriptionTitle) name:@"SubscriptionViewWasRemoved" object:nil];
    // Do any additional setup after loading the view from its nib.
    
    [subscriptionManagementLabel setHidden:YES];
    [subscriptionNeededLabel setHidden:YES];
}
- (void)refresh
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathSharePlistPath];
    if([[dictionary objectForKey:@"Facebook"] isEqualToString:@"NO"])
    {
        [facebookSharingSwitch setOn:NO];
    }
    else
    {
        [facebookSharingSwitch setOn:YES];
    }
    
    
//    BOOL twitterYes = [SA_OAuthTwitterController credentialEntryRequiredWithTwitterEngine: _twitter];
    
    if([[dictionary objectForKey:@"Twitter"] isEqualToString:@"NO"])
    {
        [twitterSharingSwitch setOn:NO];
    }
    else
    {
        [twitterSharingSwitch setOn:YES];
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (shouldRotate)
        return YES;
    else 
        return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {           CGRect landscapeFrame = CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar);
        [self.view setFrame:landscapeFrame];
        
        [titleLabel setFrame:CGRectMake(20, 14, 943, 90)];
        [applicationSettingsLabel setFrame:CGRectMake(339, 141, 347, 21)];
        [emailButton setFrame:CGRectMake(339, 176, 140, 36)];
        [webButton setFrame:CGRectMake(566, 176, 120, 36)];
        [contentSizeLabel setFrame:CGRectMake(339, 246, 347, 30)];
        [contentSlider setFrame:CGRectMake(337, 289, 351, 23)];
        [facebookSharingLabel setFrame:CGRectMake(339, 327, 150, 21)];
        [facebookSharingSwitch setFrame:CGRectMake(592, 326, 94, 27)];
        [twitterSharingLabel setFrame:CGRectMake(339, 360, 183, 21)];
        [twitterSharingSwitch setFrame:CGRectMake(592, 360, 94, 27)];
        [subscriptionManagementLabel setFrame:CGRectMake(339, 408, 347, 21)];
        [subscriptionNeededLabel setFrame:CGRectMake(339, 431, 347, 21)];
        [updateButton setFrame:CGRectMake(412, 495, 201, 37)];
        [cancelButton setFrame:CGRectMake(412, 548, 201, 37)];
    }
    else 
    {        
        CGRect portraitFrame = CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar);
        [self.view setFrame:portraitFrame]; 
        const CGFloat leftPadding = 150;
//        1024 - 943
        [titleLabel setFrame:CGRectMake(20, 14, 768-81, 90)];
        [applicationSettingsLabel setFrame:CGRectMake(339 - leftPadding, 141, 347, 21)];
        [emailButton setFrame:CGRectMake(339 - leftPadding, 176, 140, 36)];
        [webButton setFrame:CGRectMake(566 - leftPadding, 176, 120, 36)];
        [contentSizeLabel setFrame:CGRectMake(339 - leftPadding, 246, 347, 30)];
        [contentSlider setFrame:CGRectMake(337 - leftPadding, 289, 351, 23)];
        [facebookSharingLabel setFrame:CGRectMake(339 - leftPadding, 327, 150, 21)];
        [facebookSharingSwitch setFrame:CGRectMake(592 - leftPadding, 326, 94, 27)];
        [twitterSharingLabel setFrame:CGRectMake(339 - leftPadding, 360, 183, 21)];
        [twitterSharingSwitch setFrame:CGRectMake(592 - leftPadding, 360, 94, 27)];
        [subscriptionManagementLabel setFrame:CGRectMake(339 - leftPadding, 408, 347, 21)];
        [subscriptionNeededLabel setFrame:CGRectMake(339 - leftPadding, 431, 347, 21)];
        
        [updateButton setFrame:CGRectMake(subscriptionNeededLabel.frame.origin.x + 75, subscriptionNeededLabel.frame.origin.y + subscriptionNeededLabel.frame.size.height + 30, 201, 37)];
        [cancelButton setFrame:CGRectMake(subscriptionNeededLabel.frame.origin.x + 75, updateButton.frame.origin.y + updateButton.frame.size.height + 20, 201, 37)];
    }
}
#pragma mark - IBActions
- (IBAction)cancelButtonPressed {
    NSURL *URL = [[NSURL alloc] initWithString:[NSString stringWithFormat:kWebServiceCancelSubscriptionWithUserID,
                                                [[NSUserDefaults standardUserDefaults] objectForKey:@"USER_ID"]
                                                ]];
    NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL 
                                                cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                            timeoutInterval:kWebServiceTimeoutInterval];
    [URL release];
    cancelSubscriptionURLConnection = [[NSURLConnection alloc] initWithRequest:URLRequest 
                                                                      delegate:self
                                                              startImmediately:YES];
}

- (IBAction)updateButtonPressed 
{
    //[[[[[[ApplicationDelegate window] rootViewController] navigationController] viewControllers] objectAtIndex:0] willAnimateRotationToInterfaceOrientation:UIInterfaceOrientationPortrait duration:0.1];
    if (subscriptionVC)
    {
        [subscriptionVC release];
        subscriptionVC = nil;
    }
    subscriptionVC = [[SubscriptionViewController alloc] init];
    [subscriptionVC setPurposeIsToUpdate:YES];
    shouldRotate = NO;
    [[ApplicationDelegate tabletureViewController] presentModalViewController:subscriptionVC animated:YES];
    /*if([[[UIDevice currentDevice] systemVersion] intValue] == 3)
    {
        subscriptionVC.view.frame = CGRectMake(0, 0, 768, 1024);
        subscriptionVC.view.transform = CGAffineTransformMakeRotation(2*M_PI);
        [[ApplicationDelegate window] addSubview:subscriptionVC.view];
        //[subscriptionVC.view release];
        subscriptionVC.view.alpha = 0;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        subscriptionVC.view.alpha = 1;
        
        [UIView commitAnimations];
    }
    else if ([[[UIDevice currentDevice] systemVersion] intValue] >= 4)
    {
        [[ApplicationDelegate tabletureViewController] retain];
        [[ApplicationDelegate window] setRootViewController:subscriptionVC];  
    }*/ 
}

- (IBAction)emailButtonPressed {
    NSString *applicationTitle = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"headTitle"];
    
    [self sendEmailWithRecipients:[NSArray arrayWithObjects:[[NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath] objectForKey:@"linkEmail"],nil] 
                          andBody:@"" 
                       andSubject:applicationTitle];
}

- (IBAction)webButtonPressed {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath] objectForKey:@"linkSite"]]];
}

- (IBAction)facebookSharingSwitchValueChanged {
    if([facebookSharingSwitch isOn])
    {
        [self facebookLogin];
    }
    else
    {
        [self facebookLogout];
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathSharePlistPath];
        [dictionary setObject:@"" forKey:@"FacebookaccessToken"];
        [dictionary setObject:@"" forKey:@"FacebookexpirationDate"];
        [dictionary setObject:@"NO" forKey:@"Facebook"];
        [dictionary writeToFile:kPathSharePlistPath atomically:YES];
        [dictionary release];
    }
}

- (IBAction)twitterSharingSwitchValueChanged {
    if([twitterSharingSwitch isOn])
    {
        [self twitterLogin];
    }
    else
    {       
        [self twitterLogout];
    }
}

- (IBAction)contentSliderValueChanged {
    [contentSizeLabel setFont:[UIFont systemFontOfSize:contentSlider.value]];
    [contentSizeLabel setText:[NSString stringWithFormat:@"Content Story Font Size: %.0f",contentSlider.value]];
    
	NSString *content = [NSString stringWithFormat:@"body {background-color:#E9E0DD; width: 700px; margin-left: 3%;  margin-right: 3%; font-size: %dpx;}",(int)contentSlider.value];	

	NSString *interiorStoryCSS = [kPathDocumentsDirectory stringByAppendingPathComponent:@"interiorStory.css"];
    NSURL *interiorStoryCSSURL = [NSURL fileURLWithPath:interiorStoryCSS];
	[[NSFileManager defaultManager] removeItemAtURL:interiorStoryCSSURL error:nil];
    [content writeToURL:interiorStoryCSSURL 
             atomically:YES 
               encoding:NSUTF8StringEncoding 
                  error:nil];
    
    
    NSString *pathToFont = [kPathDocumentsDirectory stringByAppendingPathComponent:@"font.plist"];
	NSURL *url =[NSURL fileURLWithPath:pathToFont];
	NSMutableDictionary *fontSize = [[[NSMutableDictionary alloc]initWithContentsOfURL:url] autorelease];
    if(fontSize == nil)
    {
        fontSize = [NSMutableDictionary dictionary];
    }
	[fontSize setValue:[NSString stringWithFormat:@"%d",(int)contentSlider.value] forKey:@"font"];
	[fontSize writeToURL:url atomically:NO];
}

#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject
{
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            [picker setSubject:subject];
            [picker setToRecipients:recipients];
            [picker setMessageBody:body isHTML:NO];
            
            [[ApplicationDelegate tabletureViewController] presentModalViewController:picker animated:YES];
            [picker release];            
        }
        else
        {
            NSString *stringWithData =[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",[recipients objectAtIndex:0], subject,body];
            NSString *url = [stringWithData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];	
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];            
        }
    }
    else
    {
        NSString *stringWithData =[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",[recipients objectAtIndex:0], subject,body];
        NSString *url = [stringWithData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];	
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }    
}

#pragma mark - MFMailComposeViewControllerDelegate Protocol
- (void)mailComposeController:(MFMailComposeViewController *)controller 
          didFinishWithResult:(MFMailComposeResult)result 
                        error:(NSError *)error {	
	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled: {
			break;
        }
            
		case MFMailComposeResultSaved: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done"
                                                                message:@"The e-mail has been saved!"
                                                               delegate:nil 
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil];
			[alertView show];
			[alertView release];
            break;
		}
			
		case MFMailComposeResultSent: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done"
                                                                message:@"The e-mail has been sent!"
                                                               delegate:nil 
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil]; 
			[alertView show];
			[alertView release];
            break;
		}
			
		case MFMailComposeResultFailed: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:[NSString stringWithFormat:@"The e-mail has failed with error: %@", [error localizedDescription]]
                                                               delegate:nil
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil]; 
			[alertView show];
			[alertView release];
            break;
		}
		default: {
			break;
        }
	}
	
	[self dismissModalViewControllerAnimated:YES];
}


#pragma mark - Twitter Methods
- (void)twitterLogout {
//    [_twitter log
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathSharePlistPath];
    [dictionary setObject:@"NO" forKey:@"Twitter"];
    [dictionary writeToFile:kPathSharePlistPath atomically:YES];
    [dictionary release];
    [_twitter endUserSession];
    [_twitter closeAllConnections];
    [_twitter clearAccessToken];
    [_twitter clearsCookies];
}
- (void)twitterLogin {
    _twitter = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];
    _twitter.consumerKey    = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"twitterConsumerKey"];
    _twitter.consumerSecret = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"twitterConsumerSecret"];
    
    BOOL authorized = [_twitter isAuthorized];
    
    if (!authorized)
    {
        UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_twitter 
                                                                                                       delegate:self];
        controller.view.frame = CGRectMake(0, 0, 768,1024);
        [controller setModalPresentationStyle:UIModalPresentationPageSheet];
        [controller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [[ApplicationDelegate tabletureViewController] presentModalViewController:controller 
                                animated:YES];
        
    }

}

#pragma mark - SA_OAuthTwitterEngineDelegate Protocol

- (void)storeCachedTwitterOAuthData:(NSString *)data 
                        forUsername:(NSString *)username {
    //implement these methods to store off the creds returned by Twitter
//	[[NSUserDefaults standardUserDefaults] setObject:data forKey:@"authData"];
//	[[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathSharePlistPath];
    [dictionary setObject:@"YES" forKey:@"Twitter"];
    [dictionary writeToFile:kPathSharePlistPath atomically:YES];
    [dictionary release];
}
//- (NSString *)cachedTwitterOAuthDataForUsername:(NSString *)username {
//    //if you don't do this, the user will have to re-authenticate every time they run
//    return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];
//}
- (void)twitterOAuthConnectionFailedWithData:(NSData *)data {
    NSLog(@"FAIL");
}

#pragma mark SA_OAuthTwitterControllerDelegate
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) username {
	NSLog(@"Authenicated for %@", username);
}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
	NSLog(@"Authentication Failed!");
}
- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller
{
    NSLog(@"Authentication Canceled.");
    [self refresh];
}


#pragma mark - Facebook Methods
- (void)facebookLogin {
    _permissions =  [[NSArray arrayWithObjects:@"status_update",nil] retain];
    
    if (![ApplicationDelegate facebook])
    {
        [ApplicationDelegate setFacebook: [[Facebook alloc] initWithAppId:[[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"facebookAppID"]]];
    }
    
//    [ApplicationDelegate facebook] = [[Facebook alloc] initWithAppId:[[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"facebookAppID"]];
    
    [[ApplicationDelegate facebook] authorize:_permissions 
                delegate:self
           andSafariAuth:NO];
}
//- (void)facebookReauthorize {
//    
//    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:kPathSharePlistPath];
//    {
//        _facebook = [[Facebook alloc] initWithAppId:kFacebookAppId];
//        _facebook.accessToken    = [dictionary objectForKey:@"FacebookaccessToken"];
//        _facebook.expirationDate = (NSDate *) [dictionary objectForKey:@"FacebookexpirationDate"];
//        [_facebook fbDialogLogin:_facebook.accessToken 
//                  expirationDate:_facebook.expirationDate]; 
//    }
//}
- (void)facebookLogout {
    [[ApplicationDelegate facebook] logout:self];
}
#pragma mark - FBSessionDelegate Protocol
- (void) fbDidLogout {
    NSLog(@"did logout");
}
- (void) fbDidLogin {
    
    if([[ApplicationDelegate facebook] accessToken])
    {
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathSharePlistPath];
    [dictionary setObject:[[ApplicationDelegate facebook] accessToken] forKey:@"FacebookaccessToken"];
    [dictionary setObject:[[ApplicationDelegate facebook] expirationDate] forKey:@"FacebookexpirationDate"];
    [dictionary setObject:@"YES" forKey:@"Facebook"];
    [dictionary writeToFile:kPathSharePlistPath atomically:YES];
    [dictionary release];
    }
}
- (void) fbDidNotLogin:(BOOL)cancelled {
    [self refresh];
}

#pragma mark - FBRequestDelegate Protocol
- (void)requestLoading:(FBRequest *)request {
    /**
     * Called just before the request is sent to the server.
     */
}
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    /**
     * Called when the server responds and begins to send back data.
     */
}
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    /**
     * Called when an error prevents the request from completing successfully.
     */
}
- (void)request:(FBRequest *)request didLoadRawResponse:(NSData *)data {
    /**
     * Called when a request returns a response.
     *
     * The result object is the raw response from the server of type NSData
     */
}

#pragma mark - NSURLConnectionDelegate Protocol
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;    

    if(connection == cancelSubscriptionURLConnection)
    {
//        NSLog(@"cancelSubscription - didReceiveResponse");
        cancelSubscriptionData = [[NSMutableData alloc] init];
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    if(connection == cancelSubscriptionURLConnection)
    {
//        NSLog(@"cancelSubscription - didReceiveData");
        [cancelSubscriptionData appendData:data];
    }
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if(connection == cancelSubscriptionURLConnection)
    {
//        NSLog(@"cancelSubscription - didFailWithError: %@", [error localizedDescription]);
        if(cancelSubscriptionData != nil)
        {
            [cancelSubscriptionData release];
            cancelSubscriptionData = nil;
        }
        [cancelSubscriptionURLConnection release];
    }
    [connection release];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if(connection == cancelSubscriptionURLConnection)
    {
//        NSLog(@"cancelSubscription - didFinishLoading");
        NSString *cancelSubscriptionString = [[NSString alloc] initWithData:cancelSubscriptionData 
                                                                   encoding:NSUTF8StringEncoding];
        [cancelSubscriptionData release];
        cancelSubscriptionData = nil;
        
        NSDictionary *configurationJSONResponse = [[NSDictionary alloc] initWithDictionary:[cancelSubscriptionString JSONValue]];  
        NSLog(@"%@",configurationJSONResponse);
        NSNumber *number = [configurationJSONResponse objectForKey:@"success"];
        
        [cancelSubscriptionString release];
        [configurationJSONResponse release];
        if(number)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Subscription canceled successfully" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            [alertView release];
            
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults removeObjectForKey:@"PAYMENT"];
            [standardUserDefaults removeObjectForKey:@"USER_ID"];
            [standardUserDefaults removeObjectForKey:@"aProductWasBought"];
            [standardUserDefaults removeObjectForKey:@"buyDate"];
            [standardUserDefaults removeObjectForKey:@"FirstName"];
            [standardUserDefaults removeObjectForKey:@"LastName"];
            [standardUserDefaults removeObjectForKey:@"Address"];
            [standardUserDefaults removeObjectForKey:@"Email"];
            [standardUserDefaults removeObjectForKey:@"State"];
            [standardUserDefaults removeObjectForKey:@"ZIP"];
            [standardUserDefaults removeObjectForKey:@"Email"];
            [standardUserDefaults removeObjectForKey:@"Coupon"];
            [standardUserDefaults synchronize];
        
            [ApplicationDelegate resetApp];
            [ApplicationDelegate startWithForm:YES];
        }
        else
        {
            NSString *error = [configurationJSONResponse objectForKey:@"error"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error when communicating with server. Try again." message:error delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            [alertView release];
        }
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [connection release];
    }
}
- (NSCachedURLResponse *)connection:(NSURLConnection *)theConnection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

@end
