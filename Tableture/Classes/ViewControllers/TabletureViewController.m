#import "TabletureViewController.h"
#import "Constants.h"
#import "SavedStoriesViewController.h"
#import "AdTextViewController.h"
#import "AdTextImageViewController.h"
#import "AdImageViewController.h"


const float delayTime = 2;
const int touches = 400;

@implementation TabletureViewController
@synthesize currentTabIndex;
@synthesize timerIsValid;


#pragma mark - View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad]; 
    timerIsValid = NO;
    timer = nil; 
	self.view.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0];	
	bottomMenuView = [[BottomMenuView alloc] initWithFrame:CGRectMake(0, 
                                                                      self.view.frame.size.height - kHeightBottomMenu, 
                                                                      self.view.frame.size.width, 
                                                                      kHeightBottomMenu)];	
	bottomMenuView.delegate = self;
    firstTime = YES;
    

//    categoriesViewControllers = [[NSMutableArray alloc] init];
//    for(int i = 0; i < [categories count]; ++i)
//    {
//        CategoryViewController *categoryViewController = [[CategoryViewController alloc] initWithCategoryId:i+1];
//        [categoriesViewControllers addObject:categoryViewController];
//        [categoryViewController release];
//    }
    [self fillContentsArray];       
    

    [adImageView release];
    adImageView = [[UIImageView alloc] init];
    [adImageView setUserInteractionEnabled:YES];
    [adImageView setContentMode:UIViewContentModeScaleToFill];
    [self.view addSubview:adImageView];
    //[adImageView release];
}

//- (void)testNewTabCalled:(NSNumber *)number
//{
//    [bottomMenuView selectButtonWithIndex:[number intValue]%7];
//    currentTabIndex = [number intValue]%7;
//}
//- (void)testNewTab
//{
//    if(currentTestTab <= touches)
//    {
//        currentTestTab++;
//        [self performSelector:@selector(testNewTabCalled:) withObject:[NSNumber numberWithInt:currentTestTab] afterDelay:delayTime];
//    }
//}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0];

//    currentTestTab = 0;
    [bottomMenuView selectButtonWithIndex:0];
    currentTabIndex = 0;
    
    //[bottomMenuView selectButtonWithIndex:0];
    //currentTabIndex = 0;  
    
//    NSDictionary *homeDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath];
// 
//    NSDictionary *topStoryDictionary = [homeDictionary objectForKey:@"topStory"];
//
//    [self performSelector:@selector(presentNewsViewControllerWithInfoDictionary:) withObject:topStoryDictionary afterDelay:4];
}


#pragma mark - Rotation Support
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation 
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation 
                                            duration:duration];    
    [bottomMenuView setFrame:CGRectMake(bottomMenuView.frame.origin.x, 
                                        self.view.bounds.size.height - kHeightBottomMenu, 
                                        self.view.bounds.size.width, 
                                        kHeightBottomMenu)];
    [bottomMenuView refreshScrollFrame];
    [bottomMenuView adjustButtonsPosition];    

    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {       
        CGRect landscapeFrame = CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar);
            
            [adImageView setFrame:landscapeFrame];
            
//            if([[self.view subviews] containsObject:homeViewController.view])
//            {
                adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_landscape.jpg"]]; 
        [onlyViewController.view setFrame:landscapeFrame];
        [onlyViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
        //                [homeViewController.view setFrame:landscapeFrame];
//                [homeViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
//                                                                     duration:duration];
//            }
//            else if([[self.view subviews] containsObject:eventsViewController.view])
//            {
//                adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_landscape.jpg"]]; 
//                [eventsViewController.view setFrame:landscapeFrame];
//                [eventsViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
//                                                                       duration:duration];
//            }
//            else if([[self.view subviews] containsObject:settingsViewController.view])
//            {
//                adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_landscape.jpg"]];
//                [settingsViewController.view setFrame:landscapeFrame];
//                [settingsViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
//                                                                         duration:duration];
//            }
//            else if([categoriesViewControllers count] && [[self.view subviews] containsObject:[[categoriesViewControllers objectAtIndex:currentCategoryIndex] view]])
//            {
//                adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/section_%d_landscape.jpg",kPathDocumentsDirectory,currentCategoryIndex]]; 
//                [[[categoriesViewControllers objectAtIndex:currentCategoryIndex] view] setFrame:landscapeFrame];
//                [[categoriesViewControllers objectAtIndex:currentCategoryIndex]  willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
//                                                                         duration:duration];
//            }
        }
        else
        {
            CGRect portraitFrame = CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar);
            
            [adImageView setFrame:portraitFrame];
            
//            if([[self.view subviews] containsObject:homeViewController.view])
//            {
                adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_portrait.jpg"]]; 
            [onlyViewController.view setFrame:portraitFrame];
            [onlyViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
//                [homeViewController.view setFrame:portraitFrame];
//                [homeViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
//                                                                     duration:duration];
//            }
//            else if([[self.view subviews] containsObject:eventsViewController.view])
//            {
//                adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_portrait.jpg"]]; 
//                [eventsViewController.view setFrame:portraitFrame];
//                [eventsViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
//                                                                       duration:duration];
//            }
//            else if([[self.view subviews] containsObject:settingsViewController.view])
//            {
//                adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_portrait.jpg"]];
//                [settingsViewController.view setFrame:portraitFrame];
//                [settingsViewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation
//                                                                         duration:duration];
//            }
//            else if([categoriesViewControllers count]>0)
//            {
//                if([[self.view subviews] containsObject:[[categoriesViewControllers objectAtIndex:currentCategoryIndex] view]])
//                {
//                    adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/section_%d_portrait.jpg",kPathDocumentsDirectory,currentCategoryIndex]]; 
//                    [[[categoriesViewControllers objectAtIndex:currentCategoryIndex] view] setFrame:portraitFrame];
//                    [[categoriesViewControllers objectAtIndex:currentCategoryIndex]  willAnimateRotationToInterfaceOrientation:toInterfaceOrientation                                                                                                            duration:duration];
//                }
//            }
        }
    }

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    
	if(timerIsValid == NO)
    {
       return YES; 
    }
    else
    {
        return NO;
    }
}

#pragma mark - Memory Management
- (void)viewDidUnload {
    [super viewDidUnload];
    [adImageView release];
    adImageView = nil;
    
}
- (void)dealloc {
//    [categoriesViewControllers release];
    [adImageView release];
    [timer invalidate];
    timer = nil;
    [categories release];
    [bottomMenuView setDelegate:nil];
    [bottomMenuView release];
    [super dealloc];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
//    for(CategoryViewController *categoryViewController in categoriesViewControllers)
//    {
//        [categoryViewController didReceiveMemoryWarning];
//    }
    // Release any cached data, images, etc that aren't in use.
}
//- (void)viewDidUnload {
//    [super viewDidUnload];
//    for(CategoryViewController *categoryViewController in categoriesViewControllers)
//    {
//        [categoryViewController viewDidUnload];
//    }
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}



#pragma mark - BottomMenuViewDelegate Protocol
- (void)bottomMenuTouchWithIndex:(NSInteger)index 
                    andContentId:(NSString *)contentId {  
    if(timerIsValid == NO)
    {
//        NSLog(@"%@ - %d",contentId,index);
        [self gotoProperPageViewControllerForContentId:contentId 
                                              andIndex:index 
                                           andOldIndex:bottomMenuView.touchedIndex];
    }
   
}

#pragma mark - TabBar Manipulation
- (void)fillContentsArray {
    NSMutableDictionary *dictionaryConfiguration = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathConfigurationPlistPath];
    NSMutableArray *contents = [[NSMutableArray alloc] init];
	categories = [[NSMutableArray alloc] initWithArray:[dictionaryConfiguration objectForKey:@"list"]];
    numberOfCategories = [categories count] + 2;
    
    //------------------------------------------------------------------------------------------
    //Home
	BottomMenuViewData *entry = [[BottomMenuViewData alloc] init];
	entry.title = [NSString stringWithFormat:@"Home"];
	entry.contentId = [NSString stringWithFormat:@"%d", -1];
	entry.colorDark = [NSString stringWithString:@"#EA7C23"];
	entry.colorLight = [NSString stringWithString:@"#EF9E4F"];
	[contents addObject:entry];	
	[entry release];
	
    //Categories
	for (int i=0; i<[categories count]; i++) {
		NSDictionary *dictionary = [categories objectAtIndex:i];
		BottomMenuViewData *entry = [[BottomMenuViewData alloc] init];
		entry.title = [dictionary objectForKey:@"sectionMenuTitle"];
		entry.contentId = [dictionary objectForKey:@"id"];
		entry.colorDark = [dictionary objectForKey:@"colorDark"];
		entry.colorLight = [dictionary objectForKey:@"colorLight"];
		[contents addObject:entry];	
		[entry release];
	}
    
    //Events
	entry = [[BottomMenuViewData alloc] init];
	entry.title = [[dictionaryConfiguration objectForKey:@"events"] objectForKey:@"sectionMenuTitle"];
	entry.contentId = [NSString stringWithFormat:@"%d", [categories count]+1];
	entry.colorDark = [[dictionaryConfiguration objectForKey:@"events"] objectForKey:@"colorDark"];
	entry.colorLight = [[dictionaryConfiguration objectForKey:@"events"] objectForKey:@"colorLight"];
    [dictionaryConfiguration release];
	[contents addObject:entry];	
    [entry release];
	
    //Settings
	entry = [[BottomMenuViewData alloc] init];
	entry.title = @"Settings";
	entry.contentId = [NSString stringWithFormat:@"%d", categories.count+2];
	entry.colorDark = [NSString stringWithString:@"#6F54A3"];
	entry.colorLight = [NSString stringWithString:@"#927EB9"];
	[contents addObject:entry];	
	[entry release];
    //------------------------------------------------------------------------------------------
    
	[self.view addSubview:bottomMenuView];
    //[bottomMenuView release];
	[bottomMenuView setFrame:CGRectMake(bottomMenuView.frame.origin.x, 
                                        self.view.bounds.size.height-kHeightBottomMenu,
                                        self.view.bounds.size.width, 
                                        kHeightBottomMenu)];	  
    [bottomMenuView refreshScrollFrame];
    [bottomMenuView setAutoresizingMask:
     UIViewAutoresizingFlexibleWidth | 
     UIViewAutoresizingFlexibleTopMargin | 
     UIViewAutoresizingFlexibleLeftMargin | 
     UIViewAutoresizingFlexibleRightMargin];
    [bottomMenuView setAutoresizesSubviews:YES];
    
    [bottomMenuView initButtonsByArray:contents];
    [bottomMenuView setUserInteractionEnabled:YES];
	[contents release];
}

#pragma mark - Page Switching
- (void)gotoProperPageViewControllerForContentId:(NSString*)contentId 
                                        andIndex:(int)index 
                                     andOldIndex:(int)oldIndex {   
//    NSLog(@"%@",contentId);
    
    //Timer reset
	if (timer != nil)
    {
        [timer invalidate];
    }
	timer = nil;    
    
    //Home
	if (index == 0)
	{   
        [onlyViewController release];
        onlyViewController = [[HomeViewController alloc] init];
//        [eventsViewController release];
//        [settingsViewController release];

        [(HomeViewController *)onlyViewController setBottomAdIsUp:NO];
        if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))		
        {
            [adImageView setFrame:CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar)];
            [adImageView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_landscape.jpg"]]]; 
//            if(![[self.view subviews] containsObject:adImageView])
//            {
////                NSLog(@"add subview");
//                [self.view addSubview:adImageView];
//            }
            [self.view bringSubviewToFront:adImageView];
            [[(HomeViewController *)onlyViewController view] setFrame:CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar)];
        }
        else 
        {	
//            adImageView = [[UIImageView alloc] init];
//            [adImageView setUserInteractionEnabled:YES];
//            [adImageView setContentMode:UIViewContentModeScaleToFill];
            [adImageView setFrame:CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar)];
            [adImageView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_portrait.jpg"]]]; 
//            if(![[self.view subviews] containsObject:adImageView])
//            {
////                NSLog(@"add subview");
//                [self.view addSubview:adImageView];
//            }
            [self.view bringSubviewToFront:adImageView];
			[[(HomeViewController *)onlyViewController view] setFrame:CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar)];
        }
        for(UIView *view in [self.view subviews])
        {
            if(view != bottomMenuView && view != adImageView)
            {
                [view removeFromSuperview];
            }
        }
        [self.view addSubview:[(HomeViewController *)onlyViewController view]];
        [(HomeViewController *)onlyViewController willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0];
        if(kAdsEnabled)
        {
            [self.view sendSubviewToBack:[(HomeViewController *)onlyViewController view]];            
            if (adImageView.image == nil)
            {
                [self.view sendSubviewToBack:adImageView];
            }	
                 
            [UIView beginAnimations:nil context:NULL]; 
            [UIView setAnimationDuration:1.0];
            [UIView setAnimationTransition:((index > oldIndex) ? UIViewAnimationTransitionCurlUp : UIViewAnimationTransitionCurlDown) 
                                   forView:self.view 
                                     cache:YES];                      
            [UIView commitAnimations];
            
            if (adImageView.image == nil)
            {
                [self.view sendSubviewToBack:adImageView];
            }
            else 
            {
                timer = [NSTimer scheduledTimerWithTimeInterval:kAdDisplayTime 
                                                         target:self 
                                                       selector:@selector(stopCurlAnimationHome:) 
                                                       userInfo:nil 
                                                        repeats:NO];
            }
        }
	}
    //Events
	else if (index == numberOfCategories - 1)
    {
        [onlyViewController release];
        onlyViewController = [[EventsViewController alloc] init];
//        [homeViewController release];
//        [settingsViewController release];
        
        if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))		
        {
//            adImageView = [[UIImageView alloc] init];
//            [adImageView setUserInteractionEnabled:YES];
//            [adImageView setContentMode:UIViewContentModeScaleToFill];
//            [adImageView setFrame:CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar)];
//            [adImageView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_landscape.jpg"]]]; 
//            if(![[self.view subviews] containsObject:adImageView])
//            {
////                NSLog(@"add subview");
//                [self.view addSubview:adImageView];
//            }
//            [self.view bringSubviewToFront:adImageView];
            [onlyViewController.view setFrame:CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar)];
        }
        else 
        {	
//            adImageView = [[UIImageView alloc] init];
//            [adImageView setUserInteractionEnabled:YES];
//            [adImageView setContentMode:UIViewContentModeScaleToFill];
//            [adImageView setFrame:CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar)];
//            [adImageView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_portrait.jpg"]]]; 
//            if(![[self.view subviews] containsObject:adImageView])
//            {
////                NSLog(@"add subview");
//                [self.view addSubview:adImageView];
//            }
//            [self.view bringSubviewToFront:adImageView];
            [onlyViewController.view setFrame:CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar)];			
        }
        for(UIView *view in [self.view subviews])
        {
            if(view != bottomMenuView && view != adImageView)
            {
                [view removeFromSuperview];
            }
        }
        [self.view addSubview:onlyViewController.view];  
        [onlyViewController willAnimateRotationToInterfaceOrientation:self.interfaceOrientation 
                                                               duration:0];
        if(kAdsEnabled)
        {
//            [self.view sendSubviewToBack:eventsViewController.view];
            
            [UIView beginAnimations:nil context:NULL]; 
            [UIView setAnimationDuration:1.0]; 
            [UIView setAnimationTransition: ( (index > oldIndex) ? UIViewAnimationTransitionCurlUp :UIViewAnimationTransitionCurlDown) 
                                   forView:self.view 
                                     cache:YES];         
            [UIView commitAnimations];
            
//            if (adImageView.image == nil)
//            {
//                [self.view sendSubviewToBack:adImageView];
//            }
//            else 
//            {
//                timer = [NSTimer scheduledTimerWithTimeInterval:kAdDisplayTime 
//                                                         target:self 
//                                                       selector:@selector(stopCurlAnimationEvents:) 
//                                                       userInfo:nil 
//                                                        repeats:NO];
//            }
        }
    }
    //Settings
    else if (index == numberOfCategories)
    {
        [onlyViewController release];
        onlyViewController = [[SettingsViewController alloc] init];
        
        if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))		
        {
//            adImageView = [[UIImageView alloc] init];
//            [adImageView setUserInteractionEnabled:YES];
//            [adImageView setContentMode:UIViewContentModeScaleToFill];
//            [adImageView setFrame:CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar)];
//            [adImageView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_landscape.jpg"]]]; 
//            if(![[self.view subviews] containsObject:adImageView])
//            {
////                NSLog(@"add subview");
//                [self.view addSubview:adImageView];
//            }
//            [self.view bringSubviewToFront:adImageView];
            [onlyViewController.view setFrame:CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar)];
        }
        else 
        {	
//            adImageView = [[UIImageView alloc] init];
//            [adImageView setUserInteractionEnabled:YES];
//            [adImageView setContentMode:UIViewContentModeScaleToFill];
//            [adImageView setFrame:CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar)];
//            [adImageView setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/%@",kPathDocumentsDirectory,@"home_portrait.jpg"]]]; 
//            if(![[self.view subviews] containsObject:adImageView])
//            {
////                NSLog(@"add subview");
//                [self.view addSubview:adImageView];
//            }
//            [self.view bringSubviewToFront:adImageView];
            [onlyViewController.view setFrame:CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar)];
        }
        for(UIView *view in [self.view subviews])
        {
            if(view != bottomMenuView && view != adImageView)
            {
                [view removeFromSuperview];
            }
        }
        [self.view addSubview:onlyViewController.view];
        //[settingsViewController willAnimateRotationToInterfaceOrientation:self.interfaceOrientation
         //                                                        duration:0];
        [(SettingsViewController *)onlyViewController refresh];
        if(kAdsEnabled)
        {            
//            [self.view sendSubviewToBack:settingsViewController.view];

            [UIView beginAnimations:nil context:NULL]; 
            [UIView setAnimationDuration:1.0]; 
            [UIView setAnimationTransition: ( (index > oldIndex) ? UIViewAnimationTransitionCurlUp :UIViewAnimationTransitionCurlDown) 
                                   forView:self.view 
                                     cache:YES];         
            [UIView commitAnimations];
            
//            if (adImageView.image == nil)
//            {
//                [self.view sendSubviewToBack:adImageView];
//            }
//            else 
//            {
//                timer = [NSTimer scheduledTimerWithTimeInterval:kAdDisplayTime 
//                                                         target:self 
//                                                       selector:@selector(stopCurlAnimationSettings:) 
//                                                       userInfo:nil 
//                                                        repeats:NO];
//            }
        }
    }
//    //Categories
    else
    {
        currentCategoryIndex = index;
        [onlyViewController release];
        onlyViewController = [[CategoryViewController alloc] initWithCategoryId:currentCategoryIndex];
//        CategoryViewController *auxCategory = [categoriesViewControllers objectAtIndex:currentCategoryIndex];
        [(CategoryViewController *)onlyViewController setBottomAdIsUp:NO];
        if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))		
        {
//            adImageView = [[UIImageView alloc] init];
//            [adImageView setUserInteractionEnabled:YES];
//            [adImageView setContentMode:UIViewContentModeScaleToFill];
            [adImageView setFrame:CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar)];
            adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/section_%d_landscape.jpg",kPathDocumentsDirectory,currentCategoryIndex-1]];
//            if(![[self.view subviews] containsObject:adImageView])
//            {
////                NSLog(@"add subview");
//                [self.view addSubview:adImageView];
//            }
            [self.view bringSubviewToFront:adImageView];
            [[onlyViewController view] setFrame:CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar)];

        }
        else 
        {	
//            adImageView = [[UIImageView alloc] init];
//            [adImageView setUserInteractionEnabled:YES];
//            [adImageView setContentMode:UIViewContentModeScaleToFill];
            [adImageView setFrame:CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar)];
            adImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ads/section_%d_portrait.jpg",kPathDocumentsDirectory,currentCategoryIndex-1]];
//            if(![[self.view subviews] containsObject:adImageView])
//            {
////                NSLog(@"add subview");
//                [self.view addSubview:adImageView];
//            }
            [self.view bringSubviewToFront:adImageView];
            [[onlyViewController view] setFrame:CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar)];
        }
        for(UIView *view in [self.view subviews])
        {
            if(view != bottomMenuView && view != adImageView)
            {
                [view removeFromSuperview];
            }
        }
        [self.view addSubview:[onlyViewController view]];      
        [onlyViewController willAnimateRotationToInterfaceOrientation:self.interfaceOrientation 
                                                      duration:0];
        if(kAdsEnabled)
        {
            [self.view sendSubviewToBack:[onlyViewController view]];

            [UIView beginAnimations:nil context:NULL]; 
            [UIView setAnimationDuration:1.0]; 
            [UIView setAnimationTransition: ( (index > oldIndex) ? UIViewAnimationTransitionCurlUp :UIViewAnimationTransitionCurlDown) 
                                   forView:self.view 
                                     cache:YES];         
            [UIView commitAnimations];
            
            if (adImageView.image == nil)
            {
                [self.view sendSubviewToBack:adImageView];
            }
            else 
            {
                timer = [NSTimer scheduledTimerWithTimeInterval:kAdDisplayTime 
                                                         target:self 
                                                       selector:@selector(stopCurlAnimationCategory:) 
                                                       userInfo:nil 
                                                        repeats:NO];
            }
        }
    }
} 
- (void)validateTimer 
{
            timerIsValid = NO;
}
- (void)stopCurlAnimationHome:(id)sender {

    [UIView beginAnimations:nil 
                    context:NULL]; 
	[UIView setAnimationDuration:1.0];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp 
                           forView:self.view 
                             cache:YES]; 
    [UIView setAnimationDidStopSelector:@selector(validateTimer)];
	[self.view sendSubviewToBack:adImageView];
    [self.view bringSubviewToFront:[(HomeViewController *)onlyViewController view]];
	[UIView commitAnimations];
//    [adImageView removeFromSuperview];
    [timer invalidate];
	timer = nil;
    
//    [self testNewTab];
}
- (void)stopCurlAnimationEvents:(id)sender {  
	[UIView beginAnimations:nil 
                    context:NULL]; 
	[UIView setAnimationDuration:1.0];
	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp 
                           forView:self.view  
                             cache:YES]; 
        [UIView setAnimationDidStopSelector:@selector(validateTimer)];
	[self.view sendSubviewToBack:adImageView];  
    [self.view bringSubviewToFront:onlyViewController.view];
	[UIView commitAnimations];
//    [adImageView removeFromSuperview];
    [timer invalidate];
	timer = nil;
    
//    [self testNewTab];
}
- (void)stopCurlAnimationSettings:(id)sender { 
	[UIView beginAnimations:nil 
                    context:NULL]; 
	[UIView setAnimationDuration:1.0];
	[UIView setAnimationTransition: UIViewAnimationTransitionCurlUp 
                           forView:self.view 
                             cache:YES]; 
        [UIView setAnimationDidStopSelector:@selector(validateTimer)];
	[self.view sendSubviewToBack:adImageView];
    [self.view bringSubviewToFront:onlyViewController.view];
	[UIView commitAnimations];
//    [adImageView removeFromSuperview];
    [timer invalidate];
	timer = nil;

//    [self testNewTab];
}
- (void)stopCurlAnimationCategory:(id)sender { 
	[UIView beginAnimations:nil 
                    context:NULL]; 
	[UIView setAnimationDuration:1.0];
 	[UIView setAnimationTransition:UIViewAnimationTransitionCurlUp
                           forView:self.view 
                             cache:YES]; 
        [UIView setAnimationDidStopSelector:@selector(validateTimer)];
	[self.view sendSubviewToBack:adImageView];
//    [adImageView release];
    [self.view bringSubviewToFront:[onlyViewController view]];
	[UIView commitAnimations];
//    [adImageView removeFromSuperview];
    [timer invalidate];
	timer = nil;
    
//    [self testNewTab];
}

#pragma mark - Present Modal VC
- (void)presentAdViewControllerWithInfoDictionary:(NSDictionary *)adDictionary
{
    NSString *type = [adDictionary objectForKey:@"type"];
    
    if([type isEqualToString:@"text"])
    {
        AdTextViewController *adTextViewController = [[AdTextViewController alloc] initWithDictionary:adDictionary];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:adTextViewController];
        
        [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
        [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentModalViewController:navigationController animated:YES];
        navigationController.view.superview.bounds = CGRectMake(0, 0, 600, 400);
        [adTextViewController refresh];
        [adTextViewController release];
        
        [navigationController release];
    }    
    if([type isEqualToString:@"textImage"])
    {
        AdTextImageViewController *adTextImageViewController = [[AdTextImageViewController alloc] initWithDictionary:adDictionary];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:adTextImageViewController];

        [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
        [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentModalViewController:navigationController animated:YES];
        navigationController.view.superview.bounds = CGRectMake(0, 0, 600, 400);
        [adTextImageViewController refresh];
        [adTextImageViewController release];
        
        [navigationController release];
    }
    if([type isEqualToString:@"image"])
    {
        AdImageViewController *adImageViewController = [[AdImageViewController alloc] initWithDictionary:adDictionary];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:adImageViewController];
        [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
        [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentModalViewController:navigationController animated:YES];
        navigationController.view.superview.bounds = CGRectMake(0, 0, 600, 400);
        [adImageViewController refresh];
        [adImageViewController release];
        
        [navigationController release];
    }
}
- (void)presentSavedStoriesViewController {
    SavedStoriesViewController *savedStoriesViewController = [[SavedStoriesViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:savedStoriesViewController];
       [savedStoriesViewController release];
    [navigationController setModalPresentationStyle:UIModalPresentationPageSheet];
    [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve]; 
    [self presentModalViewController:navigationController
                            animated:YES];
    [navigationController release];
    
}

- (void)close {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)presentNewsViewControllerWithInfoDictionary:(NSDictionary *)newsDictionary
{
    NewsViewController *newsViewController = [[NewsViewController alloc] initWithDictionary:newsDictionary];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:newsViewController];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" 
                                                                            style:UIBarButtonItemStylePlain 
                                                                           target:self 
                                                                           action:@selector(close)];
    [newsViewController.navigationItem setLeftBarButtonItem:closeButton];
    [closeButton release];
    
    [newsViewController release];
    [navController setModalPresentationStyle:UIModalPresentationPageSheet];
    [navController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentModalViewController:navController
                            animated:YES];
    [navController release];
}
- (void)jumpToEvents 
{
    int theCategoryID = [categories count] + 1;
    currentTabIndex = theCategoryID;
    [bottomMenuView selectButtonWithIndex:theCategoryID];
    NSString *contentId = [NSString stringWithFormat:@"%d",theCategoryID];
    [self bottomMenuTouchWithIndex:theCategoryID andContentId:contentId];
//    [self gotoProperPageViewControllerForContentId:contentId 
//                                          andIndex:theCategoryID 
//                                       andOldIndex:bottomMenuView.touchedIndex];
//     [bottomMenuView selectButtonWithIndex:[categories count]+1];
}
- (void)jumpToCategoryWithCategoryID:(int)theCategoryID
{
    if([categories count] >= theCategoryID)
    {
        currentTabIndex = theCategoryID;
        [bottomMenuView selectButtonWithIndex:theCategoryID];
        NSString *contentId = [NSString stringWithFormat:@"%d",theCategoryID];
        [self bottomMenuTouchWithIndex:theCategoryID andContentId:contentId];
    
//        [self gotoProperPageViewControllerForContentId:contentId 
//                                              andIndex:theCategoryID 
//                                           andOldIndex:bottomMenuView.touchedIndex];
//        [bottomMenuView selectButtonWithIndex:theCategoryID];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Category does not exist" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        [alertView show];
        [alertView release];
    }
}

#pragma mark - Touches
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *singleTouch = [[event allTouches] anyObject];
	if (singleTouch.view == adImageView)
	{
//        if ([[self.view subviews] containsObject:homeViewController.view])
//        {
            [self stopCurlAnimationHome:nil];
//        }
//        else if([[self.view subviews] containsObject:eventsViewController.view])
//        {
//            [self stopCurlAnimationEvents:nil];
//        }
//        else if([[self.view subviews] containsObject:settingsViewController.view])
//        {
//            [self stopCurlAnimationSettings:nil];
//        }
//        else
//        {
//            [self stopCurlAnimationCategory:nil];
//        }
	}
}
@end
