#import "CheckmarkTile.h"
#import "TopAdView.h"

@interface EventsViewController : UIViewController 
<KLCalendarViewDelegate, 
UITableViewDataSource, 
UITableViewDelegate> {
    
    IBOutlet UITableView *tableView1;
    IBOutlet UITableView *tableView2;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIImageView *marginImageView;
    UIView *v;
    KLCalendarView *calendarView;
	KLTile *currentTile;
	KLTile *previousTile;
    
    TopAdView *topAd1;
	TopAdView *topAd2; 
    
    NSMutableArray *eventsArray;  

}
- (void)setupView;
@end
