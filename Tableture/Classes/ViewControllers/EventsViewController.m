
#import "EventsViewController.h"
#import "Constants.h"
#import "KLCalendarView.h"
#import "KLTile.h"
#import "KLColors.h"
#import "EventsDetailViewController.h"
@implementation EventsViewController

#pragma mark - Memory Management
- (void)viewDidUnload
{
    [tableView1 release];
    tableView1 = nil;
    [tableView2 release];
    tableView2 = nil;
    [titleLabel release];
    titleLabel = nil;
    [marginImageView release];
    marginImageView = nil;
    [super viewDidUnload];
}

- (void)dealloc
{
    [eventsArray release];
    [titleLabel release];
    [tableView1 release];
    [tableView2 release];    
    [marginImageView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    
    tableView1.delegate = tableView2.delegate = self;
    tableView1.dataSource = tableView2.dataSource = self;   
    tableView1.backgroundColor = tableView2.backgroundColor = [UIColor clearColor];
    
//    [self setupView];
    
    NSMutableDictionary *dataContent = [NSMutableDictionary dictionaryWithContentsOfFile:kPathEventsPlistPath];   
	if ([[dataContent allKeys] containsObject:[NSString stringWithFormat:@"%@",calendarView._selectedMonthLabel.text]]) {

        if(eventsArray != nil)
        {
            [eventsArray release];
            eventsArray = nil;
        }
		eventsArray = [[NSMutableArray alloc]initWithArray:[dataContent objectForKey:calendarView._selectedMonthLabel.text]];
    }
    else
    {
        if(eventsArray != nil)
        {
            [eventsArray release];
            eventsArray = nil;
        }
        eventsArray = [[NSMutableArray alloc] init];
    }
    
    [tableView1 reloadData];
    [tableView2 reloadData];
    
    previousTile = nil;
    
    [self willAnimateRotationToInterfaceOrientation:[[UIDevice currentDevice] orientation]
                                           duration:0];
//    alphaView = [[UIView alloc]initWithFrame:CGRectMake(0,0,1024, 1024)];
//	alphaView.userInteractionEnabled = YES;
//    //	[alphaView addTarget:self action:@selector(selectr:) forControlEvents:UIControlEventTouchUpInside];
//    //	alphaView.enabled = YES;
//	alphaView.backgroundColor = [UIColor blackColor];
//	alphaView.alpha = 0.5;
    

    
//    [v1 release];
//	v1 = [[UIView alloc]initWithFrame:CGRectMake(344, 210, 380, 360)];
//	v1.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0];
//	[self.view addSubview:v1];
    
}
- (void)setupView
{   
    
    
    NSDictionary *adsDictionary = [NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath];
    NSDictionary *adsEventsDictionary = [adsDictionary objectForKey:@"events"];
    
    const float sideMargin = 30;
    const float topMargin = 20;
    const float betweenSpace = 20;
//    const float betweenNewsSpace = 40;
    const float borderSize = 24;
    
    topAd1 = [[TopAdView alloc] init];
    [topAd1 setFrame:CGRectMake(sideMargin, 
                                topMargin, 
                                (self.view.frame.size.width - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                150)];
    [topAd1 initWithImageString:[[adsEventsDictionary objectForKey:@"left"] objectForKey:@"bannerImage"]
                     andtopAdId:@"events_left"];
    [topAd1 setAutoresizingMask:UIViewAutoresizingNone];
    [topAd1 setAutoresizesSubviews:NO];
    [self.view addSubview:topAd1];
    [topAd1 release];
    
    
    topAd2 = [[TopAdView alloc] init];
    [topAd2 setFrame:CGRectMake(topAd1.frame.origin.x + topAd1.frame.size.width + betweenSpace, 
                                topMargin, 
                                (self.view.frame.size.width - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                150)];
    [topAd2 initWithImageString:[[adsEventsDictionary objectForKey:@"right"] objectForKey:@"bannerImage"]
                     andtopAdId:@"events_right"];
    [topAd2 setAutoresizingMask:UIViewAutoresizingNone];
    [topAd2 setAutoresizesSubviews:NO];
    [self.view addSubview:topAd2];
    [topAd2 release];  
    
    NSMutableDictionary *configurationDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath];
    
    [titleLabel setText:[[configurationDictionary objectForKey:@"events"] objectForKey:@"sectionTitle"]];
    [titleLabel setFrame:CGRectMake(topAd1.frame.origin.x, 
                                    topAd1.frame.origin.y + topAd1.frame.size.height + 20, 
                                    topAd1.frame.size.width + betweenSpace + topAd2.frame.size.width, 
                                    40)];
    
    calendarView = [[KLCalendarView alloc] initWithFrame:CGRectMake(sideMargin, 
                                                                    titleLabel.frame.origin.y + titleLabel.frame.size.height + 40, 
                                                                    320, 
                                                                    320) delegate:self];
	calendarView._selectedMonthLabel.userInteractionEnabled = YES;
    [self.view addSubview:calendarView];
    //[calendarView release];
    [self didChangeMonths];
    
    
    
    [tableView1 setDirectionalLockEnabled:YES];
    [tableView2 setDirectionalLockEnabled:YES];
//    [calendarView release];
//    [v release];
//	v = [[UIView alloc]initWithFrame:calendarView.frame];
//	v.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0];
//	[self.view addSubview:v];
    
    
}


#pragma mark - Orientation Support
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return YES;
}
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation 
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    const float sideMargin = 30;
    const float topMargin = 20;
    const float betweenSpace = 20;
//    const float betweenNewsSpace = 40;
    const float borderSize = 24;
    

    
    if(UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        CGRect landscapeFrame = CGRectMake(0, 0, 1024, 768 - kHeightBottomMenu - kHeightStatusBar);
        [self.view setFrame:landscapeFrame];
        [topAd1 setFrame:CGRectMake(sideMargin, 
                                    topMargin, 
                                    (1024 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                    150)];
        [topAd2 setFrame:CGRectMake(topAd1.frame.origin.x + topAd1.frame.size.width + betweenSpace, 
                                    topMargin, 
                                    (1024 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                    150)];
        [topAd1 updateContentOnOrientationChange:toInterfaceOrientation];
        [topAd2 updateContentOnOrientationChange:toInterfaceOrientation];
        
        [titleLabel setFrame:CGRectMake(topAd1.frame.origin.x, 
                                        topAd1.frame.origin.y + topAd1.frame.size.height + 20, 
                                        topAd1.frame.size.width + betweenSpace + topAd2.frame.size.width, 
                                        40)];
        
        calendarView.frame = CGRectMake(sideMargin, 
                                        titleLabel.frame.origin.y + titleLabel.frame.size.height + 20, 
                                        320, 
                                        320);
        tableView1.frame = CGRectMake(calendarView.frame.origin.x + calendarView.frame.size.width + betweenSpace,
                                      calendarView.frame.origin.y,
                                      (1024 - calendarView.frame.size.width - calendarView.frame.origin.x - betweenSpace - marginImageView.frame.size.width - betweenSpace - 10) / 2, 
                                       768 - kHeightBottomMenu - kHeightStatusBar - kHeightBottomMenu - kHeightStatusBar - topAd1.frame.origin.y - topAd1.frame.size.height - 40);
        tableView2.frame = CGRectMake(tableView1.frame.origin.x + tableView1.frame.size.width + betweenSpace,
                                      tableView1.frame.origin.y,
                                      tableView1.frame.size.width,
                                      tableView1.frame.size.height);
    }   
    else
    {
        CGRect portraitFrame = CGRectMake(0, 0, 768, 1024 - kHeightBottomMenu - kHeightStatusBar);
        [self.view setFrame:portraitFrame];
        
        [topAd1 setFrame:CGRectMake(sideMargin, 
                                    topMargin, 
                                    (768 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                    111)];
        [topAd2 setFrame:CGRectMake(topAd1.frame.origin.x + topAd1.frame.size.width + betweenSpace, 
                                    topMargin, 
                                    (768 - sideMargin * 2 - betweenSpace - borderSize) / 2, 
                                    111)];
        [topAd1 updateContentOnOrientationChange:toInterfaceOrientation];
        [topAd2 updateContentOnOrientationChange:toInterfaceOrientation];
        
        [titleLabel setFrame:CGRectMake(topAd1.frame.origin.x, 
                                        topAd1.frame.origin.y + topAd1.frame.size.height + 20, 
                                        topAd1.frame.size.width + betweenSpace + topAd2.frame.size.width, 
                                        40)];
        
        calendarView.frame = CGRectMake(sideMargin,
                                        titleLabel.frame.origin.y + titleLabel.frame.size.height + 20, 
                                        320, 
                                        320);
        tableView1.frame = CGRectMake(calendarView.frame.origin.x,
                                      calendarView.frame.origin.y + calendarView.frame.size.height + 20 ,
                                      topAd1.frame.size.width, 
                                      360);
        tableView2.frame = CGRectMake(topAd2.frame.origin.x,
                                      calendarView.frame.origin.y,
                                      topAd2.frame.size.width,
                                      calendarView.frame.size.height + 20 + tableView1.frame.size.height);
    }
//    NSLog(@"%@",NSStringFromCGRect(topAd1.frame));
//    NSLog(@"%@",NSStringFromCGRect(topAd2.frame));
}

#pragma mark - KCalendarViewDelegate Protocol

- (void)calendarView:(KLCalendarView *)calendarView1 tappedTile:(KLTile *)aTile {
    if (currentTile != aTile)
    {
//        [currentTile flashForColor:[UIColor colorWithCGColor:kCalendarBodyLightColor]];
        
        currentTile = aTile;
//        [currentTile flashForColor:[UIColor greenColor]];       
    
    
        NSMutableArray *auxiliaryArray = [[[NSMutableArray alloc] init] autorelease];
        
        NSMutableDictionary *dataContent = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathEventsPlistPath];    
        NSMutableArray *dataMonthContent;
        
        if ([[dataContent allKeys] containsObject:[NSString stringWithFormat:@"%@",calendarView._selectedMonthLabel.text]]) {		

            dataMonthContent = [[[NSMutableArray alloc] initWithArray:[dataContent objectForKey:calendarView._selectedMonthLabel.text]] autorelease];
            
            for (int j = 0; j < [dataMonthContent count]; j++)
            {
                NSArray *values = [[[dataMonthContent objectAtIndex:j] objectForKey:@"date"] componentsSeparatedByString:@"-"];
                
                KLDate *date = [[KLDate alloc] initWithYear:[[values objectAtIndex:0] intValue] 
                                                   month:[[values objectAtIndex:1] intValue] 
                                                     day:[[values objectAtIndex:2] intValue]];
                
                if ([date isEqual:[aTile date]])
                {
                    [auxiliaryArray addObject:[dataMonthContent objectAtIndex:j]];
                }
                [date release];
            }
            
            if(eventsArray != nil)
            {
                [eventsArray release];
                eventsArray = nil;
            }
            eventsArray = [[NSMutableArray alloc]initWithArray:auxiliaryArray];
        }
        else
        {
            if(eventsArray != nil)
            {
                [eventsArray release];
                eventsArray = nil;
            }
            eventsArray = [[NSMutableArray alloc]init];
        }
        [dataContent release];
    }
    else
    {
        [currentTile flashForColor:[UIColor colorWithCGColor:kCalendarBodyLightColor]];
        currentTile = nil;
        
        NSMutableDictionary *dataContent = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathEventsPlistPath];   
        if ([[dataContent allKeys] containsObject:[NSString stringWithFormat:@"%@",calendarView._selectedMonthLabel.text]]) {

            if(eventsArray != nil)
            {
                [eventsArray release];
                eventsArray = nil;
            }
            eventsArray = [[NSMutableArray alloc]initWithArray:[dataContent objectForKey:calendarView._selectedMonthLabel.text]];
        }
        else
        {
            if(eventsArray != nil)
            {
                [eventsArray release];
                eventsArray = nil;
            }
            eventsArray = [[NSMutableArray alloc] init];
        }
        [dataContent release];
    }
    [tableView1 reloadData];
    [tableView2 reloadData];
}
- (KLTile *)calendarView:(KLCalendarView *)calendarView 
       createTileForDate:(KLDate *)date {
	CheckmarkTile *tile1 = [[[CheckmarkTile alloc] init] autorelease];
	return tile1;
}
- (void)didChangeMonths {
    currentTile = nil;;
    NSMutableDictionary *dataContent = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathEventsPlistPath];
	if ([[dataContent allKeys] containsObject:[NSString stringWithFormat:@"%@",calendarView._selectedMonthLabel.text]]) {
        if(eventsArray != nil)
        {
            [eventsArray release];
            eventsArray = nil;
        }
		eventsArray = [[NSMutableArray alloc]initWithArray:[dataContent objectForKey:calendarView._selectedMonthLabel.text]];	
        
            
        for (int j = 0; j < [eventsArray count]; j++)
        {
            NSArray *values = [[[eventsArray objectAtIndex:j] objectForKey:@"date"] componentsSeparatedByString:@"-"];
            
            KLDate *date = [[KLDate alloc] initWithYear:[[values objectAtIndex:0] intValue] 
                                                      month:[[values objectAtIndex:1] intValue] 
                                                        day:[[values objectAtIndex:2] intValue]];
            
            [[calendarView tileForDate:date] flashForColor:[UIColor colorWithRed:197/255.0 green:177/255.0 blue:179/255.0 alpha:1.0]];
            [date release];
        } 
    }
    else
    {
        if(eventsArray != nil)
        {
            [eventsArray release];
            eventsArray = nil;
        }
        eventsArray = [[NSMutableArray alloc] init];
    }
    [dataContent release];
    
    [tableView1 reloadData];
    [tableView2 reloadData];
    
    CGRect frame = calendarView.frame;
    NSInteger weeks = [calendarView selectedMonthNumberOfWeeks];
    CGFloat adjustment = 0.f;
    
    switch (weeks) {
        case 4:
            adjustment = (92/321)*320+30;
            break;
        case 5:
            adjustment = (46/321)*320;
            break;
        default:
            break;
    }
    frame.size.height = 320 - adjustment;
    [calendarView setFrame:frame];
}

#pragma mark - UITableViewDataSource Protocol
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([eventsArray count] == 0)
    {
        return 0;
    }
    else if ([eventsArray count] <= 10){
        if (tableView1 == tableView)
            return [eventsArray count];
        else
            return 0;
    }
    else {
        if(tableView1 == tableView) 
            return 10;
        else 
            return [eventsArray count] - 10;
    }	
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
    }
    
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.backgroundColor = [UIColor clearColor];
    
    if (tableView == tableView1) {	
        if ([eventsArray count] > 0)
        {
            if ([[[eventsArray objectAtIndex:indexPath.row] objectForKey:@"isTitle"] isEqualToString:@"YES"])
            {
                cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
            }
            else 
            {
                cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
            }        	
            cell.textLabel.text = [[eventsArray objectAtIndex:indexPath.row] objectForKey:@"title"];
            cell.detailTextLabel.text = [[eventsArray objectAtIndex:indexPath.row] objectForKey:@"subtitle"];
        }		
	}
    else {
        if ([eventsArray count] > 10)
        {                   
            if ([[[eventsArray objectAtIndex:indexPath.row + 9] objectForKey:@"isTitle"] isEqualToString:@"YES"])
            {
                cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
            }
            else 
            {
                cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
            }        
            cell.textLabel.text = [[eventsArray objectAtIndex:indexPath.row + 9] objectForKey:@"title"];
            cell.detailTextLabel.text = [[eventsArray objectAtIndex:indexPath.row + 9] objectForKey:@"subtitle"];	
        }
	}
    return cell;
}

#pragma mark - UITableViewDelegate Protocol
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tableView1) {	
        if ([eventsArray count] > 0)
        {
            NSDictionary *dataDictionary = [eventsArray objectAtIndex:indexPath.row];

            if([[dataDictionary objectForKey:@"isTitle"] isEqualToString:@"NO"])
            {
                EventsDetailViewController *eventsDetailViewController = [[EventsDetailViewController alloc] initWithDictionary:dataDictionary];
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:eventsDetailViewController];
                
                [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
                [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [self presentModalViewController:navigationController animated:YES];
                navigationController.view.superview.bounds = CGRectMake(0, 0, 600, 400);
                [eventsDetailViewController refresh];
                [eventsDetailViewController release];
                
                [navigationController release];

            }
        }		
	}
    else {
        if ([eventsArray count] > 10)
        {      
            NSDictionary *dataDictionary = [eventsArray objectAtIndex:indexPath.row + 9];
            if([[dataDictionary objectForKey:@"isTitle"] isEqualToString:@"NO"])
            {
                EventsDetailViewController *eventsDetailViewController = [[EventsDetailViewController alloc] initWithDictionary:dataDictionary];
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:eventsDetailViewController];
                
                [navigationController setModalPresentationStyle:UIModalPresentationFormSheet];
                [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                [self presentModalViewController:navigationController animated:YES];
                navigationController.view.superview.bounds = CGRectMake(0, 0, 600, 400);
                [eventsDetailViewController refresh];
                [eventsDetailViewController release];
                
                [navigationController release];
            }
        }
	}
}

@end
