#import "TopAdView.h"
#import "MiddleAdView.h"
#import "SpecialZoneView.h"
#import "TopStoryViewCategory.h"
#import "BottomAdView.h"

@interface CategoryViewController : UIViewController {
    IBOutlet UIScrollView *categoryScrollView;
    UIScrollView *rightScrollView;
    NSDictionary *categoryAds;
    NSDictionary *categoryDescription;
    NSMutableArray *newsViewArray;
    UILabel *titleLabel;    

   	TopAdView *topAd1;
	TopAdView *topAd2; 
    MiddleAdView *middleAd;    
    SpecialZoneView *specialZoneView;   
    TopStoryViewCategory *topStoryView;    
    BottomAdView *bottomAdView;
    
    int categoryId;
    BOOL bottomAdIsUp;
}
@property (nonatomic, assign) BOOL bottomAdIsUp;

- (void)setupView;
- (id)initWithCategoryId:(int)theCategoryID;
@end
