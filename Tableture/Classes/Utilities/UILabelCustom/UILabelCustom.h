typedef enum
{
	VerticalAlignmentTop = 0, // default
	VerticalAlignmentMiddle,
	VerticalAlignmentBottom,
} VerticalAlignment;

@interface UILabelCustom : UILabel {
	
@private VerticalAlignment _verticalAlignment;
	
}

@property (nonatomic) VerticalAlignment verticalAlignment;

@end