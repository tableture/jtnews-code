#import "TabletureAppDelegate.h"
#define ApplicationDelegate (TabletureAppDelegate*)[[UIApplication sharedApplication]delegate]

#define kPathDocumentsDirectory [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define kPathConfigurationPlistPath [NSString stringWithFormat:@"%@/feeds/configuration.plist",kPathDocumentsDirectory]
#define kPathAdsPlistPath [NSString stringWithFormat:@"%@/feeds/ads.plist",kPathDocumentsDirectory]
#define kPathHomePlistPath [NSString stringWithFormat:@"%@/feeds/home.plist",kPathDocumentsDirectory]
#define kPathEventsPlistPath [NSString stringWithFormat:@"%@/feeds/events.plist",kPathDocumentsDirectory]
#define kPathSharePlistPath [NSString stringWithFormat:@"%@/share.plist",kPathDocumentsDirectory]
#define kPathSavedPlistPath [NSString stringWithFormat:@"%@/saved.plist",kPathDocumentsDirectory]
#define kPathFacebookPlistPath [NSString stringWithFormat:@"%@/facebook.plist",kPathDocumentsDirectory]

#define kWebServiceCreateSubscriptionWithClientID_PaymentPlanID_Email_FirstName_LastName_Address_City_State_Zipcode @"http://www.tableture.net/external.php?cmd=subscriber&pageAction=create&client=%@&paymentPlan=%@&email=%@&fname=%@&lname=%@&address=%@&city=%@&state=%@&zipcode=%@"
#define kWebServiceUpdateSubscriptionWithPaymentPlanID_UserID @"http://www.tableture.net/external.php?cmd=subscriber&pageAction=update&paymentPlan=%@&user_id=%@"
#define kWebServiceCancelSubscriptionWithUserID @"http://www.tableture.net/external.php?cmd=subscriber&pageAction=cancel&user_id=%@"


//#define kWebServiceConfiguration @"http://devs.eastvisionsystems.net/tableture/?feed=config"
//#define kWebServiceConfiguration @"http://217.156.73.72:8099/tableture/?feed=config"
#define kWebServiceConfiguration @"http://tableture.net/dbbestfeed/?feed=config&userID="
//#define kWebServiceConfiguration @"http://192.168.0.116:7193/eastvisionsystems/?feed=config&userID="


#define UserID @"226"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
                                                 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
                                                  blue:((float)((rgbValue & 0xFF) >> 0))/255.0 \
                                                 alpha:1.0]
#define kFeedDownloadingText @"Please wait while the app loads..."
#define kHeightBottomMenu 37
#define kHeightStatusBar 20
#define kAdDisplayTime 4.0

//#define kFacebookAppId @"247197275292598"
//#define kOAuthConsumerKey @"Vg4S8t9bP87ouPudHzLXUg"
//#define kOAuthConsumerSecret @"qpQUauVg4MriOLkLLHauPo5qV0HdIziSHo6H5Zon4"
#define kOAuthConsumerKey @"3MqUrWwv9oPSQfKYmAN3A" 
#define kOAuthConsumerSecret @"hLmXhZpt6cd5raeKhgQeQ1s15KZszDV9TPLOdDqUA"	

#define kNotificationSavedPlistChanged @"NotificationSavedPlistChanged"
#define kNotificationBottomAdTouched @"NotificationBottomAdTouched"

#define kWebServiceTimeoutInterval 100

#define kFeedEnabled 1
#define kAdsEnabled 1
#define kNewTest 0
#define kLinkEnabled 0
#define kFeedErrorDisplayingEnabled 1
#define kStoreIsLive 1