

@interface AsyncImageView : UIView {
	NSURLConnection *connection;
	NSMutableData *data;
	
	UIActivityIndicatorView *activityIndicator;
	NSString *imageName;
	NSURL *lastURL;
    
    BOOL isHome;
}

@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, assign) BOOL isHome;

- (void)loadImage:(UIImage *)img 
        withScale:(BOOL)yesOrNo;
- (void)loadImageFromURL:(NSURL *)url 
                 andName:(NSString *)name
               withScale:(BOOL)yesOrNo;
- (void)refresh;
@end
