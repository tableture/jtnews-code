#import "AsyncImageView.h"
#import "Constants.h"

@implementation AsyncImageView
@synthesize imageName;
@synthesize isHome;

#pragma mark - Memory Management
- (void)dealloc {
	[connection cancel];
//    [lastURL release];
	[data release]; 
    [imageName release];    
    [super dealloc];
}

#pragma mark - Utilities
- (UIImage *)image {
	UIImageView* imageView = nil;
    if([[self subviews] count] > 0)
    {
        imageView = [[self subviews] objectAtIndex:0];
        if([imageView isKindOfClass:[UIImageView class]])
            return [imageView image];
        else
            return nil;
    }
    else
        return nil;
}
- (UIImage *)imageByScalingAndCroppingForSize:(CGSize)targetSize
                                    withImage:(UIImage *)sourceImage {
    return sourceImage;
    
	UIImage *newImage = nil;        
	CGSize imageSize = sourceImage.size;
	CGFloat width = imageSize.width;
	CGFloat height = imageSize.height;
	CGFloat targetWidth = targetSize.width;
	CGFloat targetHeight = targetSize.height;
	CGFloat scaleFactor = 0.0;
	CGFloat scaledWidth = targetWidth;
	CGFloat scaledHeight = targetHeight;
	CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
	
	if (CGSizeEqualToSize(imageSize, targetSize) == NO) 
	{
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
		
        if (widthFactor > heightFactor) 
			scaleFactor = widthFactor; // scale to fit height
        else
			scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
		
        // center the image
        if (widthFactor > heightFactor)
		{
			thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
		}
        else 
			if (widthFactor < heightFactor)
			{
				thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
			}
	}       
	
	UIGraphicsBeginImageContext(targetSize); // this will crop
	
	CGRect thumbnailRect = CGRectZero;
	thumbnailRect.origin = thumbnailPoint;
	thumbnailRect.size.width  = scaledWidth;
	thumbnailRect.size.height = scaledHeight;
	
	[sourceImage drawInRect:thumbnailRect];
	
	newImage = UIGraphicsGetImageFromCurrentImageContext();
	
	//pop the context to get back to the default
	UIGraphicsEndImageContext();
	return newImage;
}

#pragma mark - NSURLConnection Delegate
- (void)connection:(NSURLConnection *)theConnection didReceiveResponse:(NSURLResponse *)response {
    data = [[NSMutableData alloc] init]; 
}
- (void)connection:(NSURLConnection *)theConnection 
    didReceiveData:(NSData *)incrementalData {
//	if (data == nil) 
//    { 
//        data = [[NSMutableData alloc] init]; 
//    } 
	[data appendData:incrementalData];   
//    NSLog(@"%d",[data  retainCount]);
}
- (void)connectionDidFinishLoading:(NSURLConnection*)theConnection {
//	NSLog(@"%@",imageName);
    [activityIndicator removeFromSuperview];
	activityIndicator = nil;
    
	[connection release];
	connection = nil;
	if ([self viewWithTag:100]) {
		[[self viewWithTag:100] removeFromSuperview];
	}

    [data writeToFile:[NSString stringWithFormat:@"%@/%@",kPathDocumentsDirectory,imageName] 
           atomically:YES];
    NSString *path = [NSString stringWithFormat:@"%@/%@",kPathDocumentsDirectory,imageName];
    NSLog(@"AsyncImage save %@",path);
//	UIImage *imgAux = [[UIImage alloc] initWithData:data];

    
	//UIImage *img = [[self imageByScalingAndCroppingForSize:self.frame.size withImage:imgAux] copy];  
    
//    NSData *adata = UIImagePNGRepresentation([self imageByScalingAndCroppingForSize:self.frame.size withImage:imgAux]); 
    UIImage *img = [[UIImage alloc] initWithData:data ];
//    [imgAux release];
    [data release];
	data = nil;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:img];

    [img release];
	for (UIView *x in [self subviews])
    {
        [x removeFromSuperview];
    }
	[self addSubview:imageView];
        [imageView release];
	imageView.frame = self.bounds;
    imageView.contentMode = UIViewContentModeScaleToFill;
	imageView.tag = 100;
//    [imageView release];
    

}
- (NSCachedURLResponse *)connection:(NSURLConnection *)theConnection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}
#pragma mark - Custom Methods
- (void)loadImageFromURL:(NSURL *)url 
                 andName:(NSString *)name 
               withScale:(BOOL)yesOrNo {
//    return;
    
//	if (!(lastURL != nil && ([[url absoluteString] isEqual:[lastURL absoluteString]]))) 
    {
        self.imageName = name;     
        NSString *imagePath = [NSString stringWithFormat:@"%@/%@",kPathDocumentsDirectory,imageName];
        
        if([[NSFileManager defaultManager] fileExistsAtPath:imagePath])
        {
//            NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath];
            UIImage *imageFromDisk = [[UIImage alloc] initWithData:[NSData dataWithContentsOfFile:imagePath]];
//            [imageData release];
            [self loadImage:imageFromDisk
                  withScale:yesOrNo];
            [imageFromDisk release];            
        }
        else
        {
//            if(lastURL != nil)
//            {
//                [lastURL release];
//                lastURL = nil;
//            }
            
            lastURL = [[url retain] autorelease];
            
//            if ([self viewWithTag:100]) 
//            {
//                //then this must be another image, the old one is still in subviews
//                [[self viewWithTag:100] removeFromSuperview]; //so remove it (releases it also)
//            }
//            
//            if (activityIndicator!=nil) 
//            {
//                //then this must be another image, the old one is still in subviews
//                [activityIndicator removeFromSuperview]; //so remove it (releases it also)
//                activityIndicator = nil;
//            }
//            else 
//            {
//                activityIndicator = [[UIActivityIndicatorView alloc] init];
//                activityIndicator.frame = CGRectMake((self.frame.size.width/2)-10, 
//                                                     (self.frame.size.height/2)-10, 
//                                                     20, 
//                                                     20);
//                [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
//                [activityIndicator startAnimating];
//                [self addSubview:activityIndicator];
//                [activityIndicator release];
//                activityIndicator.tag = 101;
//            }
            
//            activityIndicator.hidden = NO;
            
            if (connection!=nil) 
            { 
//                activityIndicator.hidden = YES;
                [connection cancel];
                [connection release]; 
                connection = nil;
            }
            
            if (data!=nil) 
            { 
                [data release]; 
                data = nil;
            }
            NSURLRequest* request = [NSURLRequest requestWithURL:url 
                                                     cachePolicy:NSURLRequestReloadIgnoringCacheData 
                                                 timeoutInterval:kWebServiceTimeoutInterval];
            
            connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        }   
    }
}

- (void)loadImage:(UIImage *)img 
        withScale:(BOOL)yesOrNo {
//    return;
    UIImage *imgCropped = [self imageByScalingAndCroppingForSize:self.frame.size
                                                       withImage:img];  
    UIImageView* imageView;
	if(yesOrNo)
         imageView = [[UIImageView alloc] initWithImage:imgCropped] ;
    else
        imageView = [[UIImageView alloc] initWithImage:img] ;
    
	for (UIView *x in [self subviews])
    {
        [x removeFromSuperview];
    }
	[self addSubview:imageView];
        [imageView release];
	imageView.frame = self.bounds;
	imageView.tag = 100;
//    NSLog(@"load - %@",imageName);
}



- (void)refresh
{
    if([self image] != nil)
    {
        [self loadImage:[self image]
              withScale:NO];
    }
}

#pragma mark - Touches
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isHome)
    {
        NSDictionary *dictionaryToReturn = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"topStory"];
        [[ApplicationDelegate tabletureViewController] presentNewsViewControllerWithInfoDictionary:dictionaryToReturn];
    }
    else
    {
        [super touchesEnded:touches withEvent:event];
    }
}
@end
