#import "UILabelCustom.h"


@interface Utilities : NSObject {
    
}
+ (void)fillUILabelCustom:(UILabelCustom *)label 
                 withText:(NSString *)labelText 
                  andFont:(UIFont *)font 
          andMaximumWidth:(int)labelWidth 
               andNoLines:(int)noLines ;

+ (NSString *)getImageNameFromPath:(NSString *)thePath;

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;

+ (BOOL) validateEmail: (NSString *) candidate;
+ (void) showOkAlertwithTitle: (NSString*)alertTitle
				   andMessage:(NSString*)alertMessage;
@end
