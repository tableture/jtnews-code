#import "Subscription.h"
#import "Constants.h"

@interface Subscription (Private)
-(SKProduct*)productWithIdentifier:(NSString*)productID;
-(void)unlockProductWithIdentifier:(NSString*)productID;
@end

static Subscription *sharedSubscription=nil;

@implementation Subscription
@synthesize validProducts;
@synthesize delegate;

#pragma mark - Dealloc
- (void)dealloc {
    [validProducts release];
    [requestProducts cancel];
    requestProducts.delegate=nil;
    [requestProducts release];
    //[delegate release];
    delegate = nil;
    [[SKPaymentQueue defaultQueue]removeTransactionObserver:self];
    [super dealloc];
}

#pragma mark - Lifecycle
+ (void)resetSharedSubscription {
    [sharedSubscription release];
    sharedSubscription = nil;
    [self sharedSubscription];
}
+ (Subscription *)sharedSubscription {
    if (!sharedSubscription) 
    {
        sharedSubscription=[[Subscription alloc] init];
    }    
    return sharedSubscription;
}
+ (NSSet*)tranzactionIdentifiersSet {  
    NSMutableSet *productsSet = [[NSMutableSet alloc] init];
    
    if(kStoreIsLive)
    {
        
        NSMutableDictionary *configurationDictionary = [[NSMutableDictionary alloc]initWithContentsOfFile:kPathConfigurationPlistPath];
        NSLog(@"%@",configurationDictionary);
        NSMutableArray *productsArray = [[NSMutableArray alloc]initWithArray:[configurationDictionary objectForKey:@"products"]];
        NSLog(@"%@",productsArray);
        [configurationDictionary release];
        
        for(int i=0; i < [productsArray count]; ++i)
        {
            [productsSet addObject:[[productsArray objectAtIndex:i ]objectForKey:@"productID"]];
        }
        [productsArray release];
    }
    
//    NSLog(@"%@",productsSet);
//    [ApplicationDelegate setProductsArrayFromConfigurationFeed:productsSet];
    return [productsSet autorelease];
}
- (void)customInitialization {
	[[SKPaymentQueue defaultQueue]addTransactionObserver:self];
    
    [requestProducts release];
    requestProducts=[[SKProductsRequest alloc] initWithProductIdentifiers:[Subscription tranzactionIdentifiersSet]];
    requestProducts.delegate=self;
}
- (id)init {
    
    self = [super init];
    
    if (self) {
        [self customInitialization];
    }
    return self;
}

#pragma mark - Interface
- (void)makePaymentForIdentifier:(NSString*)productID {
    if ([SKPaymentQueue canMakePayments] == NO)
    {
        UIAlertView *unableToMakePaymentsAlert = [[UIAlertView alloc] initWithTitle:@"Can't make Payments" message:@"This account can't make payments." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [unableToMakePaymentsAlert show];
        [unableToMakePaymentsAlert release];
    }
    else
    {
        SKPayment *newBuy = [SKPayment paymentWithProductIdentifier:productID];        
        if (newBuy) 
        {
            NSLog(@"product id passed %@",newBuy);
            [[SKPaymentQueue defaultQueue] addPayment:newBuy];
        }
        else
        {
            UIAlertView *unableToCreatePaymentAlert = [[UIAlertView alloc] initWithTitle:@"Could't create Payment" message:@"Sorry, there was an error when creating the payment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            
            [unableToCreatePaymentAlert show];
            [unableToCreatePaymentAlert release];
        }
    }
}
- (void) cancelRequest
{
    [requestProducts cancel];
}

- (void)getProducts {
    [requestProducts start];
}

#pragma mark - Utilities
+ (NSString *)priceForProductWithIdentifier:(NSString*)productID {
    SKProduct *product = [sharedSubscription productWithIdentifier:productID];
    
    if (product == nil)
        return  nil;
    
    NSNumberFormatter *numberFormatter = [[[NSNumberFormatter alloc] init] autorelease];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.priceLocale];
    NSString *formattedString = [numberFormatter stringFromNumber:product.price];
    
    return [formattedString length]?formattedString:nil;
}
- (SKProduct *)productWithIdentifier:(NSString*)productID {
    for (SKProduct* newProduct in validProducts) 
    {
        if ([newProduct.productIdentifier isEqualToString:productID])
        {
            return newProduct;
        }
    }    
    return nil;
}

#pragma mark - SKProductsRequestDelegate Delegate
- (void)productsRequest:(SKProductsRequest *)request 
     didReceiveResponse:(SKProductsResponse *)response {

    NSLog(@"product request did receive response");
    BOOL YESorNO;
    self.validProducts = response.products;
    
    NSLog(@"first %@",[request description]);
    NSLog(@"error %@",[response description]);
    
    if([validProducts count])
    {
        YESorNO = NO;
    }
	if ([response.invalidProductIdentifiers count]) 
    {
        YESorNO = YES;
        NSLog(@"invalid %@",response.invalidProductIdentifiers);
    }
	[delegate subscriptionDelegateDidFinishSearchingProducts:self 
                                                   withError:YESorNO];
}

#pragma mark - SKPayment Actions
-(void)unlockProductWithIdentifier:(NSString*)productID
{
    NSString *udid = [[UIDevice currentDevice] uniqueIdentifier];
    NSString *keyString = [udid stringByAppendingString:@"340x480p"];
    NSData *keyData = [keyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    NSString *unlockString = @"Puchased";
    NSData *unlockData = [unlockString dataUsingEncoding:NSUTF8StringEncoding];
    NSData *unlockDataE = [unlockData encryptedWithKey:keyData];
    
    [userDefaults setObject:unlockDataE forKey:[NSString stringWithFormat:@"SK%@",productID]];
    [userDefaults synchronize];
}

+(BOOL)checkIfUserHasBoughtProductWithIdentifier:(NSString*)productID
{
    if (kStoreUnlockAllProducts) 
    {
        return YES;
    }
    
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    
    id dataObject = [userDefaults objectForKey:[NSString stringWithFormat:@"SK%@",productID]];
    if ([dataObject isKindOfClass:[NSData class]]) 
    {
        NSString *udid = [[UIDevice currentDevice] uniqueIdentifier];
        NSString *keyString = [udid stringByAppendingString:@"340x480p"];
        NSData *keyData = [keyString dataUsingEncoding:NSUTF8StringEncoding];
        
        NSData *unlockDataE = (NSData*)dataObject;
        NSData *unlockData = [unlockDataE decryptedWithKey:keyData];
        NSString *unlockString = [[[NSString alloc] initWithData:unlockData encoding:NSUTF8StringEncoding] autorelease];
        
        if ([unlockString isEqualToString:@"Puchased"])
        {
            return YES;
        }        
    }    
    return NO;
}

+ (void)restorePreviousBoughtItems
{
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

#pragma mark - SKPaymentTransactionObserver Delegate
- (void)paymentQueue:(SKPaymentQueue *)queue 
 updatedTransactions:(NSArray *)transactions {
	for(SKPaymentTransaction *aTransaction in transactions)
    {
        NSLog(@"error %@",[[aTransaction  error] localizedDescription]);
		switch (aTransaction.transactionState) 
        {
			case SKPaymentTransactionStatePurchased:
            {
                NSLog(@"SKPaymentTransactionStatePurchased - ttt");
                [[NSNotificationCenter defaultCenter] postNotificationName:SKTransactionPurchased 
                                                                    object:aTransaction];
                
                [self unlockProductWithIdentifier:aTransaction.payment.productIdentifier];

                NSLog(@"finish transaction");
                [[SKPaymentQueue defaultQueue] finishTransaction:aTransaction];
				break;
            }
			case SKPaymentTransactionStateFailed:
            {
				NSLog(@"SKPaymentTransactionStateFailed");
                [[NSNotificationCenter defaultCenter] postNotificationName:SKTransactionFailed 
                                                                    object:aTransaction];
                
                
				[[SKPaymentQueue defaultQueue] finishTransaction:aTransaction];
				break;
            }
			case SKPaymentTransactionStatePurchasing:
            {
                NSLog(@"SKPaymentTransactionStatePurchasing");
                [[NSNotificationCenter defaultCenter] postNotificationName:SKTransactionPurchasing 
                                                                    object:aTransaction];
				
				break;
            }
			case SKPaymentTransactionStateRestored:
            {
				NSLog(@"SKPaymentTransactionStateRestored");
                [[NSNotificationCenter defaultCenter] postNotificationName:SKTransactionRestored 
                                                                    object:aTransaction];
                
                [self unlockProductWithIdentifier:aTransaction.payment.productIdentifier];
                NSLog(@"finish transaction");
				[[SKPaymentQueue defaultQueue] finishTransaction:aTransaction];
				break;
            }
			default:
				break;
		}
	}
}

@end
