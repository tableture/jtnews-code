#import <StoreKit/StoreKit.h>
#import "NSData+AES.h"

//Store Constants
#define kStoreUnlockAllProducts 0

//Notification Constants
#define SKTransactionPurchased      @"SKTransactionPurchased"
#define SKTransactionRestored       @"SKTransactionRestored"
#define SKTransactionFailed         @"SKTransactionFailed"
#define SKTransactionPurchasing     @"SKTransactionPurchasing"

@class Subscription;

@protocol SubscriptionDelegate <NSObject>
- (void)subscriptionDelegateDidFinishSearchingProducts:(Subscription *)theSubscription 
                                             withError:(BOOL)YESorNO;
@end

@interface Subscription : NSObject <SKPaymentTransactionObserver, SKProductsRequestDelegate> {
    SKProductsRequest *requestProducts;
    NSArray           *validProducts;
    
    id <SubscriptionDelegate> delegate;
}
@property(nonatomic,assign) id <SubscriptionDelegate> delegate;
@property(nonatomic,retain) NSArray *validProducts;

#pragma mark - Lifecycle
+ (void)resetSharedSubscription;
+ (Subscription *)sharedSubscription;

#pragma mark - Utilities
+ (NSString*)priceForProductWithIdentifier:(NSString*)productID;

#pragma mark - Inteface
- (void)getProducts;
- (void)makePaymentForIdentifier:(NSString*)productID;
- (void) cancelRequest;

+(BOOL)checkIfUserHasBoughtProductWithIdentifier:(NSString*)productID;
@end
