//
//  Utilities.m
//  Tableture
//
//  Created by Octav Chelaru on 7/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Utilities.h"


@implementation Utilities
+ (void)fillUILabelCustom:(UILabelCustom *)label 
                 withText:(NSString *)labelText 
                  andFont:(UIFont *)font 
          andMaximumWidth:(int)labelWidth
               andNoLines:(int)noLines {	
	CGSize sizeForALine = [labelText sizeWithFont:font];
	CGSize sizeToMakeLabel = [labelText sizeWithFont:font 
                                   constrainedToSize:CGSizeMake(labelWidth, sizeForALine.height*noLines) 
                                       lineBreakMode:UILineBreakModeWordWrap];
	if (sizeToMakeLabel.width < labelWidth) {
        sizeToMakeLabel.width = labelWidth-12;
    }
    [label setFrame:CGRectMake(label.frame.origin.x,
                               label.frame.origin.y, 
                               sizeToMakeLabel.width+12, 
                               sizeToMakeLabel.height)];
	label.text = labelText;
	label.backgroundColor = [UIColor clearColor];	
    label.numberOfLines = noLines;
    //label.lineBreakMode = UILineBreakModeWordWrap;
	[label setFont:font];	
}

+ (NSString *)getImageNameFromPath:(NSString *)thePath {
    NSArray *componentsArray = [thePath componentsSeparatedByString:@"/"];
    if ([componentsArray count] > 0)
    {
        return [componentsArray lastObject];
    }
    else {
        return @"";
    }
}

+ (UIColor *)colorWithHexString:(NSString *)stringToConvert {
	NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
	
	if ([cString length] < 6) 
    {
        // String should be 6 or 8 characters
        return [UIColor blackColor];
    }    
	
	if ([cString hasPrefix:@"#"]) 
    {
        // strip # if it appears
        cString = [cString substringFromIndex:1];
    }
	if ([cString length] != 6) 
    {
        return [UIColor blackColor];        
    }
    
	// Separate into r, g, b substrings
	NSRange range;
	range.location = 0;
    
	range.length = 2;
	NSString *rString = [cString substringWithRange:range];
    
	range.location = 2;
	NSString *gString = [cString substringWithRange:range];
    
	range.location = 4;
	NSString *bString = [cString substringWithRange:range];
    
	// Scan values
	unsigned int r, g, b;
	[[NSScanner scannerWithString:rString] scanHexInt:&r];
	[[NSScanner scannerWithString:gString] scanHexInt:&g];
	[[NSScanner scannerWithString:bString] scanHexInt:&b];
	
	return [UIColor colorWithRed:((float) r / 255.0f)
						   green:((float) g / 255.0f)
							blue:((float) b / 255.0f)
						   alpha:1.0f];
}
+ (BOOL) validateEmail: (NSString *) candidate 
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    return [emailTest evaluateWithObject:candidate];
}


+ (void) showOkAlertwithTitle: (NSString*)alertTitle
				   andMessage:(NSString*)alertMessage
{
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertTitle
														message:alertMessage
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView  show];
	[alertView release];
}


@end
