#import "MiddleAdView.h"
#import "Constants.h"

@implementation MiddleAdView
@synthesize imageView;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) 
    {
    }
    return self;
}
- (void)initWithImageString:(NSString *)imageString
                    andAdId:(NSString *)theAdId
{
    adId = [theAdId retain];
	backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 
                                                             0,
                                                             self.frame.size.width, 
                                                             self.frame.size.height)];
//    backgroundView.backgroundColor = [UIColor colorWithRed:134/255.0 
//                                                     green:154/255.0 
//                                                      blue:205/255.0 
//                                                     alpha:1.0];
    [backgroundView setBackgroundColor:[UIColor clearColor]];
	[self addSubview:backgroundView];
	[backgroundView release];
    
    imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(2,
                                                                2, 
                                                                self.frame.size.width-4, 
                                                                self.frame.size.height-4)];
    
    NSArray *content = [imageString componentsSeparatedByString:@"/"];
    NSString *name = [content objectAtIndex:[content count]-1];
    [imageView loadImageFromURL:[NSURL URLWithString:imageString] 
                      andName:name
                    withScale:YES];	
	[self addSubview:imageView];
    [imageView release];
	
}
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation {
	[backgroundView setBackgroundColor:[UIColor clearColor]];
	backgroundView.frame = CGRectMake(0,
                                      0,
                                      self.frame.size.width, 
                                      self.frame.size.height);
    	imageView.frame = CGRectMake(5, 
                                 5,
                                 self.frame.size.width-10, 
                                 self.frame.size.height-10);
    [imageView refresh];
    backgroundView.backgroundColor = [UIColor colorWithRed:134/255.0 
                                                     green:154/255.0 
                                                      blue:205/255.0 
                                                     alpha:1.0];
    
}

- (void)dealloc {
//    [imageView release];
    [adId release];
    [super dealloc];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{   
    NSDictionary *adsDictionary = [[NSDictionary alloc] initWithContentsOfFile:kPathAdsPlistPath];
    NSArray *array = [adId componentsSeparatedByString:@"_"];
    if([array count] == 2)
    {
        NSDictionary *dictionaryToSend = [[[[adsDictionary objectForKey:@"sections"] objectForKey:[array objectAtIndex:1]] objectForKey:@"middle"] retain];
        [adsDictionary release];
        [[ApplicationDelegate tabletureViewController] presentAdViewControllerWithInfoDictionary:[dictionaryToSend autorelease]];
    }
    else
    {
        NSDictionary *dictionaryToSend = [[[adsDictionary objectForKey:@"home"] objectForKey:@"middle"] retain];
        [adsDictionary release];
        [[ApplicationDelegate tabletureViewController] presentAdViewControllerWithInfoDictionary:[dictionaryToSend autorelease]];
    }

}

@end

