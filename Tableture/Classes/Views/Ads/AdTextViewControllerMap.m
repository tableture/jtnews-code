//
//  AdTextViewControllerMap.m
//  Tableture
//
//  Created by Octav Chelaru on 8/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AdTextViewControllerMap.h"


@implementation AdTextViewControllerMap

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithLatitude:(NSString *)theLatitude
          andLongitude:(NSString *)theLongitude
            andAddress:(NSString *)theAddress
{
    self = [super init];
    if (self)
    {
        latitude = [theLatitude retain];
        longitude = [theLongitude retain];
        address = [theAddress retain];
    }
    return self;
}
- (void)dealloc
{
    [mapView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    self.title = address;
    
	mapView.mapType = MKMapTypeStandard;
	CLLocationCoordinate2D coord;
    coord.latitude = [latitude floatValue];
    coord.longitude = [longitude floatValue];
	MKCoordinateSpan span;
    span.latitudeDelta = 0.05;
    span.longitudeDelta = 0.05;
	MKCoordinateRegion region = {coord, span};
	
	[mapView setRegion:region];
	MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coord addressDictionary:nil];
	[mapView addAnnotation:placemark];
    [placemark release];
//	[self.view sendSubviewToBack:mapView];
}
- (void)viewDidUnload
{
    [mapView release];
    mapView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

@end
