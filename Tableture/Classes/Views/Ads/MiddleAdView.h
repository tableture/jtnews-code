#import "Utilities.h"
#import "AsyncImageView.h"

@interface MiddleAdView : UIView {
    UIView *backgroundView;
    AsyncImageView *imageView;
    NSString *adId;
}
@property (nonatomic, retain) AsyncImageView *imageView;
- (void)initWithImageString:(NSString *)imageString
                    andAdId:(NSString *)theAdId;
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;
@end