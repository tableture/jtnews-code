#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AdTextViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    
    IBOutlet UILabel *headlineLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *addressLabel;
    IBOutlet UILabel *phoneLabel;
    IBOutlet UIButton *emailButton;
    IBOutlet UIButton *webButton;
    IBOutlet UIButton *mapButton;
    
    NSDictionary *adDictionary;
}
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (IBAction)emailButtonPressed;
- (IBAction)webButtonPressed;
- (IBAction)mapButtonPressed;
- (void)refresh;

#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject;
@end
