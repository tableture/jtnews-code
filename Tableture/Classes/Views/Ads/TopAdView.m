#import "TopAdView.h"
#import "Constants.h"

@implementation TopAdView
@synthesize imageView;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) 
    {
    }
    return self;
}
- (void)initWithImageString:(NSString *)imageString
                 andtopAdId:(NSString *)theTopAdId
{
    topAdId = [theTopAdId retain];
	backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 
                                                             0,
                                                             self.frame.size.width, 
                                                             self.frame.size.height)];
    [backgroundView setBackgroundColor:[UIColor clearColor]];
	[self addSubview:backgroundView];
	[backgroundView release];
    
    imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(2,
                                                                2, 
                                                                self.frame.size.width-4, 
                                                                self.frame.size.height-4)];
    NSArray *content = [imageString componentsSeparatedByString:@"/"];
    NSString *name = [content objectAtIndex:[content count]-1];
    [imageView loadImageFromURL:[NSURL URLWithString:imageString] 
                        andName:name
                      withScale:YES];
	[self addSubview:imageView];
    [imageView release];
	
}
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation {
    [backgroundView setBackgroundColor:[UIColor clearColor]];
	backgroundView.frame = CGRectMake(0,
                                      0,
                                      self.frame.size.width, 
                                      self.frame.size.height);
    imageView.frame = CGRectMake(5, 
                                 5,
                                 self.frame.size.width-10, 
                                 self.frame.size.height-10);
    [imageView  refresh];
    backgroundView.backgroundColor = [UIColor colorWithRed:134/255.0 
                                                     green:154/255.0 
                                                      blue:205/255.0 
                                                     alpha:1.0];
}

- (void)dealloc {
    [topAdId release];
    [super dealloc];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSArray *array = [topAdId componentsSeparatedByString:@"_"];
    if([array count] == 3)
    {
        [[ApplicationDelegate tabletureViewController] presentAdViewControllerWithInfoDictionary:[[[[NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath] objectForKey:@"sections"] objectForKey:[array objectAtIndex:1]] objectForKey:[array objectAtIndex:2]]]; 
    }
    else
    {
        [[ApplicationDelegate tabletureViewController] presentAdViewControllerWithInfoDictionary:[[[NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath] objectForKey:@"events"] objectForKey:[array objectAtIndex:1]]];
    }}

@end

