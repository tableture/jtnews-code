#import "AsyncImageView.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AdTextImageViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    
    IBOutlet UILabel *headlineLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *addressLabel;
    IBOutlet UILabel *phoneLabel;
    IBOutlet UIButton *emailButton;
    IBOutlet UIButton *webButton;
    IBOutlet UIButton *mapButton;
    IBOutlet UIView *adImageFrame;
    AsyncImageView *adImage;    
    NSDictionary *adDictionary;
}
- (IBAction)emailButtonPressed;
- (IBAction)webButtonPressed;
- (IBAction)mapButtonPressed;
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (void)refresh;

#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject;
@end
