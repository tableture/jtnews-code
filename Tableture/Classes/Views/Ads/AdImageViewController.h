#import "AsyncImageView.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AdImageViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    
    IBOutlet UIView *adImageFrame;
	IBOutlet UIButton *emailButton;
    IBOutlet UIButton *mapButton;
    IBOutlet UIButton *webButton;
    AsyncImageView *adImage;
    NSDictionary *adDictionary;
}

- (IBAction)emailButtonPressed;
- (IBAction)webButtonPressed;
- (IBAction)mapButtonPressed;

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (void)refresh;

#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject;
@end
