#import "Utilities.h"
#import "AsyncImageView.h"

@interface TopAdView : UIView {
    UIView *backgroundView;
    AsyncImageView *imageView;
    NSString *topAdId;
}
@property (nonatomic, retain) AsyncImageView *imageView;
- (void)initWithImageString:(NSString *)imageString
                 andtopAdId:(NSString *)theAdId;
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;
@end