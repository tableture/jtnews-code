#import "Utilities.h"
#import "AsyncImageView.h"

@interface BottomAdView : UIView {
    UIView *backgroundView;
    AsyncImageView *imageView;
}
@property (nonatomic, retain) AsyncImageView *imageView;
- (void)initWithImagePath:(NSString *)imagePath;
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;
@end