//
//  AdTextViewController.m
//  Tableture
//
//  Created by Octav Chelaru on 8/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AdTextViewController.h"
#import "AdTextViewControllerMap.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation AdTextViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        adDictionary = [dictionary retain];
            }
    return self;
}
- (void)dealloc
{
    [adDictionary release];
    [headlineLabel release];
    [descriptionLabel release];
    [addressLabel release];
    [phoneLabel release];
    [emailButton release];
    [webButton release];
    [mapButton release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([[adDictionary allKeys] containsObject:@"email"] == NO)
        [emailButton setHidden:YES];
    else if ([[adDictionary objectForKey:@"email"] isEqualToString:@""])
        [emailButton setHidden:YES];
    if([[adDictionary allKeys] containsObject:@"website"] == NO)
        [webButton setHidden:YES];
    else if ([[adDictionary objectForKey:@"website"] isEqualToString:@""])
        [webButton setHidden:YES];
    if(([[adDictionary allKeys] containsObject:@"latitude"] == NO) || ([[adDictionary allKeys] containsObject:@"longitude"] == NO))
        [mapButton setHidden:YES];
    else if ([[adDictionary objectForKey:@"latitude"] isEqualToString:@""] && [[adDictionary objectForKey:@"longitude"] isEqualToString:@""])
        [mapButton setHidden:YES];
}

- (void)viewDidUnload
{
    [headlineLabel release];
    headlineLabel = nil;
    [descriptionLabel release];
    descriptionLabel = nil;
    [addressLabel release];
    addressLabel = nil;
    [phoneLabel release];
    phoneLabel = nil;
    [emailButton release];
    emailButton = nil;
    [webButton release];
    webButton = nil;
    [mapButton release];
    mapButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)close 
{
    [self dismissModalViewControllerAnimated:YES];
}
- (void)refresh
{
    [emailButton.layer setMasksToBounds:YES];
	[emailButton.layer setCornerRadius:10.0];
	[webButton.layer setMasksToBounds:YES];
	[webButton.layer setCornerRadius:10.0];
	[mapButton.layer setMasksToBounds:YES];
	[mapButton.layer setCornerRadius:10.0];
    
    [headlineLabel setText:[adDictionary objectForKey:@"headline"]];
    [descriptionLabel setText:[adDictionary objectForKey:@"description"]];
    
    NSString *address = [adDictionary objectForKey:@"locationAdress"];
    NSString *city = [adDictionary objectForKey:@"city"];
    NSString *state = [adDictionary objectForKey:@"state"];
    NSString *zipcode = [adDictionary objectForKey:@"zipCode"];
    
    if (![city hasPrefix:@"No need"] && ![state hasPrefix:@"No need"] && ![zipcode hasPrefix:@"No need"])
        address = [address stringByAppendingFormat:@", %@, %@, %@",city,state,zipcode];
    
    [addressLabel setText:address];
    [phoneLabel setText:[adDictionary objectForKey:@"phone"]];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" 
                                                                    style:UIBarButtonItemStylePlain 
                                                                   target:self 
                                                                   action:@selector(close)];
    [self.navigationItem setLeftBarButtonItem:closeButton];
    [closeButton release];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (IBAction)emailButtonPressed {
            NSString *applicationTitle = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"headTitle"];
    
    /*[self sendEmailWithRecipients:[NSArray arrayWithObject:[[NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath] objectForKey:@"linkEmail"]]
                          andBody:@"" 
                       andSubject:applicationTitle];*/
    [self sendEmailWithRecipients:[NSArray arrayWithObject:[adDictionary objectForKey:@"email"]]
                          andBody:@"" 
                       andSubject:applicationTitle];
	

}

- (IBAction)webButtonPressed {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[adDictionary objectForKey:@"website"]]];
}

- (IBAction)mapButtonPressed {
    AdTextViewControllerMap *adTextViewControllerMap = [[AdTextViewControllerMap alloc] 
                                                        initWithLatitude:[adDictionary objectForKey:@"latitude"] 
                                                        andLongitude:[adDictionary objectForKey:@"longitude"]
                                                        andAddress:[adDictionary objectForKey:@"locationAdress"]];
    [self.navigationController pushViewController:adTextViewControllerMap 
                                         animated:YES];
    [adTextViewControllerMap release];
}



#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            [picker setSubject:subject];
            [picker setToRecipients:recipients];
            [picker setMessageBody:body isHTML:NO];
            
            [self presentModalViewController:picker animated:YES];
            [picker release];            
        }
        else
        {
            NSString *stringWithData =[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",[recipients objectAtIndex:0], subject,body];
            NSString *url = [stringWithData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];	
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];            
        }
    }
    else
    {
        NSString *stringWithData =[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",[recipients objectAtIndex:0], subject,body];
        NSString *url = [stringWithData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];	
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }    
}

#pragma mark - MFMailComposeViewControllerDelegate Protocol
- (void)mailComposeController:(MFMailComposeViewController *)controller 
          didFinishWithResult:(MFMailComposeResult)result 
                        error:(NSError *)error {	
	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled: {
			break;
        }
            
		case MFMailComposeResultSaved: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done"
                                                                message:@"The e-mail has been saved!"
                                                               delegate:nil 
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil];
			[alertView show];
			[alertView release];
            break;
		}
			
		case MFMailComposeResultSent: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done"
                                                                message:@"The e-mail has been sent!"
                                                               delegate:nil 
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil]; 
			[alertView show];
			[alertView release];
            break;
		}
			
		case MFMailComposeResultFailed: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:[NSString stringWithFormat:@"The e-mail has failed with error: %@", [error localizedDescription]]
                                                               delegate:nil
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil]; 
			[alertView show];
			[alertView release];
            break;
		}
		default: {
			break;
        }
	}
	
	[self dismissModalViewControllerAnimated:YES];
}

@end
