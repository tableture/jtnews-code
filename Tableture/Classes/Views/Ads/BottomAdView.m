#import "BottomAdView.h"
#import "Constants.h"

@implementation BottomAdView
@synthesize imageView;

- (void)initWithImagePath:(NSString *)imagePath
{
	backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 
                                                             0,
                                                             self.frame.size.width, 
                                                             self.frame.size.height)];
    backgroundView.backgroundColor = [UIColor colorWithRed:134/255.0 
                                                     green:154/255.0 
                                                      blue:205/255.0 
                                                     alpha:1.0];
    [backgroundView setBackgroundColor:[UIColor clearColor]];
	[self addSubview:backgroundView];
	[backgroundView release];
    
    imageView = [[AsyncImageView alloc]initWithFrame:CGRectMake(2,
                                                                2, 
                                                                self.frame.size.width-4, 
                                                                self.frame.size.height-4)];
  
    NSData *imageData = [[NSData alloc] initWithContentsOfFile:imagePath];
    
    UIImage *imag = [[UIImage alloc]initWithData:imageData];
    [imageData release];
    [imageView loadImage:imag withScale:NO];
	[imag release];
    [self addSubview:imageView];
    [imageView release];
	
}
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation {
	[backgroundView setBackgroundColor:[UIColor clearColor]];
	backgroundView.frame = CGRectMake(0,
                                      0,
                                      self.frame.size.width, 
                                      self.frame.size.height);
    imageView.frame = CGRectMake(5, 
                                 5,
                                 self.frame.size.width-10, 
                                 self.frame.size.height-10);
    [imageView refresh];
    backgroundView.backgroundColor = [UIColor colorWithRed:134/255.0 
                                                     green:154/255.0 
                                                      blue:205/255.0 
                                                     alpha:1.0];    
}

- (void)dealloc {
//    [backgroundView release];
//    [imageView release];
    [super dealloc];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBottomAdTouched object:nil];
}

@end

