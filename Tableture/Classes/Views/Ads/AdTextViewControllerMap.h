#import <MapKit/MKMapView.h>
#import <MapKit/MKPlacemark.h>

@interface AdTextViewControllerMap : UIViewController {
    
    IBOutlet MKMapView *mapView;
    NSString *latitude;
    NSString *longitude;
    NSString *address;
}
- (id)initWithLatitude:(NSString *)theLatitude
          andLongitude:(NSString *)theLongitude 
            andAddress:(NSString *)theAddress;
@end
