#import "AsyncImageView.h"

@interface SavedStoriesViewControllerCell : UITableViewCell {
    AsyncImageView *newsThumbImageView;
    
    IBOutlet UILabel *newsTitleLabel;
    IBOutlet UILabel *newsTeaserLabel;
    IBOutlet UILabel *newsDateLabel;
    
}
@property (nonatomic, retain) IBOutlet UILabel *newsTitleLabel;
@property (nonatomic, retain) IBOutlet UILabel *newsTeaserLabel;
@property (nonatomic, retain) IBOutlet UILabel *newsDateLabel;
@property (nonatomic, retain) AsyncImageView *newsThumbImageView;

- (NSString *)reuseIdentifier;
@end
