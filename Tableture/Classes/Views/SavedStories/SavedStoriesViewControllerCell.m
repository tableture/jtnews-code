//
//  SavedStoriesViewControllerCell.m
//  Tableture
//
//  Created by Octav Chelaru on 8/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SavedStoriesViewControllerCell.h"


@implementation SavedStoriesViewControllerCell
@synthesize newsTitleLabel;
@synthesize newsTeaserLabel;
@synthesize newsDateLabel;
@synthesize newsThumbImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        newsThumbImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 85 - 80, 
                                                                              5, 
                                                                              80, 
                                                                              80)];
        [newsThumbImageView setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin];
        [self addSubview:newsThumbImageView];
        [newsThumbImageView release];
        [[self contentView] setBackgroundColor:[UIColor colorWithRed:134/255.0 green:154/255.0 blue:205/255.0 alpha:1.0]];
    }
    return self;
}
- (void)dealloc
{
    [newsTitleLabel release];
    [newsTeaserLabel release];
    [newsDateLabel release];
    [super dealloc];
}
- (NSString *)reuseIdentifier
{
    return @"SavedStoriesViewControllerCell";
}
@end
