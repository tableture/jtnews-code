#import "UILabelCustom.h"
#import "Utilities.h"

@interface SavedStoriesView : UIView {
    
	UILabelCustom *headlineTitle;
	
	UIImageView *img;
	
	UIView *backgroundView1;
    UIView *backgroundView2;
}

- (void)updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation;
@end
