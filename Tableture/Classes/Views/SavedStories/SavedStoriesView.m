//
//  SavedStoriesView.m
//  Tableture
//
//  Created by Octav Chelaru on 8/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SavedStoriesView.h"
#import "Constants.h"

@implementation SavedStoriesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//       	
        backgroundView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 
                                                                   0,
                                                                   self.frame.size.width, 
                                                                   self.frame.size.height)];
        backgroundView2.backgroundColor = [UIColor colorWithRed:134/255.0 
                                                          green:154/255.0 
                                                           blue:205/255.0 
                                                          alpha:1.0];
        [self addSubview:backgroundView2];
        [backgroundView2 release];
//        
        backgroundView1 = [[UIView alloc] initWithFrame:CGRectMake(2, 
                                                                   2,
                                                                   self.frame.size.width-4, 
                                                                   self.frame.size.height-4)];
        backgroundView1.backgroundColor = [UIColor colorWithRed:197/255.0 
                                                          green:177/255.0 
                                                           blue:179/255.0 
                                                          alpha:1.0];
        [self addSubview:backgroundView1];
        [backgroundView1 release];
        img = [[UIImageView alloc]initWithFrame:CGRectMake(20, 23, 32, 32)];
        img.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"book_open" ofType:@"png"]];
        [self addSubview:img];
        [img release];
        
//        
        headlineTitle = [[UILabelCustom alloc] initWithFrame:CGRectMake(60, 28,self.frame.size.width-40,30)];
        [Utilities fillUILabelCustom:headlineTitle 
                            withText:@"Read Saved Articles" 
                             andFont:[UIFont boldSystemFontOfSize:22] 
                     andMaximumWidth:self.frame.size.width-40 
                          andNoLines:1];			
        [self addSubview:headlineTitle];
        [headlineTitle release];
    }
    return self;
}

-(void) updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation
{
	
	if (UIInterfaceOrientationIsLandscape(newOrientation))	{

		
		backgroundView2.frame = CGRectMake(0, 0,self.frame.size.width, self.frame.size.height);
		backgroundView2.backgroundColor = [UIColor colorWithRed:134/255.0 green:154/255.0 blue:205/255.0 alpha:1.0];
		
		backgroundView1.frame = CGRectMake(2, 2,self.frame.size.width-4, self.frame.size.height-4);
        //	backgroundView1.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0];
		
        
		
	}
	
	else {
		
		
		
		backgroundView2.frame = CGRectMake(0, 0,self.frame.size.width, self.frame.size.height);
		backgroundView2.backgroundColor = [UIColor colorWithRed:134/255.0 green:154/255.0 blue:205/255.0 alpha:1.0];
		
		backgroundView1.frame = CGRectMake(2, 2,self.frame.size.width-4, self.frame.size.height-4);
        //	backgroundView1.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0];
        
		
	}
	
	
	
	
}

- (void)dealloc
{    

    [super dealloc];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[ApplicationDelegate tabletureViewController] presentSavedStoriesViewController];

}
@end
