//
//  SavedStoriesViewController.m
//  Tableture
//
//  Created by Octav Chelaru on 8/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SavedStoriesViewController.h"
#import "Constants.h"
#import "NewsViewController.h"

@implementation SavedStoriesViewController
@synthesize savedStoriesViewControllerCell;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [list release];
    [newsTableView release];
//    [savedStoriesViewControllerCell release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)close
{
    [self dismissModalViewControllerAnimated:YES];
}
- (void)edit
{
    if([newsTableView isEditing])
    {
        [newsTableView setEditing:NO];
    }
    else
    {
        [newsTableView setEditing:YES];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    newsTableView.delegate = self;
    newsTableView.dataSource = self;
    list = [[NSMutableArray alloc] initWithArray:[[NSDictionary dictionaryWithContentsOfFile:kPathSavedPlistPath] objectForKey:@"list"]];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" 
                                                                    style:UIBarButtonItemStylePlain 
                                                                   target:self 
                                                                   action:@selector(close)];
    [self.navigationItem setLeftBarButtonItem:closeButton];
    [closeButton release];
    UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(edit)];
    [self.navigationItem setRightBarButtonItem:editButton];
    [editButton release];
    
    self.title = @"Saved Stories";
}
- (void)viewDidUnload
{
    [newsTableView release];
    newsTableView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

#pragma mark - UITableViewDataSource Protocol
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    return [list count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    static NSString *CellIdentifier = @"SavedStoriesViewControllerCell";
        
    savedStoriesViewControllerCell = (SavedStoriesViewControllerCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (savedStoriesViewControllerCell == nil) {
        savedStoriesViewControllerCell = [[[[NSBundle mainBundle] loadNibNamed:@"SavedStoriesViewControllerCell" owner:self options:nil] objectAtIndex:0] autorelease];
        
        NSDictionary *savedNewsDictionary = [list objectAtIndex:indexPath.row];
        
        savedStoriesViewControllerCell.newsTitleLabel.text = [savedNewsDictionary objectForKey:@"title"];
        savedStoriesViewControllerCell.newsTeaserLabel.text = [savedNewsDictionary objectForKey:@"teaser"];
        savedStoriesViewControllerCell.newsDateLabel.text = [savedNewsDictionary objectForKey:@"saved"];

        NSString *imageString = [savedNewsDictionary objectForKey:@"thumb"];
        NSArray *content = [imageString componentsSeparatedByString:@"/"];
        NSString *name = [content objectAtIndex:[content count]-1];
        [savedStoriesViewControllerCell.newsThumbImageView loadImageFromURL:[NSURL URLWithString:imageString]
                                                                    andName:name
                                                                  withScale:YES];        
    }
    return savedStoriesViewControllerCell;
}

#pragma mark - UITableViewDelegate Protocol
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90.0;}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsViewController *newsViewController = [[NewsViewController alloc] initWithDictionary:[list objectAtIndex:indexPath.row]];
//    [newsViewController setNewsDictionary:[list objectAtIndex:indexPath.row]];
//    [newsViewController setModalPresentationStyle:UIModalPresentationPageSheet];
//    [newsViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//    [self presentModalViewController:newsViewController
//                            animated:YES];
//    [newsViewController setNewsNavigationBar:self.navigationController.navigationBar];
    [self.navigationController pushViewController:newsViewController
                                         animated:YES];
    [newsViewController release];
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewCellEditingStyleDelete;
}
- (void)tableView:(UITableView *)tableView 
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
forRowAtIndexPath:(NSIndexPath *)indexPath {
    [list removeObjectAtIndex:indexPath.row];
    [newsTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    NSString *saved;
    if ([list count] > 0)
    {
        saved = @"YES";
    }
    else
    {
        saved = @"NO";
    }
    
	NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathSavedPlistPath];
    [dictionary setObject:saved forKey:@"saved"];
    [dictionary setObject:list forKey:@"list"];
    [dictionary writeToFile:kPathSavedPlistPath atomically:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSavedPlistChanged object:nil];
    
    if ([saved isEqualToString:@"NO"])
    {
        [self close];
    }
}
@end
