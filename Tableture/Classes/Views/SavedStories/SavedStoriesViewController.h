//
//  SavedStoriesViewController.h
//  Tableture
//
//  Created by Octav Chelaru on 8/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SavedStoriesViewControllerCell.h"


@interface SavedStoriesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet SavedStoriesViewControllerCell *savedStoriesViewControllerCell;
    IBOutlet UITableView *newsTableView;
    
    NSMutableArray *list;
}
@property (nonatomic, retain)  IBOutlet SavedStoriesViewControllerCell *savedStoriesViewControllerCell;
@end
