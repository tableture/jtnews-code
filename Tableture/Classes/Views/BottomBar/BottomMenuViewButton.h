#import "BottomMenuViewData.h"
#import "Utilities.h"


@protocol BottomMenuViewButtonDelegate
- (void)buttonTouchWithIndex:(NSInteger)index;
@end

@interface BottomMenuViewButton : UIView {	
	NSString *contentId;	
    NSMutableData *imageData;
	id <BottomMenuViewButtonDelegate> delegate;	
	int index;
	BOOL imageFinished;
}

@property (nonatomic, assign) id <BottomMenuViewButtonDelegate> delegate;
@property (nonatomic, readonly) NSString *contentId;


- (void)initButtonWithData:(BottomMenuViewData *)data 
                  andIndex:(int)theIndex;
- (void)setButtonState:(NSString *)state ;


@end
