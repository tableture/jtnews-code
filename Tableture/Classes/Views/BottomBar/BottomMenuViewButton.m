#import "BottomMenuViewButton.h"
#import "UILabelCustom.h"
#import "Constants.h"

@implementation BottomMenuViewButton

@synthesize delegate; 
@synthesize contentId;

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
		self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)initButtonWithData:(BottomMenuViewData *)data 
                  andIndex:(int)theIndex {
	index = theIndex; 
	contentId = data.contentId;	
	NSString *title = data.title;
    
	if ([data.title isEqualToString:@"Settings"])
    {
		UIImage *image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"png"]];
		UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
		int x = (self.frame.size.width - image.size.width)/2;
		int y = 3; // top Padding
		[imageView setFrame:CGRectMake(x, 
                                       y, 
                                       image.size.width,
                                       image.size.height)];		
		[self addSubview:imageView];
        [imageView release];
		title = @"";
	}

	UILabelCustom *titleLabel = [[UILabelCustom alloc] init];
    [Utilities fillUILabelCustom:titleLabel 
                        withText:title 
                         andFont:[UIFont boldSystemFontOfSize:16.0] 
                 andMaximumWidth:self.frame.size.width+24 
                      andNoLines:1];

	
	UIColor *color = [[UIColor alloc] initWithRed:(240.0/255.0) 
                                            green:(240.0/255.0) 
                                             blue:(240.0/255.0) 
                                            alpha:1];			
	[titleLabel setTextColor:color];	
	int y = (self.frame.size.height - titleLabel.frame.size.height - titleLabel.frame.size.height/2);
	[titleLabel setFrame:CGRectMake(12, 
                                    y, 
                                    titleLabel.
                                    frame.size.width+12, 
                                    titleLabel.frame.size.height)];
	[self addSubview:titleLabel];    
	[color release];
	[titleLabel release];
}


- (void)setButtonState:(NSString *)state {
	if([state isEqualToString:@"normal"]) 
    {		
		for (UILabelCustom *title in self.subviews) 
        {
			if([title isKindOfClass:[UILabelCustom class]]) 
            {
                title.textColor = [UIColor whiteColor];
            }
		}
	}
	else 
    {
		for (UILabelCustom *title in self.subviews)
        {
            if([title isKindOfClass:[UILabelCustom class]])
            {
              title.textColor = [UIColor colorWithRed:171/255.0 green:171/255.0 blue:171/255.0 alpha:1.0];  
            }            
        }	
	}
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if(([[ApplicationDelegate tabletureViewController] currentTabIndex] == index) || ([[ApplicationDelegate tabletureViewController] timerIsValid] == YES))
//    if([[ApplicationDelegate tabletureViewController] timerIsValid] == YES)
    {
        return;
    }
    else
    {
        [delegate buttonTouchWithIndex:index];
        [[ApplicationDelegate tabletureViewController] setCurrentTabIndex:index];
    }
	
}


- (void)dealloc {
    [super dealloc];
}


@end
