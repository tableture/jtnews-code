#import "BottomMenuViewData.h"

@implementation BottomMenuViewData
@synthesize colorDark;
@synthesize colorLight;
@synthesize title;
@synthesize contentId;
@synthesize thumbPath;

- (void)dealloc {
    [colorDark release];
    [colorLight release];
    [title release];
    [contentId release];
    [thumbPath release];
    [super dealloc];
}

@end
