#import "BottomMenuView.h"

@implementation BottomMenuView

#pragma mark - Properties
@synthesize delegate;
@synthesize touchedIndex;
@synthesize scrollView;

#pragma mark - View Lifecycle

-(void) refreshScrollFrame
{
    scrollView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
		UIColor *color = [[UIColor alloc] initWithRed:(5.0/255.0) green:(5.0/255.0) blue:(5.0/255.0) alpha:1];			
		self.backgroundColor = color;
        [color release];
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:scrollView];
        scrollView.delaysContentTouches = YES;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        //[scrollView release];
    }
    return self;
}

#pragma mark - Setup
- (void)initButtonsByArray:(NSArray *) btnTitles {
	totalWidth = 0;
	int i;    
	
	for (i=0; i<[btnTitles count]; i++) {
		int x = totalWidth;
		BottomMenuViewData *c = (BottomMenuViewData*)[btnTitles objectAtIndex:i];
		NSString *labelText = c.title;
		CGSize sizeToMakeLabel = [labelText sizeWithFont:[UIFont boldSystemFontOfSize:16.0] constrainedToSize:CGSizeMake(150,self.frame.size.height) lineBreakMode:UILineBreakModeWordWrap];
        
		
		BottomMenuViewButton *button = [[BottomMenuViewButton alloc] initWithFrame:CGRectMake(x, 0, sizeToMakeLabel.width+24, self.frame.size.height)];
		[button initButtonWithData:(BottomMenuViewData *)[btnTitles objectAtIndex:i] andIndex:i];
		button.delegate = self;
		button.alpha = 0;
		button.backgroundColor = [UIColor whiteColor];
        
		
		UIView *v = [[UIView alloc]initWithFrame:CGRectMake(x+1, 0, sizeToMakeLabel.width+22, self.frame.size.height)];
		v.backgroundColor = UIColor.clearColor;
		CAGradientLayer *gradient = [CAGradientLayer layer];
		gradient.frame = v.bounds;
        [v release];
		gradient.colors = [NSArray arrayWithObjects:
                           (id)[[Utilities colorWithHexString:c.colorLight] CGColor],
                           (id)[[Utilities colorWithHexString:c.colorDark] CGColor], 
                           nil];
		
		[button.layer insertSublayer:gradient atIndex:0];
		
		// Update the totalWidth:
		totalWidth += button.frame.size.width;
		
		// Add Button
		[scrollView addSubview:button];
		
		// Clear data:
		[button release];
	}
    if (totalWidth > 700)
    {
        scrollView.contentSize = CGSizeMake(totalWidth, scrollView.frame.size.height);
    }
	[self adjustButtonsPosition];	
}
- (void)adjustButtonsPosition {	
    //Called first time and on orientation changes
	int posDiff = self.bounds.size.width - totalWidth;
	int diffForBtn = posDiff/2;
	int startWihtX = diffForBtn - ([scrollView.subviews count]+1) * 1;
	int i;
	
	NSMutableArray *array = [[NSMutableArray alloc] init]; 
	
	for (UIView *v in scrollView.subviews)
	{
		if ([v isKindOfClass:[BottomMenuViewButton class]])
        {
          [array addObject:v];  
        }
	}		
	
	for (i=0; i<[array count]; i++) 
    {		        
		BottomMenuViewButton *button = [scrollView.subviews objectAtIndex:i];	
		
        // Make sure is visible:
		button.alpha = 1;
		
		// Adjust HORIZONTAL position:		
		if (i==0) 
        {		
            if ((self.bounds.size.width == 768 && totalWidth > 768) || (self.bounds.size.width == 1024 && totalWidth > 1024))
                button.frame = CGRectMake(0, 
                                          button.frame.origin.y, 
                                          button.frame.size.width, 
                                          button.frame.size.height);
            else
                button.frame = CGRectMake(startWihtX, 
                                          button.frame.origin.y, 
                                          button.frame.size.width, 
                                          button.frame.size.height);
		}
        else 
        {
			BottomMenuViewButton *prevButton = [array objectAtIndex:i-1];
			button.frame = CGRectMake((prevButton.frame.origin.x + prevButton.frame.size.width), 
                                      button.frame.origin.y, 
                                      button.frame.size.width, 
                                      button.frame.size.height);
		}
	}
    [array release];
    
	CAGradientLayer *gradient = [CAGradientLayer layer];
	gradient.frame = self.bounds;
	gradient.colors = [NSArray arrayWithObjects:
                       (id)[[UIColor darkGrayColor] CGColor],
                       (id)[[UIColor blackColor] CGColor], 
                       nil];
	[self.layer insertSublayer:gradient 
                       atIndex:0];	
}

#pragma mark - Selecting
- (void)deselectAllButtons {
	int i;
	NSMutableArray *array = [[NSMutableArray alloc]init]; 
	
	for (UIView *v in scrollView.subviews)
	{
		if ([v isKindOfClass:[BottomMenuViewButton class]])[array addObject:v];
	}		 
	
	for (i=0; i<[array count]; i++) {
		[(BottomMenuViewButton*)[array objectAtIndex:i] setButtonState:@"normal"];		
	}
    [array release];
}
- (void)blockInterfaceForInterval:(float)seconds {
    self.userInteractionEnabled = NO;
    [self performSelector:@selector(setUserInteractionEnabled:) withObject:@"yes" afterDelay:seconds];
}
- (void)selectButtonWithIndex:(NSInteger) index {
    /*
     *	Called to brodcaster farward the TouchEvent
     */
    [self blockInterfaceForInterval:0.1];
	// Deselec All Buttons:
	[self deselectAllButtons];
	
	// Select the Touched Button:
	[(BottomMenuViewButton*)[scrollView.subviews objectAtIndex:index] setButtonState:@"selected"];
	
	// Brodcast Message to Delegate:
 	[delegate bottomMenuTouchWithIndex:index andContentId:((BottomMenuViewButton*)[scrollView.subviews objectAtIndex:index]).contentId];	
}
- (void)changeSelection {
    [self selectButtonWithIndex:touchedIndex];
}
- (void)buttonTouchWithIndex:(NSInteger)index {
    touchedIndex = index;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(changeSelection) object:nil];
	[self performSelector:@selector(changeSelection) withObject:nil afterDelay:0.2];	
}

#pragma mark - Memory Management
- (void)dealloc {
    for(BottomMenuViewButton *button in [scrollView subviews])
    {
        [button removeFromSuperview];
    }
    [scrollView release];
//    delegate = nil;
    [super dealloc];
}


@end
