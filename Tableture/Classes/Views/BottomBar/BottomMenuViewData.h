@interface BottomMenuViewData : NSObject {
	
	NSString *title;
	NSString *contentId;
	NSString *thumbPath;
	NSString *colorDark;	
	NSString *colorLight;
	
}

@property (nonatomic, retain) NSString *colorLight;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *colorDark;
@property (nonatomic, retain) NSString *contentId;
@property (nonatomic, retain) NSString *thumbPath;

@end
