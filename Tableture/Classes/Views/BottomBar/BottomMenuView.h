#import <QuartzCore/QuartzCore.h>
#import "BottomMenuViewButton.h"
#import "BottomMenuViewData.h"

@protocol BottomMenuViewDelegate
- (void)bottomMenuTouchWithIndex:(NSInteger)index 
                    andContentId:(NSString *) contentId;
@end


@interface BottomMenuView : UIView <BottomMenuViewButtonDelegate> {
    int totalWidth;	
    int touchedIndex;
    UIScrollView *scrollView;
	id <BottomMenuViewDelegate> delegate;
}
@property (nonatomic, assign) id <BottomMenuViewDelegate> delegate;
@property (nonatomic, assign) int touchedIndex;
@property (nonatomic, retain) UIScrollView *scrollView;

#pragma mark - Setup
- (void)initButtonsByArray:(NSArray *)btnTitles;
- (void)adjustButtonsPosition;
-(void) refreshScrollFrame;

#pragma mark - Selecting
- (void)deselectAllButtons;
- (void)blockInterfaceForInterval:(float)seconds;
- (void)selectButtonWithIndex:(NSInteger)index;
- (void)changeSelection;
- (void)buttonTouchWithIndex:(NSInteger)index;

@end
