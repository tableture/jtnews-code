#import "EventsDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"

@implementation EventsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        eventDictionary = [dictionary retain];
    }
    return self;
}
- (void)dealloc
{
    [eventMapView release];
    [titleLabel release];
    [dateLabel release];
    [addressLabel release];
    [descriptionLabel release];
    [priceLabel release];
    [emailButton release];
    [webButton release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)close 
{
    [self dismissModalViewControllerAnimated:YES];
}
- (void)refresh
{
    [emailButton.layer setMasksToBounds:YES];
	[emailButton.layer setCornerRadius:10.0];
	[webButton.layer setMasksToBounds:YES];
	[webButton.layer setCornerRadius:10.0];
    
    [titleLabel setText:[eventDictionary objectForKey:@"subtitle"]];
    [dateLabel setText:[eventDictionary objectForKey:@"date"]];
    [addressLabel setText:[[eventDictionary objectForKey:@"content"] objectForKey:@"address"]];
    [priceLabel setText:[[eventDictionary objectForKey:@"content"] objectForKey:@"price"]];
    [descriptionLabel setText:[[eventDictionary objectForKey:@"content"] objectForKey:@"decription"]];
    
    CLLocationCoordinate2D coord;
    coord.latitude = [[[eventDictionary objectForKey:@"content"] objectForKey:@"latitude"] floatValue];
    coord.longitude = [[[eventDictionary objectForKey:@"content"] objectForKey:@"longitude"] floatValue];
    eventMapView.centerCoordinate = coord;
    
    MKCoordinateSpan span;
    span.latitudeDelta = 2.5;
    span.longitudeDelta = 2.5;
    
    MKCoordinateRegion region;
    region.span = span;
    region.center = coord;    
    eventMapView.region = region;
    
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coord
                                                   addressDictionary:nil];
    [eventMapView addAnnotation:placemark];
    [placemark release];

    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Close" 
                                                                    style:UIBarButtonItemStylePlain 
                                                                   target:self 
                                                                   action:@selector(close)];
    [self.navigationItem setLeftBarButtonItem:closeButton];
    [closeButton release];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [eventMapView release];
    eventMapView = nil;
    [titleLabel release];
    titleLabel = nil;
    [dateLabel release];
    dateLabel = nil;
    [addressLabel release];
    addressLabel = nil;
    [descriptionLabel release];
    descriptionLabel = nil;
    [priceLabel release];
    priceLabel = nil;
    [emailButton release];
    emailButton = nil;
    [webButton release];
    webButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

- (IBAction)emailButtonPressed {
    NSString *applicationTitle = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"headTitle"];
    
    [self sendEmailWithRecipients:[NSArray arrayWithObjects:[[NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath] objectForKey:@"linkEmail"],nil] 
                          andBody:@"" 
                       andSubject:applicationTitle];
}

- (IBAction)websiteButtonPressed {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath] objectForKey:@"linkSite"]]];
}


#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject
{
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            [picker setSubject:subject];
            [picker setToRecipients:recipients];
            [picker setMessageBody:body isHTML:NO];
            
            [self presentModalViewController:picker animated:YES];
            [picker release];            
        }
        else
        {
            NSString *stringWithData =[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",[recipients objectAtIndex:0], subject,body];
            NSString *url = [stringWithData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];	
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];            
        }
    }
    else
    {
        NSString *stringWithData =[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",[recipients objectAtIndex:0], subject,body];
        NSString *url = [stringWithData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];	
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }    
}

#pragma mark - MFMailComposeViewControllerDelegate Protocol
- (void)mailComposeController:(MFMailComposeViewController *)controller 
          didFinishWithResult:(MFMailComposeResult)result 
                        error:(NSError *)error {	
	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled: {
			break;
        }
            
		case MFMailComposeResultSaved: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done"
                                                                message:@"The e-mail has been saved!"
                                                               delegate:nil 
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil];
			[alertView show];
			[alertView release];
            break;
		}
			
		case MFMailComposeResultSent: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done"
                                                                message:@"The e-mail has been sent!"
                                                               delegate:nil 
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil]; 
			[alertView show];
			[alertView release];
            break;
		}
			
		case MFMailComposeResultFailed: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:[NSString stringWithFormat:@"The e-mail has failed with error: %@", [error localizedDescription]]
                                                               delegate:nil
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil]; 
			[alertView show];
			[alertView release];
            break;
		}
		default: {
			break;
        }
	}
	
	[self dismissModalViewControllerAnimated:YES];
}
@end
