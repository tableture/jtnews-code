
#import <MapKit/MapKit.h>
#import <MapKit/MKReverseGeocoder.h>
#import <MapKit/MKPlacemark.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface EventsDetailViewController : UIViewController <MFMailComposeViewControllerDelegate> {
    
    IBOutlet MKMapView *eventMapView;
    IBOutlet UILabel *titleLabel;
    IBOutlet UILabel *dateLabel;
    IBOutlet UILabel *addressLabel;
    IBOutlet UILabel *descriptionLabel;
    IBOutlet UILabel *priceLabel;
    
    IBOutlet UIButton *emailButton;
    IBOutlet UIButton *webButton;
    
    NSDictionary *eventDictionary;
}

- (id)initWithDictionary:(NSDictionary *)dictionary;
- (IBAction)emailButtonPressed;
- (IBAction)websiteButtonPressed;
- (void)refresh;

#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject;
@end
