//
//  NewestNewsCategoryView.m
//  Tableture
//
//  Created by Octav Chelaru on 7/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewestNewsCategoryView.h"
#import "Constants.h"

@implementation NewestNewsCategoryView
@synthesize categoryID;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

- (void)initWithTitle:(NSString*)title 
           andTeaser1:(NSString*)teaser1 
           andTeaser2:(NSString*)teaser2 
           andTeaser3:(NSString*)teaser3 
                andID:(int)theID {
	
	categoryID = theID;
    
    //-------------------
	backgroundView = [[UIView alloc] initWithFrame:CGRectMake(2, 
                                                              2,
                                                              self.frame.size.width, 
                                                              self.frame.size.height)];
    backgroundView.backgroundColor = [UIColor colorWithRed:197/255.0 
                                                     green:177/255.0 
                                                      blue:179/255.0 
                                                     alpha:1.0];    
    [backgroundView.layer setMasksToBounds:YES];
    [backgroundView.layer setBorderWidth:2.0];
    [backgroundView.layer setBorderColor:[[UIColor colorWithRed:134/255.0 
                                                          green:154/255.0 
                                                           blue:205/255.0 
                                                          alpha:1.0] CGColor]];
    
//    backgroundView.userInteractionEnabled = YES;
	[self addSubview:backgroundView];
    [backgroundView release];	
//	self.userInteractionEnabled = YES;						  
    //-------------------
    
    //-------------------
    titleString = title;
	titleLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(8, 
                                                                 7,
                                                                 self.frame.size.width-10,
                                                                 22)];
    [Utilities fillUILabelCustom:titleLabel 
                        withText:titleString
                        andFont:[UIFont fontWithName:@"Georgia-Bold" size:22] 
                 andMaximumWidth:self.frame.size.width-10
                      andNoLines:1];
    [self addSubview:titleLabel];
    [titleLabel release];
    //-------------------
    
    //-------------------
    teaser1String = teaser1;
    teaser1Label = [[UILabelCustom alloc] initWithFrame:CGRectMake(8,
                                                                   33,
                                                                   self.frame.size.width-10,
                                                                   24)];
    [Utilities fillUILabelCustom:teaser1Label 
                        withText:teaser1String 
                         andFont:[UIFont boldSystemFontOfSize:18] 
                 andMaximumWidth:self.frame.size.width - 10
                      andNoLines:1];
    [self addSubview:teaser1Label];
    [teaser1Label release];
    //-------------------
    
    //-------------------
    teaser2String = teaser2;
    teaser2Label = [[UILabelCustom alloc] initWithFrame:CGRectMake(8,
                                                                   59,
                                                                   self.frame.size.width-10,
                                                                   24)];
    [Utilities fillUILabelCustom:teaser2Label 
                        withText:teaser2String 
                         andFont:[UIFont boldSystemFontOfSize:18] 
                 andMaximumWidth:self.frame.size.width - 10
                      andNoLines:1];
    [self addSubview:teaser2Label];
    [teaser2Label release];
    //-------------------
    
    //-------------------
    teaser3String = teaser3;
    teaser3Label = [[UILabelCustom alloc] initWithFrame:CGRectMake(8,
                                                                   85,
                                                                   self.frame.size.width-10,
                                                                   24)];
    [Utilities fillUILabelCustom:teaser3Label 
                        withText:teaser3String 
                         andFont:[UIFont boldSystemFontOfSize:18] 
                 andMaximumWidth:self.frame.size.width - 10
                      andNoLines:1];
    [self addSubview:teaser3Label];
    [teaser3Label release];
    //-------------------
}

-(void) updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation {
	
    if(UIInterfaceOrientationIsPortrait(newOrientation))
    {
        [backgroundView setFrame:CGRectMake(2, 
                                            2,
                                            self.frame.size.width, 
                                            self.frame.size.height)];

        [titleLabel setFrame:CGRectMake(8, 
                                        7,
                                        self.frame.size.width - 10,
                                        22)];
        [Utilities fillUILabelCustom:titleLabel 
                            withText:titleString
                             andFont:[UIFont fontWithName:@"Georgia-Bold" size:22] 
                     andMaximumWidth:self.frame.size.width-10
                          andNoLines:1];

        [teaser1Label setFrame:CGRectMake(8,
                                          33,
                                          self.frame.size.width - 10,
                                          24)];
        [Utilities fillUILabelCustom:teaser1Label 
                            withText:teaser1String 
                             andFont:[UIFont boldSystemFontOfSize:18] 
                     andMaximumWidth:self.frame.size.width - 10
                          andNoLines:1];

        [teaser2Label setFrame:CGRectMake(8,
                                          59,
                                          self.frame.size.width - 10,
                                          24)];
        [Utilities fillUILabelCustom:teaser2Label 
                            withText:teaser2String 
                             andFont:[UIFont boldSystemFontOfSize:18] 
                     andMaximumWidth:self.frame.size.width - 10
                          andNoLines:1];

        [teaser3Label setFrame:CGRectMake(8,
                                          85,
                                          self.frame.size.width - 10,
                                          24)];
        [Utilities fillUILabelCustom:teaser3Label 
                            withText:teaser3String 
                             andFont:[UIFont boldSystemFontOfSize:18] 
                     andMaximumWidth:self.frame.size.width - 10
                          andNoLines:1];

    }
    else
    {
        [backgroundView setFrame:CGRectMake(2, 
                                            2,
                                            self.frame.size.width, 
                                            self.frame.size.height)];
        
        [titleLabel setFrame:CGRectMake(8, 
                                        7,
                                        self.frame.size.width - 10,
                                        22)];
        [Utilities fillUILabelCustom:titleLabel 
                            withText:titleString
                             andFont:[UIFont fontWithName:@"Georgia-Bold" size:22] 
                     andMaximumWidth:self.frame.size.width - 10
                          andNoLines:1];
        
        [teaser1Label setFrame:CGRectMake(8,
                                          33,
                                          self.frame.size.width - 16,
                                          24)];
        [Utilities fillUILabelCustom:teaser1Label 
                            withText:teaser1String 
                             andFont:[UIFont boldSystemFontOfSize:16] 
                     andMaximumWidth:self.frame.size.width - 16
                          andNoLines:1];
        
        [teaser2Label setFrame:CGRectMake(8,
                                          59,
                                          self.frame.size.width - 16,
                                          24)];
        [Utilities fillUILabelCustom:teaser2Label 
                            withText:teaser2String 
                             andFont:[UIFont boldSystemFontOfSize:16] 
                     andMaximumWidth:self.frame.size.width - 16
                          andNoLines:1];
        
        [teaser3Label setFrame:CGRectMake(8,
                                          85,
                                          self.frame.size.width - 16,
                                          24)];
        [Utilities fillUILabelCustom:teaser3Label 
                            withText:teaser3String 
                             andFont:[UIFont boldSystemFontOfSize:16] 
                     andMaximumWidth:self.frame.size.width - 16
                          andNoLines:1];
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[ApplicationDelegate tabletureViewController] jumpToCategoryWithCategoryID:categoryID];
}
@end
