#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "MiddleAdView.h"

#import "NewsHTMLParser.h"

#import "Facebook.h"

#import "SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"

//@class NewsViewController;
//@protocol NewsViewControllerDelegate <NSObject>
//
//-(void)newsClosed;
//
//@end

@interface NewsViewController : UIViewController 
<UIWebViewDelegate, 
MFMailComposeViewControllerDelegate,
NewsHTMLParserDelegate, 
FBRequestDelegate, 
FBSessionDelegate, 
SA_OAuthTwitterControllerDelegate, 
SA_OAuthTwitterEngineDelegate> {
    
    IBOutlet UIButton *saveButton;
    IBOutlet UIScrollView *containerScrollView;

    UIWebView *newsWebView;
    UIAlertView *theAlertView;
    UIAlertView *twitterAlertView;
    NSDictionary *newsDictionary;
    NewsHTMLParser *newsHTMLParser;
    
    MiddleAdView *leftAd;
    MiddleAdView *rightAd;
    
    //Facebook
    Facebook *_facebook;
	NSArray *_permissions;
    
    //Twitter
    SA_OAuthTwitterEngine *_twitter;
    
    //BOOLs
    BOOL didPostedOnFacebook;
    
    IBOutlet UIActivityIndicatorView *waintingSpinner;
    IBOutlet UILabel *waitingLabel;
    
//    id <NewsViewControllerDelegate> delegate;
}

//@property (nonatomic, assign)     id <NewsViewControllerDelegate> delegate;
@property (nonatomic, retain) NSDictionary *newsDictionary;
- (id)initWithDictionary:(NSDictionary *)dictionary;

#pragma mark - IBActions
//- (IBAction)closeButtonPressed:(id)sender;
- (IBAction)facebookButtonPressed;
- (IBAction)twitterButtonPressed;
- (IBAction)emailButtonPressed;
- (IBAction)saveButtonPressed;

#pragma mark - Facebook Methods
- (void)facebookLogin;
- (void)facebookPost;
- (void)facebookReauthorize;
+ (NSDictionary*)createWallPostWithParameters:(NSString*)appID andLink:(NSString*)link andPictureURL:(NSString*)pictureURL andName:(NSString*)name andCaption:(NSString*)caption andDescription:(NSString*)description andMessage:(NSString*)message;

#pragma mark - Twitter Methods
- (void)twitterLogin;
- (void)twitterPost;

#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                        andBody:(NSString *)body 
                     andSubject:(NSString *)subject;
@end
