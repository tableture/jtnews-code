#import "TopStoryViewCategory.h"
#import "Constants.h"


@implementation TopStoryViewCategory


- (void)dealloc
{
    [newsId release];
    [titleString release];
    [teaserString release];
    [imageString release];
    [super dealloc];
}

- (void)initWithTitle:(NSString *)title 
            andTeaser:(NSString *)teaser 
             andImage:(NSString *)image
                andId:(NSString *)theId
{
    newsId = [theId retain];
    titleString = [title retain];
    teaserString = [teaser retain];
    imageString = [image retain];
	
	//-------------------
	titleLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 6, self.frame.size.width - 10, 30)];
    [Utilities fillUILabelCustom:titleLabel 
                        withText:titleString 
                         andFont:[UIFont boldSystemFontOfSize:20]
                 andMaximumWidth:self.frame.size.width - 10 
                      andNoLines:1];
    [self addSubview:titleLabel];
    [titleLabel release];
    //-------------------
    
    //-------------------
    teaserLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 
                                                                  titleLabel.frame.origin.y + titleLabel.frame.size.height + 8, 
                                                                  self.frame.size.width - 130, 
                                                                  90)];
    [Utilities fillUILabelCustom:teaserLabel 
                        withText:teaserString 
                         andFont:[UIFont boldSystemFontOfSize:12]
                 andMaximumWidth:self.frame.size.width - 130 
                      andNoLines:7];
    [self addSubview:teaserLabel];
    [teaserLabel release];
    //-------------------
    
    
    //-------------------
	imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 130 - 5, 
                                                                 titleLabel.frame.origin.y + titleLabel.frame.size.height + 8, 
                                                                 130, 
                                                                 130)];
    NSArray *content = [imageString componentsSeparatedByString:@"/"];
    NSString *name = [content objectAtIndex:[content count]-1];
    [imageView loadImageFromURL:[NSURL URLWithString:imageString] 
                        andName:name
                      withScale:YES];
    [self addSubview:imageView];    
	[imageView release];
    //-------------------
}


- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation {
    
	if (UIInterfaceOrientationIsLandscape(newOrientation))
	{
        titleLabel.frame = CGRectMake(6, 6, self.frame.size.width - 15 - 150, 30);
        [Utilities fillUILabelCustom:titleLabel
                            withText:titleString
                             andFont:[UIFont boldSystemFontOfSize:26] 
                     andMaximumWidth:self.frame.size.width - 15 - 150
                          andNoLines:2];
        
        teaserLabel.frame = CGRectMake(5, 
                                       titleLabel.frame.origin.y + titleLabel.frame.size.height + 10, 
                                       self.frame.size.width - 30 - 150, 
                                       90);
        [Utilities fillUILabelCustom:teaserLabel
                            withText:teaserString
                             andFont:[UIFont boldSystemFontOfSize:13] 
                     andMaximumWidth:self.frame.size.width - 30 - 150
                          andNoLines:5];
        
        [imageView setFrame:CGRectMake(self.frame.size.width - 150 - 5, 
                                       5, 
                                       150, 
                                       150)];
        [imageView refresh];
	}	
	else 
    {
        titleLabel.frame = CGRectMake(6, 
                                      6, 
                                      self.frame.size.width - 20 - 120, 
                                      30);
        [Utilities fillUILabelCustom:titleLabel
                            withText:titleString
                             andFont:[UIFont boldSystemFontOfSize:20] 
                     andMaximumWidth:self.frame.size.width - 20 - 120
                          andNoLines:2];
        
        teaserLabel.frame = CGRectMake(6, 
                                       titleLabel.frame.origin.y + titleLabel.frame.size.height + 10, 
                                       self.frame.size.width - 30 - 120, 
                                       80);
        [Utilities fillUILabelCustom:teaserLabel
                            withText:teaserString
                             andFont:[UIFont boldSystemFontOfSize:12] 
                     andMaximumWidth:self.frame.size.width - 30 - 120 
                          andNoLines:5];
        
        [imageView setFrame:CGRectMake(self.frame.size.width - 120 - 5, 
                                       5, 
                                       120, 
                                       120)];
        [imageView refresh];
	}    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {    
    int totalCategories = [[[NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath] objectForKey:@"categoriesNumber"] intValue];
    
    BOOL found = NO;
    
    for(int i=0; i < totalCategories && !found; i++)
    {
        NSString *path = [NSString stringWithFormat:@"%@/feeds/section_%d.plist",kPathDocumentsDirectory,i];
       
        NSDictionary *topStoryDictionary = [[NSDictionary dictionaryWithContentsOfFile:path] objectForKey:@"topStory"];
        
        if([newsId isEqualToString:[topStoryDictionary objectForKey:@"id"]])
        {
            [[ApplicationDelegate tabletureViewController] presentNewsViewControllerWithInfoDictionary:topStoryDictionary];
            found = YES;
        }
    }
}
@end
