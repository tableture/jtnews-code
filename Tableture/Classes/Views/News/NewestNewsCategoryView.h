#import <QuartzCore/QuartzCore.h>
#import "Utilities.h"

@interface NewestNewsCategoryView : UIView {
    UILabelCustom *titleLabel;
	UILabelCustom *teaser1Label;
	UILabelCustom *teaser2Label;
	UILabelCustom *teaser3Label;    
	NSString *titleString;
	NSString *teaser1String;
	NSString *teaser2String;
	NSString *teaser3String;
    
	UIView *backgroundView;
    
    int categoryID;
}
@property (nonatomic,assign) int categoryID;

- (void)initWithTitle:(NSString*)title 
           andTeaser1:(NSString*)teaser1 
           andTeaser2:(NSString*)teaser2 
           andTeaser3:(NSString*)teaser3 
                andID:(int)theID;

- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;
@end

