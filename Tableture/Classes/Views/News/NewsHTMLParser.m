#import "NewsHTMLParser.h"
#import "Constants.h"

@implementation NewsHTMLParser
@synthesize delegate;
@synthesize didFinished;
#pragma mark - NSURLConnection Work
- (void)startDownloading {
    if([imagesArray count]==0)
    {
        [imagesArray release];
        if (delegate)
        { 
            didFinished = YES;
            [delegate newsHTMLParserDidFinishedDownloadingImages:self];
        }
    }
    else 
    {
        currentDownloadingImage++;
        
        NSString *URLString = [NSString stringWithString:[imagesArray objectAtIndex:currentDownloadingImage]];
        NSURL *URL = [[NSURL alloc] initWithString:URLString];
        NSURLRequest *URLRequest = [NSURLRequest requestWithURL:URL 
                                                    cachePolicy:NSURLRequestReloadIgnoringCacheData 
                                                timeoutInterval:3];
        [URL release];
        imageURLConnection = [[NSURLConnection alloc] initWithRequest:URLRequest 
                                                             delegate:self
                                                     startImmediately:YES];
    }
}

#pragma mark - NSURLConnectionDelegate Protocol
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    imageData = [[NSMutableData alloc] init];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;    
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [imageData appendData:data];
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    if(imageData != nil)
    {
        [imageData release];
        imageData = nil;
    }
    [connection release];
    connection = nil;

    [imagesArray release];
    
    if (delegate)
    { 
        didFinished = YES;
        [delegate newsHTMLParserDidFailedDownloadingImages:self 
                                                 withError:error];
    }
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *imagePath = [NSString stringWithFormat:@"%@/%@",kPathDocumentsDirectory,[imagesArray objectAtIndex:0]];
    
    [imageData writeToFile:imagePath
                atomically:YES];
    
    if(imageData != nil)
    {
        [imageData release];
        imageData = nil;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [connection release];
    connection = nil;
    
    if(currentDownloadingImage < [imagesArray count] - 1)
    {
        [self startDownloading];
    }
    else
    {
        [imagesArray release];
        if (delegate)
        { 
            didFinished = YES;
            [delegate newsHTMLParserDidFinishedDownloadingImages:self];
        }
    }
}
- (NSCachedURLResponse *)connection:(NSURLConnection *)theConnection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}


#pragma mark - NSXMLParser Work
- (void)startParsing {
    NSString *HTMLPath = [kPathDocumentsDirectory stringByAppendingPathComponent:@"news.html"];
    NSData *data = [NSData dataWithContentsOfFile:HTMLPath];   
    NSString *content = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];    
//    NSLog(@"%@",content);
    NSRegularExpression* regex = [[NSRegularExpression alloc] initWithPattern:@"(?<=img alt=\"\" src=\").*?(?=\")" 
                                                                      options:NSRegularExpressionCaseInsensitive 
                                                                        error:nil];
                                                                                       
    NSArray* imgBalises = [regex matchesInString:content options:0 range:NSMakeRange(0, [content length])];
    [regex release];
    imagesArray = [[NSMutableArray alloc] init];
    for (NSTextCheckingResult* b in imgBalises) // Loop through the URL list
    {
//        NSString* url = [contentCopy substringWithRange:b.range];
        
        // So stuff to download the image
        // And save it
        
        [imagesArray addObject:[content substringWithRange:b.range]];
//        NSLog(@"%@",[content substringWithRange:b.range]);
        
//        [content replaceOccurrencesOfString:url withString:@"LOCAL_PATH" options:NSLiteralSearch range:NSMakeRange(0, [content length])];
        
    }
//    NSLog(@"%@",imgBalises);
    currentDownloadingImage = -1;
    [self startDownloading];
   
    
//    HTMLParser = [[NSXMLParser alloc] initWithData:data];
//    [HTMLParser setDelegate:self];
////    [HTMLParser setShouldProcessNamespaces:NO];
////    [HTMLParser setShouldReportNamespacePrefixes:NO];
////    [HTMLParser setShouldResolveExternalEntities:YES];   
//    
//    [HTMLParser parse];
//    [HTMLParser release];
}

//#pragma mark - NSXMLParserDelegate Protocol
//- (void)parser:(NSXMLParser *)parser 
//   didStartElement:(NSString *)elementName 
//      namespaceURI:(NSString *)namespaceURI 
//     qualifiedName:(NSString *)qName 
//        attributes:(NSDictionary *)attributeDict {
//    
//    
//    if(parser == HTMLParser)
//    {
////        NSLog(@"%@",elementName);
//        if([elementName isEqualToString:@"img"])
//        {
//            [imagesArray addObject:[attributeDict objectForKey:@"src"]];
//        }
//    }
//}
//- (void)parserDidEndDocument:(NSXMLParser *)parser {
//    if(parser == HTMLParser)
//    {
//        currentDownloadingImage = -1;
//        [self startDownloading];
//    }
//}
//- (NSData *)parser:(NSXMLParser *)parser resolveExternalEntityName:(NSString *)name systemID:(NSString *)systemID
//{
////    NSAttributedString *entityString = [[[NSAttributedString alloc] initWithHTML:[[NSString stringWithFormat:@"&%@;", name] dataUsingEncoding:NSUTF8StringEncoding] documentAttributes:NULL] autorelease];
////    
////    NSLog(@"resolved entity name: %@", [entityString string]);
////    
////    return [[entityString string] dataUsingEncoding:NSUTF8StringEncoding];
////
////    NSLog(@"resolve: %@",name);
//    return nil;
//}

#pragma mark - Interface
- (void)start {
//    imageURLConnection = nil;
    didFinished = NO;
    [self startParsing];
}
- (void)stop {
    if(!didFinished)
    {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [imageURLConnection cancel];
        [imageURLConnection release];
        imageURLConnection = nil;
    }
}

@end
