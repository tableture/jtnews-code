//
//  NewsView.m
//  Tableture
//
//  Created by Octav Chelaru on 7/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewsView.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation NewsView
//@synthesize titleLabel;
//@synthesize teaserLabel;
//@synthesize imageView;
//@synthesize titleString;
//@synthesize teaserString;
//@synthesize imageString;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
//    [imageView release];
    [titleString release];
    [teaserString release];
    [imageString release];
    [newsId release];
    [super dealloc];
}

- (void)initWithTitle:(NSString *)title 
            andTeaser:(NSString *)teaser 
             andImage:(NSString *)image
                andId:(NSString *)theId
{
    newsId = [theId retain];
    titleString = [title retain];
    teaserString = [teaser retain];
    imageString = [image retain];
//	[self setTitleString:title];
//    [self setTeaserString:teaser];
//    [self setImageString:image];

    BOOL isNotImage = (imageString == nil) || [@"" isEqualToString:imageString] || [imageString hasSuffix:@"white.png"];
	//-------------------
	titleLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 
                                                                 6, 
                                                                 self.frame.size.width - (isNotImage?20:(98+20)), 
                                                                 98)];
    [Utilities fillUILabelCustom:titleLabel 
                        withText:titleString 
                         andFont:[UIFont boldSystemFontOfSize:26]
                 andMaximumWidth:self.frame.size.width - 98 - 20
                      andNoLines:3];
    [self addSubview:titleLabel];
    [titleLabel release];
    //-------------------
    
    //-------------------
    teaserLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 
                                                                  34, 
                                                                  self.frame.size.width - (isNotImage?20:(98+20)), 
                                                                  90)];
    [Utilities fillUILabelCustom:teaserLabel 
                        withText:teaserString 
                         andFont:[UIFont boldSystemFontOfSize:12]
                 andMaximumWidth:self.frame.size.width - 98 - 20
                      andNoLines:5];
    [self addSubview:teaserLabel];
    [teaserLabel release];
    //-------------------

	//-------------------
	imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 98 - 5, 
                                                                 6, 
                                                                 98, 
                                                                 98)];
    NSArray *content = [imageString componentsSeparatedByString:@"/"];
    NSString *name = [content objectAtIndex:[content count]-1];
    [imageView loadImageFromURL:[NSURL URLWithString:imageString] 
                        andName:name
                      withScale:YES];	
    [self addSubview:imageView];    
	[imageView release];
    //-------------------
	
    self.layer.borderWidth = 4;
    self.layer.borderColor = [[UIColor colorWithRed:134/255.0
                                                    green:154/255.0
                                                     blue:205/255.0
                                                    alpha:1.0] CGColor];


}


- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation {
    BOOL isNotImage = (imageView.imageName == nil) || [@"" isEqualToString:imageView.imageName] || [imageString hasSuffix:@"white.png"];
	if (UIInterfaceOrientationIsLandscape(newOrientation))
	{
        titleLabel.frame = CGRectMake(6, 
                                      6, 
                                      self.frame.size.width - (isNotImage?20:(98+20)),
                                      30);
        [Utilities fillUILabelCustom:titleLabel
                            withText:titleString
                             andFont:[UIFont boldSystemFontOfSize:26]
                     andMaximumWidth:self.frame.size.width - (isNotImage?20:(98+20))
                          andNoLines:3];
        
        teaserLabel.frame = CGRectMake(6, 
                                       titleLabel.frame.origin.y + titleLabel.frame.size.height + 5, 
                                       self.frame.size.width - (isNotImage?20:(98+20)), 
                                       self.frame.size.height - (titleLabel.frame.origin.y + titleLabel.frame.size.height + 5));
        [Utilities fillUILabelCustom:teaserLabel
                            withText:teaserString
                             andFont:[UIFont boldSystemFontOfSize:12] 
                     andMaximumWidth:self.frame.size.width - (isNotImage?20:(98+20)) 
                          andNoLines:4];
        
        imageView.frame = CGRectMake(self.frame.size.width - 98 , 
                                     10, 
                                     98, 
                                     98);	
	}	
	else 
    {
        [titleLabel setFrame:CGRectMake(6, 
                                        6, 
                                        self.frame.size.width - (isNotImage?20:(98+20)),
                                        98)];
        [Utilities fillUILabelCustom:titleLabel 
                            withText:titleString 
                             andFont:[UIFont boldSystemFontOfSize:26]
                     andMaximumWidth:self.frame.size.width - (isNotImage?20:(98+20))
                          andNoLines:3];

        [teaserLabel setFrame:CGRectMake(6, 
                                         titleLabel.frame.origin.y + titleLabel.frame.size.height + 5, 
                                         self.frame.size.width - (isNotImage?20:(98+20)), 
                                         self.frame.size.height - (titleLabel.frame.origin.y + titleLabel.frame.size.height + 5))];
        [Utilities fillUILabelCustom:teaserLabel 
                            withText:teaserString 
                             andFont:[UIFont boldSystemFontOfSize:12]
                     andMaximumWidth:teaserLabel.frame.size.width 
                          andNoLines:3];
        
        [imageView setFrame:CGRectMake(self.frame.size.width - 98 - 5, 
                                       6, 
                                       98, 
                                       98)];
        
        

	}    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    NSArray *array = [newsId componentsSeparatedByString:@"_"];
    if([[array objectAtIndex:0] isEqualToString:@"home"])
    {
        NSArray *newsArray = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"news"];
        BOOL found = NO;
        for(int i = 0; i < [newsArray count] && !found; ++i)
        {
            if([[[newsArray objectAtIndex:i] objectForKey:@"id"] isEqualToString:newsId])
            {
                found = YES;
                [[ApplicationDelegate tabletureViewController] presentNewsViewControllerWithInfoDictionary:[newsArray objectAtIndex:i]];
            }
        }
    }
    else
    {
        NSArray *listArray = [[NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath] objectForKey:@"list"];
        int count = [listArray count];
        BOOL found = NO;
        for (int i = 0; i < count && found == NO; ++i)
        {
            NSArray *newsArray = [[NSDictionary dictionaryWithContentsOfFile:[NSString stringWithFormat:@"%@/feeds/section_%d.plist",kPathDocumentsDirectory,i]] objectForKey:@"news"];
            for(int j = 0; j < [newsArray count] && found == NO; j++)
            {
                if([[[newsArray objectAtIndex:j] objectForKey:@"id"] isEqualToString:newsId])
                {
                    found = YES;
                    [[ApplicationDelegate tabletureViewController] presentNewsViewControllerWithInfoDictionary:[newsArray objectAtIndex:j]];
                }
            }
                                            
        }
    }
}
@end
