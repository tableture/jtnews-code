

@interface SpecialZoneView : UIView <UIWebViewDelegate> {
    UIWebView *htmlView;
	UIScrollView *sView;
	UIView *backgroundView;
	UIView *backgroundView1;
    NSString *page;
}
- (void)initWithHTML:(NSString*)htmlPage;
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation;

@end
