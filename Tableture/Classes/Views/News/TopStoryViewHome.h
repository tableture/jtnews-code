#import "UILabelCustom.h"
#import "Utilities.h"

@interface TopStoryViewHome : UIView {
    UILabelCustom *teaserLabel;
	UILabelCustom *titleLabel;
	
	NSString *teaserString;
	NSString *titleString;
}
- (void)initWithTitle:(NSString *)title
            andTeaser:(NSString *)teaser;

- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;
@end
