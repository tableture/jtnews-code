#import "Utilities.h"
#import "AsyncImageView.h"

@interface SecondNewsView : UIView {
    UILabelCustom *categoryLabel;
    UILabelCustom *titleLabel;
	UILabelCustom *teaserLabel;
    AsyncImageView *imageView;
    
	UIView *backgroundView;
	
	NSString *categoryString;
	NSString *titleString;
	NSString *teaserString;
    NSString *imageString;
}
- (void)initWithCategory:(NSString *)category
                andTitle:(NSString *)title
               andTeaser:(NSString *)teaser
                andImage:(NSString *)image;
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;
@end

