@class NewsHTMLParser;

@protocol NewsHTMLParserDelegate <NSObject>
@required
- (void)newsHTMLParserDidFinishedDownloadingImages:(NewsHTMLParser *)theNewsHTMLParser;
- (void)newsHTMLParserDidFailedDownloadingImages:(NewsHTMLParser *)theNewsHTMLParser
                                       withError:(NSError *)error;
@optional
- (void)newsHTMLParserDidFinishedParsing:(NewsHTMLParser *)theNewsHTMLParser;
@end

@interface NewsHTMLParser : NSObject <NSXMLParserDelegate> {
//    NSXMLParser *HTMLParser;
    NSMutableArray *imagesArray;
    int currentDownloadingImage;
    
    NSURLConnection *imageURLConnection;
    NSMutableData *imageData;
    
    id <NewsHTMLParserDelegate> delegate;
    
    BOOL didFinished;
}
@property (nonatomic, assign) id <NewsHTMLParserDelegate> delegate;
@property (nonatomic, assign) BOOL didFinished;
- (void)start;
- (void)stop;
@end

