#import "SpecialZoneView.h"
#import "Constants.h"
@implementation SpecialZoneView


-(void)reload{
    [htmlView reload];
}

-(void)reloadCurrentPage
{
    NSURL *html = [NSURL fileURLWithPath:[kPathDocumentsDirectory stringByAppendingPathComponent:page]];
	NSURLRequest *URLReq = [NSURLRequest requestWithURL:html];
	[htmlView loadRequest:URLReq]; 
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
        return NO;
    else
        return YES;
//    if(request == URLReq) 
//        return YES;
//    else
//        return NO;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
	
	
	NSString *width = [htmlView stringByEvaluatingJavaScriptFromString: @"document.body.clientHeight"];
	
	htmlView.frame = CGRectMake(0, 0,self.frame.size.width-8 , [width floatValue]);
	
	sView.contentSize = CGSizeMake(self.frame.size.width-8,htmlView.frame.size.height);   
	
}

-(void)removeInput:(UIWebView *)webView{
    UIScrollView *webViewContentView;
    for (UIView *checkView in [webView subviews] ) {
        if ([checkView isKindOfClass:[UIScrollView class]]) {
            webViewContentView = (UIScrollView*)checkView;
            break;
        }
    }
    
    for (UIView *checkView in [webViewContentView subviews] ) {
        checkView.userInteractionEnabled = NO;
    }
}

-(void)initWithHTML:(NSString*)htmlPage
{
	self.backgroundColor = [UIColor clearColor];
	self.autoresizesSubviews = YES;
    
    [page release];
    page = [htmlPage copy]; 
    
	backgroundView = [[UIView alloc ]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    backgroundView.backgroundColor = [UIColor colorWithRed:197/255.0 green:177/255.0 blue:179/255.0 alpha:1.0];
	[self addSubview:backgroundView];
    [backgroundView release];
	
	
	sView = [[UIScrollView alloc] initWithFrame:CGRectMake(4,4,self.frame.size.width-8,self.frame.size.height-8)];
    sView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	sView.backgroundColor = [UIColor clearColor];
	[self addSubview:sView];
    [sView release];
	
    htmlView=[[UIWebView alloc]initWithFrame:CGRectMake(4,4,self.frame.size.width-8,self.frame.size.height-8)];
    htmlView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	htmlView.backgroundColor = [UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0];
	htmlView.userInteractionEnabled = YES;
	htmlView.delegate = self;
    //[self removeInput:htmlView];
    [sView addSubview:htmlView];
    [self reloadCurrentPage];
    [htmlView release];

	self.backgroundColor = [UIColor redColor];
    
	
}




-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self reloadCurrentPage];
}

-(void) updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation{
    //	backgroundView.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    ////	backgroundView1.frame = CGRectMake(2,2,self.frame.size.width-4,self.frame.size.height-4);
    //	
    //	sView.frame = CGRectMake(4,4,self.frame.size.width-8,self.frame.size.height-8);
    //	htmlView.frame = CGRectMake(4,4,self.frame.size.width-8,self.frame.size.height-8);
    //	[htmlView reload];
}


- (void)dealloc {
    [page release];
    //[htmlView release];
    //[sView release];
    [super dealloc];
}


@end
