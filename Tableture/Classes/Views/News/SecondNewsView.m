#import "SecondNewsView.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation SecondNewsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)dealloc
{
//    [imageView release];
    [super dealloc];
}

- (void)initWithCategory:(NSString *)category
                andTitle:(NSString *)title
               andTeaser:(NSString *)teaser
                andImage:(NSString *)image
{
    
	backgroundView = [[UIView alloc] initWithFrame:CGRectMake(2, 
                                                              2,
                                                              self.frame.size.width, 
                                                              self.frame.size.height)];
    backgroundView.backgroundColor = [UIColor colorWithRed:233/255.0 
                                                     green:224/255.0 
                                                      blue:221/255.0 
                                                     alpha:1.0];    
    [backgroundView.layer setMasksToBounds:YES];
    [backgroundView.layer setBorderWidth:2.0];
    [backgroundView.layer setBorderColor:[[UIColor colorWithRed:134/255.0 
                                                          green:154/255.0 
                                                           blue:205/255.0 
                                                          alpha:1.0] CGColor]];    
    backgroundView.userInteractionEnabled = YES;
	[self addSubview:backgroundView];
    [backgroundView release];	
	self.backgroundColor = [UIColor clearColor];
    
    categoryString = category;
    titleString = title;
    teaserString = teaser;
    imageString = image;

	//-------------------
    categoryLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 
                                                                    6, 
                                                                    self.frame.size.width - 12,
                                                                    30)];
    [Utilities fillUILabelCustom:categoryLabel 
                        withText:categoryString
                         andFont:[UIFont boldSystemFontOfSize:22]
                 andMaximumWidth:categoryLabel.frame.size.width
                      andNoLines:1];
    [self addSubview:categoryLabel];
    [categoryLabel release];
    //-------------------

	//-------------------
    titleLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 
                                                                 categoryLabel.frame.size.height + categoryLabel.frame.origin.y + 5, 
                                                                 self.frame.size.width - 12, 
                                                                 30)];
    [Utilities fillUILabelCustom:titleLabel 
                        withText:titleString
                         andFont:[UIFont boldSystemFontOfSize:20]
                 andMaximumWidth:titleLabel.frame.size.width
                      andNoLines:2];
    [titleLabel setBackgroundColor:[UIColor greenColor]];
    [self addSubview:titleLabel];
    [titleLabel release];
    //-------------------
	
    //-------------------
    teaserLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 
                                                                  titleLabel.frame.size.height + titleLabel.frame.origin.y + 5, 
                                                                  self.frame.size.width - 125, 
                                                                  90)];
    [Utilities fillUILabelCustom:teaserLabel 
                        withText:teaserString
                         andFont:[UIFont boldSystemFontOfSize:12]
                 andMaximumWidth:teaserLabel.frame.size.width
                      andNoLines:6];
//    [teaserLabel setBackgroundColor:[UIColor redColor]];
    [self addSubview:teaserLabel];
    [teaserLabel release];
    //-------------------
	
    //-------------------
    imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 98 - 5, 
                                                                 titleLabel.frame.origin.y,  
                                                                 98, 
                                                                 self.frame.size.height - titleLabel.frame.origin.y - 5)];
    NSArray *content = [imageString componentsSeparatedByString:@"/"];
    NSString *name = [content objectAtIndex:[content count]-1];
    [imageView loadImageFromURL:[NSURL URLWithString:imageString] 
                        andName:name
                      withScale:YES];
    [self addSubview:imageView];
    [imageView release];
	//-------------------	
	
}

- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation
{
	backgroundView.frame = CGRectMake(2, 2,self.frame.size.width-4, self.frame.size.height-4);
	if (UIInterfaceOrientationIsLandscape(newOrientation))
	{
        [backgroundView setFrame:CGRectMake(2, 
                                            2,
                                            self.frame.size.width, 
                                            self.frame.size.height)];
        
        categoryLabel.frame = CGRectMake(6, 
                                         6,
                                         self.frame.size.width - 12,
                                         30);
        [Utilities fillUILabelCustom:categoryLabel 
                            withText:categoryString
                             andFont:[UIFont boldSystemFontOfSize:22]
                     andMaximumWidth:self.frame.size.width-12 
                          andNoLines:1];
        
        titleLabel.frame = CGRectMake(6, 
                                      categoryLabel.frame.size.height + categoryLabel.frame.origin.y + 5, 
                                      self.frame.size.width - 12, 
                                      60);
        [Utilities fillUILabelCustom:titleLabel 
                            withText:titleString
                             andFont:[UIFont boldSystemFontOfSize:20]
                     andMaximumWidth:self.frame.size.width - 12 
                          andNoLines:2];
        
        teaserLabel.frame = CGRectMake(6, 
                                       titleLabel.frame.origin.y + titleLabel.frame.size.height + 15, 
                                       self.frame.size.width - 25 - 110, 
                                       110);
        [Utilities fillUILabelCustom:teaserLabel
                            withText:teaserString 
                             andFont:[UIFont systemFontOfSize:14]
                     andMaximumWidth:self.frame.size.width - 25 - 110
                          andNoLines:6];        
        
        imageView.frame = CGRectMake(self.frame.size.width - 110 - 5, 
                                     titleLabel.frame.origin.y + titleLabel.frame.size.height + 15, 
                                     110, 
                                     110);	
        [imageView refresh];
	}	
	else 
    {		
        [backgroundView setFrame:CGRectMake(2, 
                                            2,
                                            self.frame.size.width, 
                                            self.frame.size.height)];
        
        categoryLabel.frame = CGRectMake(6, 
                                         6, 
                                         self.frame.size.width-12, 
                                         30);
        [Utilities fillUILabelCustom:categoryLabel 
                            withText:categoryString
                             andFont:[UIFont boldSystemFontOfSize:22]
                     andMaximumWidth:self.frame.size.width - 12
                          andNoLines:1];   
        
        titleLabel.frame = CGRectMake(6, 
                                      categoryLabel.frame.size.height + categoryLabel.frame.origin.y + 5, 
                                      self.frame.size.width - 12, 
                                      60);
        [Utilities fillUILabelCustom:titleLabel 
                            withText:titleString
                             andFont:[UIFont boldSystemFontOfSize:20]
                     andMaximumWidth:self.frame.size.width - 12 
                          andNoLines:2];
        
        teaserLabel.frame = CGRectMake(6, 
                                       titleLabel.frame.origin.y + titleLabel.frame.size.height + 12, 
                                       self.frame.size.width - 25 - 98, 
                                       90);
        [Utilities fillUILabelCustom:teaserLabel
                            withText:teaserString 
                             andFont:[UIFont systemFontOfSize:12]
                     andMaximumWidth:self.frame.size.width - 25 - 98
                          andNoLines:6];
        
        imageView.frame = CGRectMake(self.frame.size.width - 98 - 5, 
                                     titleLabel.frame.origin.y + titleLabel.frame.size.height + 12, 
                                     98, 
                                     98);
        [imageView refresh];
	}   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSDictionary *dictionaryToReturn = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"secondNews"];
    [[ApplicationDelegate tabletureViewController] presentNewsViewControllerWithInfoDictionary:dictionaryToReturn];
}
@end
