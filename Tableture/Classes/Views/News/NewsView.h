#import "AsyncImageView.h"
#import "UILabelCustom.h"

@interface NewsView : UIView {
    UILabelCustom *teaserLabel;
	UILabelCustom *titleLabel;
	AsyncImageView *imageView;
	
	NSString *imageString;
	NSString *teaserString;
	NSString *titleString;
    
    NSString *newsId;
}
//@property (nonatomic,retain) UILabelCustom *teaserLabel;
//@property (nonatomic,retain) UILabelCustom *titleLabel;
//@property (nonatomic,retain) AsyncImageView *imageView;
//@property (nonatomic,retain) NSString *imageString;
//@property (nonatomic,retain) NSString *teaserString;
//@property (nonatomic,retain) NSString *titleString;

- (void)initWithTitle:(NSString *)title 
            andTeaser:(NSString *)teaser 
             andImage:(NSString *)image
                andId:(NSString *)theId;

- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;
@end
