//
//  TopStoryView.m
//  Tableture
//
//  Created by Octav Chelaru on 7/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TopStoryViewHome.h"
#import "Constants.h"

@implementation TopStoryViewHome

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)initWithTitle:(NSString *)title 
            andTeaser:(NSString *)teaser 
{
	titleString = title;
	teaserString = teaser;
	
    //	self.backgroundColor = [UIColor clearColor];
	//-------------------
	titleLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 6, self.frame.size.width - 10, 30)];
    [Utilities fillUILabelCustom:titleLabel 
                        withText:titleString 
                         andFont:[UIFont boldSystemFontOfSize:20]
                 andMaximumWidth:self.frame.size.width - 10 
                      andNoLines:1];
    [self addSubview:titleLabel];
    [titleLabel release];
    //-------------------
    
    //-------------------
    teaserLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(6, 
                                                                  titleLabel.frame.origin.y + titleLabel.frame.size.height + 8, 
                                                                  self.frame.size.width - 20, 
                                                                  90)];
    [Utilities fillUILabelCustom:teaserLabel 
                        withText:teaserString 
                         andFont:[UIFont boldSystemFontOfSize:12]
                 andMaximumWidth:self.frame.size.width - 20 
                      andNoLines:7];
    [self addSubview:teaserLabel];
    [teaserLabel release];
    //-------------------
}


- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation {
    
    
    int titleBoldSystemFontOfSize;
    int titleNoLines;
    int teaserBoldSystemFontOfSize;
    int teaserSomeHeiht;
    int teaserNoLines;

    if (UIInterfaceOrientationIsLandscape(newOrientation)){
        
        titleBoldSystemFontOfSize = 26;
        titleNoLines = 1;
        teaserBoldSystemFontOfSize = 13;
        teaserSomeHeiht = 10;
        teaserNoLines = 5;
    
    }else{
        
        titleBoldSystemFontOfSize = 20;
        titleNoLines = 2;
        teaserBoldSystemFontOfSize = 12;
        teaserSomeHeiht = 8;
        teaserNoLines = 7;
        
    }
	
    titleLabel.frame = CGRectMake(6, 6, self.frame.size.width - 10, 30);
    [Utilities fillUILabelCustom:titleLabel
                        withText:titleString
                         andFont:[UIFont boldSystemFontOfSize:titleBoldSystemFontOfSize] 
                 andMaximumWidth:self.frame.size.width - 10 
                      andNoLines:titleNoLines];

    
    teaserLabel.frame = 
            CGRectMake(6, titleLabel.frame.origin.y + titleLabel.frame.size.height + teaserSomeHeiht, self.frame.size.width - 20, 90);
    [Utilities fillUILabelCustom:teaserLabel
                        withText:teaserString
                         andFont:[UIFont boldSystemFontOfSize:teaserBoldSystemFontOfSize] 
                 andMaximumWidth:self.frame.size.width - 20 
                      andNoLines:teaserNoLines];	
   
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    NSDictionary *dictionaryToReturn = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"topStory"];
    [[ApplicationDelegate tabletureViewController] presentNewsViewControllerWithInfoDictionary:dictionaryToReturn];
}

@end
