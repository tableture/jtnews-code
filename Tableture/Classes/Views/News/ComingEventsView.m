#import <QuartzCore/QuartzCore.h>
#import "ComingEventsView.h"
#import "Constants.h"
@implementation ComingEventsView

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

- (void)initWithTitle:(NSString *)title 
       andTitleEvent1:(NSString *)titleEvent1
      andTeaserEvent1:(NSString *)teaserEvent1
       andTitleEvent2:(NSString *)titleEvent2
      andTeaserEvent2:(NSString *)teaserEvent2
{
    titleString = title;
	titleEvent1String = titleEvent1;
	teaserEvent1String = teaserEvent1;
	titleEvent2String = titleEvent2;
	teaserEvent2String = teaserEvent2;
    
    //-------------------
	backgroundView1 = [[UIView alloc] initWithFrame:CGRectMake(2, 
                                                               2,
                                                               self.frame.size.width, 
                                                               self.frame.size.height)];
    backgroundView1.backgroundColor = [UIColor colorWithRed:233/255.0 
                                                      green:224/255.0 
                                                       blue:221/255.0 
                                                      alpha:1.0];    
    [backgroundView1.layer setMasksToBounds:YES];
    [backgroundView1.layer setBorderWidth:2.0];
    [backgroundView1.layer setBorderColor:[[UIColor colorWithRed:134/255.0 
                                                           green:154/255.0 
                                                            blue:205/255.0 
                                                           alpha:1.0] CGColor]];
	[self addSubview:backgroundView1];
    [backgroundView1 release];
    //-------------------
    
    //-------------------
	titleLabel = [[UILabelCustom alloc] initWithFrame:CGRectMake(
                                                                 12, 
                                                                 3,
                                                                 self.frame.size.width-10,
                                                                 20)];
    [Utilities fillUILabelCustom:titleLabel 
                        withText:titleString 
                         andFont:[UIFont boldSystemFontOfSize:18]
                 andMaximumWidth:titleLabel.frame.size.width
                      andNoLines:1];
    [self addSubview:titleLabel];
    [titleLabel release];
    //-------------------
	
    //-------------------
    titleEvent1Label = [[UILabelCustom alloc] initWithFrame:CGRectMake(12,
                                                                       28,
                                                                       (self.frame.size.width / 2) - 10,
                                                                       20)];
    [Utilities fillUILabelCustom:titleEvent1Label
                        withText:titleEvent1String
                         andFont:[UIFont boldSystemFontOfSize:14]
                 andMaximumWidth:titleEvent1Label.frame.size.width
                      andNoLines:1];
//    backgroundView2 = [[UIView alloc] initWithFrame:CGRectMake(titleEvent1Label.frame.origin.x-2,
//                                                               titleEvent1Label.frame.origin.y+2,
//                                                               self.frame.size.width/2 - 14,
//                                                               titleEvent1Label.frame.size.height+30)];
//    [backgroundView2 setBackgroundColor:[UIColor colorWithRed:233/255.0
//                                                        green:224/255.0
//                                                         blue:221/255.0
//                                                        alpha:1.0]];
//    [backgroundView2.layer setMasksToBounds:YES];
//    [backgroundView2.layer setBorderWidth:2.0];
//    [backgroundView2.layer setBorderColor:[[UIColor colorWithRed:214/255.0
//                                                          green:195/255.0
//                                                           blue:189/255.0
//                                                           alpha:1.0] CGColor]];
//    [self addSubview:backgroundView2];
//    [backgroundView2 release];
    [self addSubview:titleEvent1Label];
    [titleEvent1Label release];
    //-------------------
    
    //-------------------
    titleEvent2Label = [[UILabelCustom alloc] initWithFrame:CGRectMake(self.frame.size.width/2 + 10,
                                                                       28,
                                                                       self.frame.size.width/2,
                                                                       20)];
    [Utilities fillUILabelCustom:titleEvent2Label
                        withText:titleEvent2String
                         andFont:[UIFont boldSystemFontOfSize:14]
                 andMaximumWidth:titleEvent2Label.frame.size.width
                      andNoLines:1];
    
//    backgroundView3 = [[UIView alloc] initWithFrame:CGRectMake(titleEvent2Label.frame.origin.x-2,
//                                                               titleEvent2Label.frame.origin.y+2,
//                                                               self.frame.size.width/2 - 14,
//                                                               titleEvent2Label.frame.size.height+30)];
//    [backgroundView3 setBackgroundColor:[UIColor colorWithRed:233/255.0
//                                                        green:224/255.0
//                                                         blue:221/255.0
//                                                        alpha:1.0]];
//    [backgroundView3.layer setMasksToBounds:YES];
//    [backgroundView3.layer setBorderWidth:2.0];
//    [backgroundView3.layer setBorderColor:[[UIColor colorWithRed:214/255.0
//                                                           green:195/255.0
//                                                            blue:189/255.0
//                                                           alpha:1.0] CGColor]];
//    [self addSubview:backgroundView3];
//    [backgroundView3 release];
    [self addSubview:titleEvent2Label];
    [titleEvent2Label release];
    //-------------------
    
    //-------------------
    teaserEvent1Label = [[UILabelCustom alloc] initWithFrame:CGRectMake(12,
                                                                       titleEvent1Label.frame.origin.y + titleEvent1Label.frame.size.height,
                                                                       150,
                                                                        40)];
    [Utilities fillUILabelCustom:teaserEvent1Label 
                        withText:teaserEvent1String
                         andFont:[UIFont systemFontOfSize:12]
                 andMaximumWidth:teaserEvent1Label.frame.size.width
                      andNoLines:2];
    [self addSubview:teaserEvent1Label];
    [teaserEvent1Label release];
    
    teaserEvent2Label = [[UILabelCustom alloc] initWithFrame:CGRectMake(titleEvent2Label.frame.origin.x,
                                                                        titleEvent2Label.frame.origin.y + titleEvent2Label.frame.size.height,
                                                                        150,
                                                                        40)];
    [Utilities fillUILabelCustom:teaserEvent2Label 
                        withText:teaserEvent2String
                         andFont:[UIFont systemFontOfSize:12]
                 andMaximumWidth:teaserEvent2Label.frame.size.width
                      andNoLines:2];
    [self addSubview:teaserEvent2Label];
    [teaserEvent2Label release];
	//-------------------
}	


-(void) updateContentOnOrientationChange:(UIInterfaceOrientation) newOrientation
{
    if(UIInterfaceOrientationIsPortrait(newOrientation))
    {
        [backgroundView1 setFrame:CGRectMake(2, 
                                             2,
                                             self.frame.size.width, 
                                             self.frame.size.height)];

        [titleLabel setFrame:CGRectMake(12, 
                                        3,
                                        self.frame.size.width-10,
                                        20)];
        [Utilities fillUILabelCustom:titleLabel 
                            withText:titleString 
                             andFont:[UIFont boldSystemFontOfSize:18]
                     andMaximumWidth:self.frame.size.width-10
                          andNoLines:1];

        [titleEvent1Label setFrame:CGRectMake(12,
                                              28,
                                              self.frame.size.width/2-20,
                                              20)];
        [Utilities fillUILabelCustom:titleEvent1Label
                            withText:titleEvent1String
                             andFont:[UIFont boldSystemFontOfSize:14]
                     andMaximumWidth:titleEvent1Label.frame.size.width
                          andNoLines:1];
//        [backgroundView2 setFrame:CGRectMake(titleEvent1Label.frame.origin.x-2,
//                                             titleEvent1Label.frame.origin.y+2,
//                                             self.frame.size.width/2 - 14,
//                                             titleEvent1Label.frame.size.height+30)];

        [titleEvent2Label setFrame:CGRectMake(self.frame.size.width/2 + 10,
                                              28,
                                              self.frame.size.width/2-20,
                                              20)];
        [Utilities fillUILabelCustom:titleEvent2Label
                            withText:titleEvent2String
                             andFont:[UIFont boldSystemFontOfSize:14]
                     andMaximumWidth:titleEvent2Label.frame.size.width
                          andNoLines:1];
        
//        [backgroundView3 setFrame:CGRectMake(titleEvent2Label.frame.origin.x-2,
//                                             titleEvent2Label.frame.origin.y+2,
//                                             self.frame.size.width/2 - 14,
//                                             titleEvent2Label.frame.size.height+30)];

        [teaserEvent1Label setFrame:CGRectMake(12,
                                               titleEvent1Label.frame.origin.y + titleEvent1Label.frame.size.height,
                                               self.frame.size.width/2-20,
                                               40)];
        [Utilities fillUILabelCustom:teaserEvent1Label 
                            withText:teaserEvent1String
                             andFont:[UIFont systemFontOfSize:12]
                     andMaximumWidth:teaserEvent1Label.frame.size.width
                          andNoLines:2];

        [teaserEvent2Label setFrame:CGRectMake(titleEvent2Label.frame.origin.x,
                                                                            titleEvent2Label.frame.origin.y + titleEvent2Label.frame.size.height,
                                                                            self.frame.size.width/2-30,
                                                                            40)];
        [Utilities fillUILabelCustom:teaserEvent2Label 
                            withText:teaserEvent2String
                             andFont:[UIFont systemFontOfSize:12]
                     andMaximumWidth:teaserEvent2Label.frame.size.width
                          andNoLines:2];


    }
    else
    {
        [backgroundView1 setFrame:CGRectMake(2, 
                                             2,
                                             self.frame.size.width, 
                                             self.frame.size.height)];
        
        [titleLabel setFrame:CGRectMake(
                                        12, 
                                        3,
                                        self.frame.size.width-10,
                                        20)];
        [Utilities fillUILabelCustom:titleLabel 
                            withText:titleString 
                             andFont:[UIFont boldSystemFontOfSize:18]
                     andMaximumWidth:titleLabel.frame.size.width
                          andNoLines:1];
        
        [titleEvent1Label setFrame:CGRectMake(12,
                                              28,
                                              (self.frame.size.width / 2) - 20,
                                              20)];
        [Utilities fillUILabelCustom:titleEvent1Label
                            withText:titleEvent1String
                             andFont:[UIFont boldSystemFontOfSize:14]
                     andMaximumWidth:titleEvent1Label.frame.size.width
                          andNoLines:1];
//        [backgroundView2 setFrame:CGRectMake(titleEvent1Label.frame.origin.x-2,
//                                             titleEvent1Label.frame.origin.y+2,
//                                             self.frame.size.width/2 - 14,
//                                             titleEvent1Label.frame.size.height+30)];
        
        [titleEvent2Label setFrame:CGRectMake(self.frame.size.width/2 + 10,
                                              28,
                                              (self.frame.size.width / 2) - 20,
                                              20)];
        [Utilities fillUILabelCustom:titleEvent2Label
                            withText:titleEvent2String
                             andFont:[UIFont boldSystemFontOfSize:14]
                     andMaximumWidth:titleEvent2Label.frame.size.width
                          andNoLines:1];
        
//        [backgroundView3 setFrame:CGRectMake(titleEvent2Label.frame.origin.x-2,
//                                             titleEvent2Label.frame.origin.y+2,
//                                             self.frame.size.width/2 - 14,
//                                             titleEvent2Label.frame.size.height+30)];
        
        [teaserEvent1Label setFrame:CGRectMake(12,
                                               titleEvent1Label.frame.origin.y + titleEvent1Label.frame.size.height,
                                               (self.frame.size.width / 2) - 20,
                                               40)];
        [Utilities fillUILabelCustom:teaserEvent1Label 
                            withText:teaserEvent1String
                             andFont:[UIFont systemFontOfSize:12]
                     andMaximumWidth:teaserEvent1Label.frame.size.width
                          andNoLines:2];
        
        [teaserEvent2Label setFrame:CGRectMake(titleEvent2Label.frame.origin.x,
                                               titleEvent2Label.frame.origin.y + titleEvent2Label.frame.size.height,
                                               (self.frame.size.width / 2) - 20,
                                               40)];
        [Utilities fillUILabelCustom:teaserEvent2Label 
                            withText:teaserEvent2String
                             andFont:[UIFont systemFontOfSize:12]
                     andMaximumWidth:teaserEvent2Label.frame.size.width
                          andNoLines:2];
    }
    
    /*
    
	if (newOrientation == UIInterfaceOrientationLandscapeLeft || newOrientation == UIInterfaceOrientationLandscapeRight)
	{
		
		

		
		backgroundView1.frame = CGRectMake(2, 2,self.frame.size.width-4, self.frame.size.height-4);
		
		
		headlineTitle.frame = CGRectMake(12, 5,self.frame.size.width-10,20);
		[Utils fillUILabelCustom:headlineTitle withText:headTitle andFont:[UIFont boldSystemFontOfSize:18] andMaximumWidth:self.frame.size.width-10 andNoLines:1];			
		
		titleEvent1.frame = CGRectMake(12, headlineTitle.frame.origin.y+headlineTitle.frame.size.height,(self.frame.size.width)/2+70,20);
		[Utils fillUILabelCustom:titleEvent1 withText:title1 andFont:[UIFont boldSystemFontOfSize:14] andMaximumWidth:(self.frame.size.width)/2-10 andNoLines:1];			
		
		
        backgroundView3.frame = CGRectMake(titleEvent1.frame.origin.x-2,titleEvent1.frame.origin.y+2,(self.frame.size.width)/2-14,titleEvent1.frame.size.height+26);
		
		titleEvent2.frame = CGRectMake((self.frame.size.width)/2+10, headlineTitle.frame.origin.y+headlineTitle.frame.size.height,(self.frame.size.width)/2,20);
		[Utils fillUILabelCustom:titleEvent2 withText:title2 andFont:[UIFont boldSystemFontOfSize:14] andMaximumWidth:(self.frame.size.width)/2 andNoLines:1];		
		
		
		
        
		backgroundView5.frame = CGRectMake(titleEvent2.frame.origin.x-2,titleEvent2.frame.origin.y+2,(self.frame.size.width)/2-14,titleEvent2.frame.size.height+26);
		
		teaserEvent1.frame = CGRectMake(12,titleEvent1.frame.origin.y+titleEvent1.frame.size.height,230,40);
		[Utils fillUILabelCustom:teaserEvent1 withText:teaser1 andFont:[UIFont systemFontOfSize:12] andMaximumWidth:230 andNoLines:2];			
		
		teaserEvent2.frame =  CGRectMake(titleEvent2.frame.origin.x,titleEvent2.frame.origin.y+titleEvent2.frame.size.height,230,40);
		[Utils fillUILabelCustom:teaserEvent2 withText:teaser2 andFont:[UIFont systemFontOfSize:12] andMaximumWidth:230 andNoLines:2];			
		
	}
	
	else {
		
		
		
		backgroundView1.frame = CGRectMake(2, 2,self.frame.size.width-4, self.frame.size.height-4);
		
		
		headlineTitle.frame = CGRectMake(12, 3,self.frame.size.width-10,20);
		[Utils fillUILabelCustom:headlineTitle withText:headTitle andFont:[UIFont boldSystemFontOfSize:18] andMaximumWidth:self.frame.size.width-10 andNoLines:1];			
		
		titleEvent1.frame = CGRectMake(12, headlineTitle.frame.origin.y+headlineTitle.frame.size.height,(self.frame.size.width)/2-10,20);
		[Utils fillUILabelCustom:titleEvent1 withText:title1 andFont:[UIFont boldSystemFontOfSize:14] andMaximumWidth:(self.frame.size.width)/2-10 andNoLines:1];			
		
		
		
		backgroundView3.frame = CGRectMake(titleEvent1.frame.origin.x-2,titleEvent1.frame.origin.y+2,(self.frame.size.width)/2-14,titleEvent1.frame.size.height+26);
		
		titleEvent2.frame = CGRectMake((self.frame.size.width)/2+10, headlineTitle.frame.origin.y+headlineTitle.frame.size.height,(self.frame.size.width)/2,20);
		[Utils fillUILabelCustom:titleEvent2 withText:title2 andFont:[UIFont boldSystemFontOfSize:14] andMaximumWidth:(self.frame.size.width)/2 andNoLines:1];			
		
		
		backgroundView5.frame = CGRectMake(titleEvent2.frame.origin.x-2,titleEvent2.frame.origin.y+2,(self.frame.size.width)/2-14,titleEvent2.frame.size.height+26);
		
		teaserEvent1.frame = CGRectMake(12,titleEvent1.frame.origin.y+titleEvent1.frame.size.height-5,150,40);
		[Utils fillUILabelCustom:teaserEvent1 withText:teaser1 andFont:[UIFont systemFontOfSize:12] andMaximumWidth:150 andNoLines:2];			
		
		teaserEvent2.frame =  CGRectMake(titleEvent2.frame.origin.x,titleEvent2.frame.origin.y+titleEvent2.frame.size.height-5,150,40);
		[Utils fillUILabelCustom:teaserEvent2 withText:teaser2 andFont:[UIFont systemFontOfSize:12] andMaximumWidth:150 andNoLines:2];			
		
	}
	
	*/
	
	
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[ApplicationDelegate tabletureViewController] jumpToEvents];
}

- (void)dealloc {
    [super dealloc];
}


@end

