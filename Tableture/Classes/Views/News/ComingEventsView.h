#import "Utilities.h"
#import "UILabelCustom.h"

@interface ComingEventsView : UIView {
    
	UILabelCustom *titleLabel;
	
	UILabelCustom *titleEvent1Label;
	UILabelCustom *teaserEvent1Label;
    
	UILabelCustom *titleEvent2Label;
	UILabelCustom *teaserEvent2Label;
	
	UIView *backgroundView1;
//	UIView *backgroundView2;
//	UIView *backgroundView3;
	
	NSString *titleString;
	NSString *titleEvent1String;
	NSString *teaserEvent1String;
	NSString *titleEvent2String;
	NSString *teaserEvent2String;
}

- (void)initWithTitle:(NSString *)title 
       andTitleEvent1:(NSString *)titleEvent1
      andTeaserEvent1:(NSString *)teaserEvent1
       andTitleEvent2:(NSString *)titleEvent2
      andTeaserEvent2:(NSString *)teaserEvent2;
- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;

@end
