#import "NewsViewController.h"
#import "Constants.h"

@implementation NewsViewController

#pragma mark - Properties
@synthesize newsDictionary;
//@synthesize delegate;

#pragma mark - Memory Management
- (void)dealloc {
    [newsHTMLParser stop];
    [newsHTMLParser release];
    [newsDictionary release];
    [containerScrollView release];
    [saveButton release];
    [waintingSpinner release];
    [waitingLabel release];
    [super dealloc];
}
- (void)viewDidUnload {
    [containerScrollView release];
    containerScrollView = nil;
    [saveButton release];
    saveButton = nil;
    [waintingSpinner release];
    waintingSpinner = nil;
    [waitingLabel release];
    waitingLabel = nil;
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Orientation Support
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation 
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
            [waintingSpinner setFrame:CGRectMake(768/2 - 10, 768/2 - 10, 20, 20)];;
            [waitingLabel setFrame:CGRectMake(0, waintingSpinner.frame.origin.y - 30, 768, 20)];
    if(UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {

        [containerScrollView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        CGSize contentSize = CGSizeMake(self.view.frame.size.width, leftAd.frame.origin.y + leftAd.frame.size.height + 100);
        [containerScrollView setContentSize:contentSize];
    }
    else
    {

        [containerScrollView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        CGSize contentSize = CGSizeMake(self.view.frame.size.width, leftAd.frame.origin.y + leftAd.frame.size.height + 30);
        [containerScrollView setContentSize:contentSize];
    }
    [leftAd updateContentOnOrientationChange:toInterfaceOrientation];
    [rightAd updateContentOnOrientationChange:toInterfaceOrientation];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}
#pragma mark - Spinner
- (void)showSpinner
{
    [waitingLabel setHidden:NO];
    [waintingSpinner setHidden:NO];
}
- (void)hideSpinner
{
    [waitingLabel setHidden:YES];
    [waintingSpinner setHidden:YES];
}
#pragma mark - View lifecycle
- (id)initWithDictionary:(NSDictionary*)dictionary
{
	if ((self = [super initWithNibName:@"NewsViewController" bundle:nil]))
	{
		newsDictionary = [dictionary retain];
        [self setTitle:[newsDictionary objectForKey:@"title"]];
//        NSLog(@"%@",newsDictionary);
	}
	return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *newsID = [newsDictionary objectForKey:@"id"];
    NSArray *savedNewsArray = [[NSDictionary dictionaryWithContentsOfFile:kPathSavedPlistPath] objectForKey:@"list"];
    [saveButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"save-button-off" ofType:@"png"]]
                forState:UIControlStateNormal];
    for(NSDictionary *dictionary in savedNewsArray)
    {
        if([[dictionary objectForKey:@"id"] isEqualToString:newsID])
        {
            [saveButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"save-button-on" ofType:@"png"]] 
                        forState:UIControlStateNormal];
            [saveButton setUserInteractionEnabled:NO];
            break;
        }
    }
    
	NSURL *htmlPath = [NSURL fileURLWithPath:[kPathDocumentsDirectory stringByAppendingPathComponent:@"news.html"]];
/*	NSString *htmlString = [NSString stringWithString:@"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">"
	"<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /><title></title>"
	"<link rel=\"stylesheet\" type=\"text/css\" href=\"interiorStory.css\"/></head><body><div id=\"content\">"
	"<div class=\"main-text\">"];	
	htmlString = [htmlString stringByAppendingString:[newsDictionary objectForKey:@"html"]];*/	

    
    
    NSString *htmlString = [NSString stringWithFormat:@"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />"
                            "<style type=\"text/css\"> %@ </style></head><body>",[NSString stringWithContentsOfURL:[NSURL fileURLWithPath:[kPathDocumentsDirectory stringByAppendingPathComponent:@"interiorStory.css"] ] encoding:NSASCIIStringEncoding error:nil]];	
    htmlString = [htmlString stringByAppendingString:@"<div id=\"foo\">"];
	htmlString = [htmlString stringByAppendingString:[newsDictionary objectForKey:@"html"]];
    
	htmlString = [htmlString stringByAppendingString:@"</div></body></html>"];
	[htmlString writeToURL:htmlPath 
                atomically:NO 
                  encoding:NSUTF8StringEncoding 
                     error:nil];
	
    [containerScrollView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    newsWebView = [[UIWebView alloc] init];
    newsWebView.delegate = self;
    
    newsHTMLParser = [[NewsHTMLParser alloc] init];
    [newsHTMLParser setDelegate:self];
    [waintingSpinner setFrame:CGRectMake(768/2 - 10, 768/2 - 10, 20, 20)];
    [waitingLabel setFrame:CGRectMake(0, waintingSpinner.frame.origin.y - 30, 768, 20)];
    [self showSpinner];
    [newsHTMLParser start];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view setBackgroundColor:[UIColor colorWithRed:233/255.0 green:224/255.0 blue:221/255.0 alpha:1.0]];

    [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation
                                           duration:0];
}

#pragma mark - NewsHTMLParserDelegate Protocol
- (void)newsHTMLParserDidFinishedDownloadingImages:(NewsHTMLParser *)theNewsHTMLParser {    
	NSURLRequest *URLRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[kPathDocumentsDirectory stringByAppendingPathComponent:@"news.html"]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:0];
	[newsWebView loadRequest:URLRequest]; 
    NSLog(@" %@",[NSString stringWithContentsOfFile:[kPathDocumentsDirectory stringByAppendingPathComponent:@"news.html"]]);
}
- (void)newsHTMLParserDidFailedDownloadingImages:(NewsHTMLParser *)theNewsHTMLParser 
                                       withError:(NSError *)error {
    NSURLRequest *URLRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[kPathDocumentsDirectory stringByAppendingPathComponent:@"news.html"]]];
	[newsWebView loadRequest:URLRequest]; 
}

#pragma mark - UIWebViewDelegate Protocol
- (void)webViewDidFinishLoad:(UIWebView *)webView
  {
      
    [self hideSpinner];

      int height = [[newsWebView stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"foo\").offsetHeight;"] intValue] + 20;
    newsWebView.backgroundColor = [UIColor colorWithRed:233/255.0 
                                                  green:224/255.0 
                                                   blue:221/255.0 
                                                  alpha:1.0];
    newsWebView.frame = CGRectMake(0,
                                   60,
                                   containerScrollView.frame.size.width, 
                                   height);
	newsWebView.userInteractionEnabled = NO;
    [containerScrollView addSubview:newsWebView];
    [newsWebView release];
    
    NSDictionary *adsDictionary = [[NSDictionary dictionaryWithContentsOfFile:kPathAdsPlistPath] objectForKey:@"inside"];
    float percentage = 1.3;
    leftAd = [[MiddleAdView alloc] init];    
    [leftAd initWithImageString:[adsDictionary objectForKey:@"leftImage"] 
                        andAdId:nil];
    [leftAd setFrame:CGRectMake(18, 
                                newsWebView.frame.origin.y + newsWebView.frame.size.height + 50,
                                300 * percentage, 
                                250 * percentage)];
    [leftAd setUserInteractionEnabled:NO];
    [containerScrollView addSubview:leftAd];
    [leftAd release];
    [leftAd updateContentOnOrientationChange:self.interfaceOrientation];
    
    rightAd = [[MiddleAdView alloc] init];
    [rightAd initWithImageString:[adsDictionary objectForKey:@"rightImage"] 
                         andAdId:nil];
    [rightAd setFrame:CGRectMake(leftAd.frame.origin.x + leftAd.frame.size.width + 14, 
                                 newsWebView.frame.origin.y + newsWebView.frame.size.height + 50,
                                 250 * percentage, 
                                 250 * percentage)];
    [rightAd setUserInteractionEnabled:NO];
    [containerScrollView addSubview:rightAd];
    [rightAd release];
    [rightAd updateContentOnOrientationChange:self.interfaceOrientation];
    
    [self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0];
}

#pragma mark - IBActions
//- (IBAction)closeButtonPressed:(id)sender {
//    [self dismissModalViewControllerAnimated:YES];
//    [delegate newsClosed];
//}
- (IBAction)facebookButtonPressed {
    
    if(didPostedOnFacebook)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This message was already posted on your wall" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        [alertView show];
        [alertView release];
        return;
    }
    else
    {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathFacebookPlistPath];
        NSMutableArray *array = [dictionary objectForKey:@"posted"];
        if([array containsObject:[newsDictionary objectForKey:@"id"]])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"This message was already posted on your wall" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
            [alertView show];
            [alertView release];
            return;
        }
    }
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathSharePlistPath];
    if([[dictionary objectForKey:@"Facebook"] isEqualToString:@"NO"])
    {
        [self facebookLogin];
    }
    else
    {
        if ([[ApplicationDelegate facebook] accessToken] == nil) {
            [self facebookReauthorize];
        }
        [self facebookPost];
    }    
}

- (IBAction)twitterButtonPressed {
    [self twitterLogin];
}

- (IBAction)emailButtonPressed {
    
    NSString *body = [NSString stringWithFormat:@"You have been sent the following article from <a href=\"%@\" alt=\"%@\">%@</a>.<br />Thank you for reading!", [newsDictionary objectForKey:@"url"], [newsDictionary objectForKey:@"url"], [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"headTitle"]];
    
//    NSString *body = [NSString stringWithFormat:@"<a href=\"%@\" alt=\"%@\">%@</a>", [newsDictionary objectForKey:@"url"], [newsDictionary objectForKey:@"title"],[newsDictionary objectForKey:@"title"]];
    
//    NSString *applicationTitle = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"headTitle"];
    
     
    [self sendEmailWithRecipients:nil
                          andBody:body 
                       andSubject:[NSString stringWithFormat:@"Forwarding: %@",[newsDictionary objectForKey:@"title"]]];
    /*
    [self sendEmailWithRecipients:[NSArray arrayWithObject:[[NSDictionary dictionaryWithContentsOfFile:kPathConfigurationPlistPath] objectForKey:@"linkEmail"]] 
                          andBody:body 
                       andSubject:[NSString stringWithFormat:@"Forwarding: %@",[newsDictionary objectForKey:@"title"]]];     
     */
}

- (IBAction)saveButtonPressed {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathSavedPlistPath];
    NSMutableArray *array = [dictionary objectForKey:@"list"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"MMMM dd, YYYY"];
	NSString *date = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
	[dateFormatter release];
	[newsDictionary setValue:[NSString stringWithFormat:@"Saved on %@",date] forKey:@"saved"];
    
    [array addObject:newsDictionary];
    [dictionary setObject:array forKey:@"list"];
    [dictionary setObject:[NSNumber numberWithBool:YES] forKey:@"saved"];
    [dictionary writeToFile:kPathSavedPlistPath atomically:YES];
    
    [saveButton setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"save-button-on" ofType:@"png"]] forState:UIControlStateNormal];
    [saveButton setUserInteractionEnabled:NO];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done" message:@"The article has been saved!" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alertView show];
    [alertView release];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSavedPlistChanged object:nil];
}

#pragma mark - Twitter Methods
- (void)twitterLogin {

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathSharePlistPath];
    if([[dictionary objectForKey:@"Twitter"] isEqualToString:@"NO"])
    {
        NSLog(@"[Twitter] NO");
        [_twitter release];
        _twitter = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];
        _twitter.consumerKey    = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"twitterConsumerKey"];
        _twitter.consumerSecret = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"twitterConsumerSecret"];
        

        if([SA_OAuthTwitterController credentialEntryRequiredWithTwitterEngine:_twitter])
        {
            UIViewController *controller = [[SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_twitter 
                                                                                                            delegate:self] retain];
            NSLog(@"[Twitter] needs authentication controller");
            controller.view.frame = CGRectMake(0, 0, 768,1024);
            [controller setModalPresentationStyle:UIModalPresentationPageSheet];
            [controller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentModalViewController:controller 
                                    animated:YES];
            [controller release];
        }
        else
        {
            NSLog(@"[Twitter] does not need authentication controller");
            UIViewController *controller = [[SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_twitter 
                                                                                                            delegate:self] retain];
            NSLog(@"[Twitter] needs authentication controller");
            controller.view.frame = CGRectMake(0, 0, 768,1024);
            [controller setModalPresentationStyle:UIModalPresentationPageSheet];
            [controller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentModalViewController:controller 
                                    animated:YES];
            [controller release];
        }
    }
    else
    {
        NSLog(@"[Twitter] YES");
        [_twitter release];
        _twitter = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];
        _twitter.consumerKey    = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"twitterConsumerKey"];
        _twitter.consumerSecret = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"twitterConsumerSecret"];

        if([SA_OAuthTwitterController credentialEntryRequiredWithTwitterEngine:_twitter])
        {
            UIViewController *controller = [[SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_twitter 
                                                                                                            delegate:self] retain];
            NSLog(@"[Twitter] needs authentication controller");
            controller.view.frame = CGRectMake(0, 0, 768,1024);
            [controller setModalPresentationStyle:UIModalPresentationPageSheet];
            [controller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentModalViewController:controller 
                                    animated:YES];
            [controller release];
        }
        else
        {
            NSLog(@"[Twitter] does not need authentication controller");  
            [self twitterPost];
        }
    }
}
- (void)twitterPost {
    NSString *applicationTitle = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"headTitle"];
    NSString *attachment = [NSString stringWithFormat:@"%@: %@ \n %@",applicationTitle,[newsDictionary objectForKey:@"title"],[newsDictionary objectForKey:@"url"]!=nil?[newsDictionary objectForKey:@"url"]:@""];
    [_twitter sendUpdate:attachment];
    twitterAlertView = [[UIAlertView alloc] initWithTitle:@"Posting to Twitter" message:@"Your tweet is being posted to Twitter" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    [twitterAlertView show];
    
    UIButton *submitButton = [[twitterAlertView subviews] lastObject];
    [submitButton setEnabled:NO];
}

#pragma mark - Twitter Request
- (void)requestSucceeded:(NSString *)connectionIdentifier {
    NSLog(@"[Twitter] Request succeded: %@",connectionIdentifier);
    
    // These delegate methods are called after a connection has been established
//    NSLog(@"Request %@ succeeded", connectionIdentifier);	
	/*
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Done" 
                                                    message:@"The tweet has been posted" 
                                                   delegate:nil 
                                          cancelButtonTitle:nil 
                                          otherButtonTitles:@"OK",nil];
	[alert show];
	[alert release];*/
    
    
    UIButton *submitButton = [[twitterAlertView subviews] lastObject];
    [submitButton setEnabled:YES];
    [twitterAlertView setTitle:@"Success"];
    [twitterAlertView setMessage:@"Tweet posted!"];
} 
- (void)requestFailed:(NSString *)connectionIdentifier withError:(NSError *)error {
    NSLog(@"[Twitter] Request failed: %@",[error localizedDescription]);
    // These delegate methods are called after a connection has been established
//    NSLog(@"Request %@ failed with error: %@", connectionIdentifier, error);
    
    UIButton *submitButton = [[twitterAlertView subviews] lastObject];
    [submitButton setEnabled:YES];
    [twitterAlertView setTitle:@"Error"];
    [twitterAlertView setMessage:@"Tweet already posted"];
    
    /*
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                    message:@"The same tweet has been already posted" 
                                                   delegate:nil 
                                          cancelButtonTitle:nil 
                                          otherButtonTitles:@"OK",nil];
	[alert show];
	[alert release];*/
}

#pragma mark - Twitter Login
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) username {
	NSLog(@"[Twitter] Authenticated for %@", username);
}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
	NSLog(@"[Twitter] Authentication Failed!");
}
- (void)twitterOAuthConnectionFailedWithData:(NSData *)data {
    NSLog(@"[Twitter] Auth Connection Failed");
}

#pragma mark - Twitter Login Cache
- (void)storeCachedTwitterOAuthData:(NSString *)data 
                        forUsername:(NSString *)username {
    //implement these methods to store off the creds returned by Twitter
	[[NSUserDefaults standardUserDefaults] setObject:data forKey:@"authData"];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathSharePlistPath];
    [dictionary setObject:@"YES" forKey:@"Twitter"];
    [dictionary writeToFile:kPathSharePlistPath atomically:YES];
    [dictionary release];
}
- (NSString *)cachedTwitterOAuthDataForUsername:(NSString *)username {
    return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];
}


#pragma mark - Facebook Methods
- (void)facebookLogin {
    _permissions =  [[NSArray arrayWithObjects:@"status_update",nil] retain];
    
    if (![ApplicationDelegate facebook])
    {
        [ApplicationDelegate setFacebook: [[Facebook alloc] initWithAppId:[[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"facebookAppID"]]];
    }
    
    [[ApplicationDelegate facebook] authorize:_permissions 
                delegate:self
           andSafariAuth:NO];
}
- (void)facebookPost {
    NSString *link = [newsDictionary objectForKey:@"url"];
    if (link == nil)
        link = @"";
    if (kLinkEnabled)
    {
        link = @"http://www.google.com";
    }
//    NSLog(@"%@",link);
    NSString *applicationTitle = [[NSDictionary dictionaryWithContentsOfFile:kPathHomePlistPath] objectForKey:@"headTitle"];    
	NSString *attachment = [NSString stringWithFormat:@"{\"name\":\"%@: %@\",\"href\":\"%@\",\"caption\":\"\",\"description\":\"\",\"media\":[{\"type\":\"image\",\"href\":\"%@\",\"src\":\"%@\"}]}",
							applicationTitle,
                            [newsDictionary objectForKey:@"title"],
                            link,
                            link,
                            [newsDictionary objectForKey:@"thumb"]];	
//    NSLog(@"%@",attachment);
//	NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//									 @"", @"message",
//									 attachment, @"attachment",
//									 nil];
//    
    NSString *appID = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"facebookAppID"];
    
//#warning modifyHere
    NSDictionary *params = [NewsViewController createWallPostWithParameters:appID andLink:link andPictureURL:[newsDictionary objectForKey:@"thumb"] andName:[NSString stringWithFormat:@"%@: %@", applicationTitle,[newsDictionary objectForKey:@"title"]]  andCaption:nil andDescription:nil andMessage:nil];
    
    
    theAlertView = [[UIAlertView alloc] initWithTitle:@"Posting to Facebook" message:@"Your message is being posted to Facebook" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    [theAlertView show];
    
    UIButton *submitButton = [[theAlertView subviews] lastObject];
    [submitButton setEnabled:NO];

//    for(UIView *view in theAlertView.subviews) {
//        if([view isKindOfClass:[UIButton class]]) {
//            ((UIButton *) view).enabled = NO;
//        }
//    }

    
//	[[ApplicationDelegate facebook] requestWithMethodName:@"stream.publish" 
//						   andParams:[NSMutableDictionary dictionaryWithDictionary:params]
//					   andHttpMethod:@"POST" 
//						 andDelegate:self];
    
    [[ApplicationDelegate facebook] requestWithGraphPath:@"me/feed"
                          andParams:[NSMutableDictionary dictionaryWithDictionary:params]
                      andHttpMethod:@"POST"
                        andDelegate:self];
}

+ (NSDictionary*)createWallPostWithParameters:(NSString*)appID andLink:(NSString*)link andPictureURL:(NSString*)pictureURL andName:(NSString*)name andCaption:(NSString*)caption andDescription:(NSString*)description andMessage:(NSString*)message
{
    NSMutableDictionary *wallPostDict = [[[NSMutableDictionary alloc] init] autorelease];
    
    ![appID isEqualToString:@""] ? [wallPostDict setValue:appID forKey:@"app_id"] : nil;
    ![link isEqualToString:@""] ? [wallPostDict setValue:link forKey:@"link"] : nil;
    ![pictureURL isEqualToString:@""] ? [wallPostDict setValue:pictureURL forKey:@"picture"] : nil;
    ![name isEqualToString:@""] ? [wallPostDict setValue:name forKey:@"name"] : nil;
    ![caption isEqualToString:@""] ? [wallPostDict setValue:caption forKey:@"caption"] : nil;
    ![description isEqualToString:@""] ? [wallPostDict setValue:description forKey:@"description"] : nil;
    ![message isEqualToString:@""] ? [wallPostDict setValue:message forKey:@"message"] : nil;
    
    return wallPostDict;
}

- (void)facebookReauthorize {
   
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:kPathSharePlistPath];
    {
        if (![ApplicationDelegate facebook])
        {
            [ApplicationDelegate setFacebook: [[Facebook alloc] initWithAppId:[[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]] objectForKey:@"facebookAppID"]]];
        }
        
        [[ApplicationDelegate facebook] setAccessToken:[dictionary objectForKey:@"FacebookaccessToken"]];
        [[ApplicationDelegate facebook] setExpirationDate:(NSDate *) [dictionary objectForKey:@"FacebookexpirationDate"]];
        [[ApplicationDelegate facebook] fbDialogLogin:[[ApplicationDelegate facebook] accessToken]
                  expirationDate:[[ApplicationDelegate facebook] expirationDate]]; 
    }
}

#pragma mark - FBSessionDelegate Protocol
- (void) fbDidLogout {
    NSLog(@"fbDidLogout");
}
- (void) fbDidLogin {
    NSLog(@"fbDidLogin");
        NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:kPathSharePlistPath];
    [dictionary setObject:[[ApplicationDelegate facebook] accessToken] forKey:@"FacebookaccessToken"];
    [dictionary setObject:[[ApplicationDelegate facebook] expirationDate] forKey:@"FacebookexpirationDate"];
    [dictionary setObject:@"YES" forKey:@"Facebook"];
    [dictionary writeToFile:kPathSharePlistPath atomically:YES];
    NSLog(@"%@",dictionary);

    [dictionary release];
}
- (void) fbDidNotLogin:(BOOL)cancelled {
    NSLog(@"fbDidNotLogin");
}

#pragma mark - FBRequestDelegate Protocol
- (void)requestLoading:(FBRequest *)request {
    NSLog(@"requestLoading");
    /**
     * Called just before the request is sent to the server.
     */
}
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"requestdidReceiveResponse");
    /**
     * Called when the server responds and begins to send back data.
     */
    
}
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"requestdidFailWithError - %@",[error localizedDescription]);
    /**
     * Called when an error prevents the request from completing successfully.
     */
    
    if ([request.url isEqualToString:@"https://api.facebook.com/method/stream.publish"]){
        //		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Done" 
        //                                                       message:@"The message has been posted on your wall" 
        //                                                      delegate:self 
        //                                             cancelButtonTitle:nil 
        //                                             otherButtonTitles:@"OK",nil];
        
        UIButton *submitButton = [[theAlertView subviews] lastObject];
        [submitButton setEnabled:YES];
        [theAlertView setTitle:@"Error"];
        [theAlertView setMessage:@"The message was not posted on your Wall!"];
        //        [theAlertView 
        //        [theAlertView addButtonWithTitle:@"OK"];
        //        [theAlertView release];
        //		[alert show];
        //		[alert release];
//        didPostedOnFacebook = YES;
	}
}
- (void)request:(FBRequest *)request didLoad:(id)result {
    NSLog(@"requestdidLoad");
    /**
     * Called when a request returns and its response has been parsed into
     * an object.
     *
     * The resulting object may be a dictionary, an array, a string, or a number,
     * depending on thee format of the API response.
     */
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithContentsOfFile:kPathFacebookPlistPath];
    NSMutableArray *array = [dictionary objectForKey:@"posted"];
    if(![array containsObject:[newsDictionary objectForKey:@"id"]])
    {
        [array addObject:[newsDictionary objectForKey:@"id"]];
    }
    [dictionary setObject:array forKey:@"posted"];
    [dictionary writeToFile:kPathFacebookPlistPath atomically:YES];
     
    if ([request.url isEqualToString:@"https://graph.facebook.com/me/feed"]){
//		UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Done" 
//                                                       message:@"The message has been posted on your wall" 
//                                                      delegate:self 
//                                             cancelButtonTitle:nil 
//                                             otherButtonTitles:@"OK",nil];
        
        UIButton *submitButton = [[theAlertView subviews] lastObject];
        [submitButton setEnabled:YES];
        [theAlertView setTitle:@"Success"];
        [theAlertView setMessage:@"The message was successfully posted on your Wall!"];
//        [theAlertView 
//        [theAlertView addButtonWithTitle:@"OK"];
//        [theAlertView release];
//		[alert show];
//		[alert release];
        didPostedOnFacebook = YES;
	}
}
- (void)request:(FBRequest *)request didLoadRawResponse:(NSData *)data {
    /**
     * Called when a request returns a response.
     *
     * The result object is the raw response from the server of type NSData
     */
}

#pragma mark - Email Methods
- (void)sendEmailWithRecipients:(NSArray *)recipients 
                       andBody:(NSString *)body 
                    andSubject:(NSString *)subject
{
    
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            [picker setSubject:subject];
            [picker setToRecipients:recipients];
            [picker setMessageBody:body isHTML:YES];
            
            [self presentModalViewController:picker animated:YES];
            [picker release];            
        }
        else
        {
            NSString *stringWithData =[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",[recipients objectAtIndex:0], subject,body];
            NSString *url = [stringWithData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];	
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];            
        }
    }
    else
    {
        NSString *stringWithData =[NSString stringWithFormat:@"mailto:%@?subject=%@&body=%@",[recipients objectAtIndex:0], subject,body];
        NSString *url = [stringWithData stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];	
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }    
}

#pragma mark - MFMailComposeViewControllerDelegate Protocol
- (void)mailComposeController:(MFMailComposeViewController *)controller 
          didFinishWithResult:(MFMailComposeResult)result 
                        error:(NSError *)error {	
	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled: {
			break;
        }
        
		case MFMailComposeResultSaved: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done"
                                                                message:@"The e-mail has been saved!"
                                                               delegate:nil 
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil];
			[alertView show];
			[alertView release];
            break;
		}
			
		case MFMailComposeResultSent: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Done"
                                                                message:@"The e-mail has been sent!"
                                                               delegate:nil 
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil]; 
			[alertView show];
			[alertView release];
            break;
		}
			
		case MFMailComposeResultFailed: {
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:[NSString stringWithFormat:@"The e-mail has failed with error: %@", [error localizedDescription]]
                                                               delegate:nil
                                                      cancelButtonTitle:nil 
                                                      otherButtonTitles:@"OK", nil]; 
			[alertView show];
			[alertView release];
            break;
		}
		default: {
			break;
        }
	}
	
	[self dismissModalViewControllerAnimated:YES];
}

@end
