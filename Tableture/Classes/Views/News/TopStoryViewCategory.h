#import "UILabelCustom.h"
#import "Utilities.h"
#import "AsyncImageView.h"

@interface TopStoryViewCategory : UIView {
    UILabelCustom *teaserLabel;
	UILabelCustom *titleLabel;
	AsyncImageView *imageView;
    
	NSString *teaserString;
	NSString *titleString;
    NSString *imageString;
    
    NSString *newsId;
}

- (void)initWithTitle:(NSString *)title
            andTeaser:(NSString *)teaser
             andImage:(NSString *)image
                andId:(NSString *)theId;

- (void)updateContentOnOrientationChange:(UIInterfaceOrientation)newOrientation;
@end
